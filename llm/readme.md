## llm overview

## llm 功能增強

- 讓 llm 增強的幾種方式
  - 方法
    - [prompt](prompt.md)
      - 需要額外的經驗
      - 成本最小

    - [fine-tune](finetune.md)
      - 需要重新訓練
      - 重新訓練成本高

    - [RAG](rag.md)
      - 需要額外架構輔助，例如，向量數據庫、向量化工具

- 評估標準
  - 讓 llm 根據提示回答的精準，例如，角色扮演，`prompt < fine-tune`
  - 讓 llm 學習預訓練之外的知識，`prompt < RAG`

## ref

- [提示词、RAG、微调哪个会让大模型表现更好](https://www.youtube.com/watch?v=P1iob2uKFrg)
