## Retrieval-Augmented-Generation 索引增強生成

- 基本概念

  利用向量數據庫的索引為 llm 增加額外的知識，讓 llm 可以生成更加準確地回答

- 流程
  - 將原始數據切分成基於 text 的的數據片段
  - 將數據片段轉換為 vector-stored-index 後供 llm 查詢，用於取代原始數據
  - 使用者詢問時，先透過 vector-db 得到 vector-stored-index 的數據後，再傳遞給 llm
  - llm 根據vector-stored-index的數據產生回答

## Agent

- 原理
  可以看作是一個自帶路由消費 Chains 的代理，基於 MRKL 和 ReAct 的基本原理，Agent 可以使用工具和自然語言處理

## Tools
- `Token`
  - tiktoken

- `Framwork`
  - LangChain: 通用建立AI應用的框架，內建許多預設的ToolChain可直接使用
  - LlamaIndex: 建立AI應用的框架: 提供Retrival、vector embedding、top-k retrieve 等功能

- `llm-integrate`
  - Ollama: 提供多種llm供使用者選擇和使用，快速下載和使用選擇的llm

- `Paser` 
  - LlamaParse: 為 LlamaIndex 提供 Index 功能的子模塊，
    可用於複雜的文件，例如，PDF，table、圖表，並為LlamaIndex產生更準確更好的結果
  - [document_loader](https://python.langchain.com/docs/modules/data_connection/document_loaders/) from LangChain: Customize、CSV、FileDirectory、HTML、JSON、md、PDF、

- `Vector-db`
  - Chroma
    - 優，輕量、開源、易用，提供修改的靈活性和擴展能力
    - 優，提供高效的近似最近鄰搜索（ANN）
    - 缺，適合小型語意搜尋的小型項目
    - 特色，擅長建立大型語言模型應用程式和基於音訊的用例

  - Pinecone
    - 優，具有實時搜尋能力，高性能、可擴展，適合用於推薦引擎等應用
    - 優，自動索引，簡化部屬過程
    - 缺，缺乏高級查詢功能
    - 缺，未開源
    - 特色，提供了一種簡單、直觀的方式來開發和部署機器學習應用程式

  - Faiss
    - 優，Facebook 支持
    - 優，高效的相似性搜索，支持各種距離測量: cos、IP、L1、Linf 
    - 優，實現ANN算法，例如，IVF、HNSW、IMI、PQ
    - 優，榮易於與深度學習框架集成（如PyTorch）、且具有豐富的文檔
    - 缺，比 Pinecone 需要更多的配置
    - 特色，Faiss 已成為高效能相似性搜尋的絕佳選擇

  - Weaviate
    - 優，開源
    - 優，支持多模態數據（文本、圖像等）的語義搜索
    - 優，提供強大的查詢語言和推理能力
    - 優，適用於複雜的知識圖譜和知識檢索應用
    - 特色，適合各種應用的靈活向量資料庫
  
  - Milvus
    - 優，開源
    - 優，适用于大规模内容检索、图像和视频搜索等场景，专为处理超大规模向量数据而设计
    - 優，持多种索引类型和查询优化策略
    - 特色，可擴展的索引和查詢功能而迅速

  - superbase
  - deep-lake
  - qdrant
  - vespa
  - vald
  - scaNN
  - pgvector

- `web-ui`
  - gradio:
    - 快速搭建 web-ui
    - 提供公開的 share-link
    - 可[直接託管於 Hugging-Face-space](https://www.youtube.com/watch?v=LS9Y2wDVI0k&t=2s)上，
      免費且永久使用
    - [gradio教學](https://huggingface.co/learn/nlp-course/zh-TW/chapter9/1?fw=pt)

## [tool] langchain 的使用

- langchain flow

  <img src="doc/langchain-flow.jpeg" width=800 height=auto>

- 組成
  - Models:
  - Retriver: 
  - Prompts:
  - Chains:
  - Memory:
  - Agent:

- LangChain Agent
  - Agent 是一個代理，接收用戶的輸入，
    可以使用工具和自然語言處理採取相應的行動然後返回行動的結果。

  - 基於 MRKL 和 ReAct 的基本原理
  
  - Agent 的作用是代表用戶或其他系統完成任務，
    例如，數據收集、數據處理、決策支持等

- ReAct 原理

  ReAct 的目標是探索如何使用 LLM 以交錯的方式生成推理痕跡和特定任務的行動，
  從而在兩者之間實現更大的協同作用

  例如，給小明一個任務：去廚房為你做一杯咖啡。小明不僅要完成這個任務，還要告訴你他是如何一步步完成的

  小明告訴你：“我現在去廚房。”
  小明再說：“我找到了咖啡粉和咖啡機。”
  “我現在開始煮咖啡。”
  “咖啡煮好了，我要加點糖和奶。”
  “好了，咖啡做好了，我現在給你拿過去。”

## 範例

- 範例，利用 python 編寫具有多個工具的 agent + 評估 全流程

  - [完整代碼](https://github.com/techwithtim/AI-Agent-Code-Generator)
  - [完整影片](https://www.youtube.com/watch?v=JLmI0GJuGlY)
  - 代碼流程
    - install requirement > install ollama 
    - 透過 llama_index.Ollama() 創建 llm
    - 透過 llm_parse.LlamaParse() 創建 paser，(需要網路，透過 web-api 取得結果)
    ---
    - 透過 llm_index.core.SimpleDirectoryReader() 讀取原始數據 > 指定解析器 > 加載數據切分結果
    - 透過 llama_index.core.embeding.resolve_embed_model() 創建用於建立 vector-stored-index 的 model
      - 預設使用online的llm，此處改用本地的 local:BAAI/bge-m3 模型來產生VectorStoreIndex
      - 透過 llama_index.core.VectorStoreIndex.from_documents() ，為已切分數據，創建 vector-stored-index
    - 透過 vector_index 產生 query_engine，並指定用於查詢的 llm (第二步的llm)
     透過 query_engine 進行詢問，測試用
    ---
    - 透過 llama_index.core.tools 的 QureyEngineTool創建agent需要的工具
      - QureyEngineTool，用於為 QureyEngine 提供工具
      - ToolMetadata，用於為工具提供說明
    - 創建用於產生 code 的ｌｌｍ，ｃｏｄｅ_llm = Ollama(model="codellama")
    - 透過 llama_index.core.agent.ReActAgent.from_tools(tool, ｃｏｄｅ_llm, ...) 創建 agent
    - 測試 agent

## ref

- langchain
  - [LangChain Agent 原理解析](https://zhuanlan.zhihu.com/p/655141968)
  - [LangChain原理學習筆記](https://blog.csdn.net/Taobaojishu/article/details/136246282)
  - [Langchain架構原理淺談](https://blog.csdn.net/YELLOWAUG/article/details/136219542)