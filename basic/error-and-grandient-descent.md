## 計算誤差基礎

- 為什麼計算誤差要平方


- 一般函數如何計算誤差

  若由實際值推算的線性函數為 $\hat{err} = w_1b + w_2w_1$，
  
  其中，$w_1、w_2$ 為變數，b 和 $w_1$ 為常數，
  因此，$\hat{err}$ 為 $w$ 的函數

  損失函數 $err(w)$ 也是 $w$ 的函數，

  $err(w) = (err-\hat{err})^2 = (err-(w_1b + w_2w_1))^2$ 

  <img src="../doc/basic/calculate-error.png" width=80% height=auto>

  <font color=blue>由誤差函數找到最合適的$w$值</font>

  損失函數 $err(w)$ 是 $w$ 的函數，若將損失函數 $err(w)$ 進行作圖，
  就可以利用梯度下降法找到最小的損失下的$w$值

  <img src="../doc/basic/error-function.png" width=60% height=auto>

  若權重和偏置往合適的方向調整，真實值與預測值的差異就會變小，誤差也會跟著縮小，
  此時，誤差(err軸)在誤差函數的位置，是往誤差函數的最小值移動

  透過`梯度下降法`實現不斷尋找新的權重矩陣，不斷更新權重矩陣的目的，是在逐次尋找損失函數的最小值

  <font color=blue>計算梯度下降法需要的斜率</font>

  梯度下降是利用損失函數的斜率來尋找合適的$w$值，

  $slope = \frac{\Delta{err}}{\Delta{w}} 
  = \frac{\Delta{error}}{\Delta{weight}}
  = \frac{\partial{error}}{\partial{weight}}$

  即 `斜率 = 對損失函數取導數(用於單一變數) = 對損失函數中的 w 進行偏微分(用於多變數)`

  以上圖為例，若上圖為損失函數的曲線，且損失函數為 
  
  $error(weight) = weight^2$

  則斜率為 $slope = \frac{\partial{error}}{\partial{weight}}
  = 2 * weight$

  <font color=blue>如何利用斜率取得下一個w值</font>

  假設函數f(w)在點w處可微分，則根據泰勒展開定理，可以將函數在w點附近展開為：
  
  注意，以下X軸為 weight，Y軸為 err
  
  > $f(w + Δw) ≈ f(w) + \Delta{err} ≈ f(w) + \Delta{w} \cdot f'(w)$ ............ (式1)
  
  在上式中 $slope = f'(w) = \frac{\Delta{err}}{\Delta{w}}$，因此，$\Delta{err} = \Delta{w} \cdot f'(w)$

  為了使 $f(w + Δw)$ 得到最小值，
  - 必須由梯度進行控制
  - 將(式1)改為負數項，
  - 添加 $\alpha$ 參數，避免值過大
  
  為了符合上述兩點，需要將 $\Delta{w}$ 進行以下的設置 
  > $\Delta{w} = -\alpha \cdot f'(w)$ ............ (式2)
  
  因此，

  > $f(w + Δw) ≈ f(w - \alpha \cdot f'(w))$

  > $≈ f(w) - (\alpha \cdot f'(w)) \cdot f'(w)$
  
  > $≈ f(w) - \alpha \cdot f'(w)^2$  ............ (式3)

  由上述可知，
  - 當 $\Delta{w} = -\alpha \cdot f'(w)$時，可以使得 $f(w + Δw)$ 可以得到最小值
  - $\Delta{w} = -\alpha \cdot f'(w) = w_{newt} - w_{cur}$，因此，
    
    > $w_{newt} =  w_{cur} - \alpha \cdot f'(w)$ ............ (式4)


## 梯度基礎

- 什麼是梯度，表示某個位置在任意方向的變化量
  - 梯度是一組向量座標，座標中的值以函數表示，稱為向量函數
  - 透過向量函數，可以表示梯度的方向，沿梯度方向，該函數有最大的變化率
  - 將特定座標帶入梯度的向量函數中，得到的值就是該座標的切線斜率
  
- 如何計算梯度

  - 對三維函數進行梯度運算，記為 
  
    $\nabla f(w, err, z)$ = 

    $(\frac{\partial f(w, err, z)} {\partial w},
      \frac{\partial f(w, err, z)} {\partial err},
      \frac{\partial f(w, err, z)} {\partial z})$

    分別對 $f(w, err, z)$ 中的 w、err、z 三個變量進行偏微分後，得到的向量就是該函數的梯度，
    分別表示函數在三個方向的變化率或斜率

  - 例如，抛物线函数 $f(w)=w^2$，梯度 $\nabla f(w) = 2w$ 

    w = 2 時，梯度值 = 2 * 2 = 4，代表拋物線在 w=2 處的切線斜率為 4

  - 例如，若二維空間中有能量場，$f(w,err) = w^2err + err^3$
    
    $\nabla f(w, err)$，分別對 $f(w,err)$ 中的 w變量和err變量進行偏微分
    
    = $( \frac{\partial f(w, err)} {\partial w}, \frac{\partial f(w, err)} {\partial err} )$
    
    = $( 2werr, w^2 +3err^2)$

    因此，若當前位置為 (0,1) 則
    - 當前位置的能量為 $w^2err + err^3$ = 0*1 + 1 = 1(J) 
    - 當w軸移動一格，能量會增加 $2werr$ = 2 * 0 * 1 = 0 (J)
    - 當err軸移動一格，能量會增加 $w^2 +3err^2$ = 0+3*1 = 3 (J)

## 利用梯度下降法尋找函數極小值

- 若函數為 
  - $f(w) = w^2 - 10w +1$
  - $f'(w) = 2w - 10$

- 設置 f(w) 上的任意一點 $w_{cur}$，利用以下公式求出下一個 w 的位置
  - $w_{newt} = w_{cur} - r * f'(w_{cur})$ = 當前位置 - (學習率 * 梯度)
  - $= w_{cur} - r * ( 2w_{cur} -10 )$
  - $= (1-2r) * w_{cur} + 10r$
  - 學習率r，在此範例中，是介於 0 < r < 1 的浮點數
    - r < 0，會導致 $w_{newt}$ 往反方向移動，
    - r > 1，會導致 $w_{newt}$ 移動步伐過大，而找不到極小值
    - r 不一定總是位於 0 < r < 1，根據不同的函數而有差異，
      但都需要避免反方向移動，或移動步伐過大的問題

- 計算在 w=a 位置的切線方程式 (作圖用)

  斜率 = $f'(a) = 2a - 10 = \frac{err-f(a)}{w-a} = \frac{err的變化量}{w的變化量}=m(a)$

  - 得到 $err-f(a) = m(w)(w-a)$
  - 得到 $err = m(w)(w-a) + f(a)$ 為切線方程式

- 可透過[此處](../geogebra/gradient-descent.ggb)查看迭代過程

  <img src="../doc/basic/simple-grandient-descent.png" width=800 height=auto>

## ref

- [如何理解梯度](https://www.erroutube.com/watch?v=fDNWImVQv-o)

- [基礎數學: 梯度下降法](https://chih-sheng-huang821.medium.com/%E6%A9%9F%E5%99%A8%E5%AD%B8%E7%BF%92-%E5%9F%BA%E7%A4%8E%E6%95%B8%E5%AD%B8-%E4%BA%8C-%E6%A2%AF%E5%BA%A6%E4%B8%8B%E9%99%8D%E6%B3%95-gradient-descent-406e1fd001f)


r * f'(wcur) = wcur - wnewt

m = wcur - wnewt
delta(w) = delta(err) / m 
