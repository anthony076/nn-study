## 線性回歸基礎

- 計算誤差的幾個方法
  - Mean Square Error(MSE、L2): 預測值與真實值的`差值取平方值`做相加後取平均
  - Mean Absolute Error(MAE、L1): 預測值與真實值的`差值取絕對值`做相加後取平均

- 比較MSE和MAE
  - 把MSE做平方根後，得到RMSE，將RMSE和MAE進行比較
  - 優，MAE對異常值敏感，要檢測離群值時，使用MSE很好
  - 缺，MAE在在更新權重時，因為梯度在整個過程都是相同的造成找不到最低點的問題，需要搭配動態學習率來避免

## ref
- [幾種常見的 loss-function](https://home.gamer.com.tw/artwork.php?sn=5368862)
- [Loss Function、Cost Function 和 Objective Function 的區別](https://zhuanlan.zhihu.com/p/77686118)
- [回歸用Loss function](https://ithelp.ithome.com.tw/articles/10236205)，簡單，白話
- [使用損失函數(Loss Functions)來評估ML模型的好壞](https://ithelp.ithome.com.tw/articles/10218158)
- [MSE（L2损失）与MAE（L1损失）的比较](https://blog.csdn.net/fengchi863/article/details/125387045)
- [數據科學【機器學習】｜線性回歸](https://www.youtube.com/watch?v=UkPDtP_21dc)，簡單易懂
- [Loss Function、Cost Function 和 Objective Function 的區別](https://zhuanlan.zhihu.com/p/77686118)，文字版公式推導
- [线性回归算法](https://www.youtube.com/playlist?list=PLGmd9-PCMLha8W8vunYVg-OroC_yFgdHC)，影片，含推導，推導過程清楚
- [损失函数 | Mean-Squared Loss](https://zhuanlan.zhihu.com/p/35707643)
- [神经网络中的Cross-entropy Cost Function](https://zhuanlan.zhihu.com/p/48081819)
- [損失函數(loss function)](https://chih-sheng-huang821.medium.com/%E6%A9%9F%E5%99%A8-%E6%B7%B1%E5%BA%A6%E5%AD%B8%E7%BF%92-%E5%9F%BA%E7%A4%8E%E4%BB%8B%E7%B4%B9-%E6%90%8D%E5%A4%B1%E5%87%BD%E6%95%B8-loss-function-2dcac5ebb6cb)


- [DNN Backpropogation](https://speech.ee.ntu.edu.tw/~tlkagk/courses/MLDS_2015_2/Lecture/DNN%20backprop.ecm.mp4/index.html)
- [深度学习数学基础之链式法则](https://zhuanlan.zhihu.com/p/113112455)





