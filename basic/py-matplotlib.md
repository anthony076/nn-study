## matplotlib 的使用

- matplotlib 架構
  - Figure: GUI物件，最上層的物件
  - Axes: 圖表，多個坐標軸構成圖表，可用於坐標軸的通用設置
  - Axis: 坐標軸，圖表中單一坐標軸

- 參數對應圖
  <img src="./matplotlib/common-parameter-visualize.png" width=800 height=auto>

## 常用方法

- 設置全局參數，
  - `plt.rcParams[參數名]=參數值`
  - `plt.rcParams['font.sans-serif']=['SimHei']`，正常顯示中文標簽
  - `plt.rcParams['axes.unicode_minus']=False`，正常顯示負號

- figure 相關
  - `matplotlib.pyplot.gcf()`，Get-Current-Figure，取得當前的figure
  - `fig.set_size_inches(寬, 高)`，設置 figure 的大小

- 圖表相關
  - `plt.xlabel()`，設置x軸 label
  - `plt.xticks()`，設定x軸對應的刻度，並替換為文字
  - `plt.xlim()`，設定x軸左右範圍
  - `plt.title()`，設置圖標題
  - `plt.text()`，放置文字
  - `plt.legend()`，設置圖例
    - 曲線必須先加上lable，例如，plt.plot(x,y,label='line1')

  - 繪製曲線
    - `plt.plot()`，繪製折線

    - `plt.scatter(x軸數據，y軸數據，c=顏色，marker=標記符號，s=數據點大小)`，繪製散射
      - c = r、g、b、m、c、y，[顏色列表](https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py)
  
      - maker = ,、o、v，[標記符號列表](https://matplotlib.org/stable/api/markers_api.html#module-matplotlib.markers)

    - `plt.subplot()`，在當前視窗中，繪製多子圖

    - `plt.show()`，顯示圖表

- 軸相關
  - `matplotlib.pyplot.gca()`，Get-Current-Axes，取得當前的 axes
  - `ax.spines['right'].set_color('none')`，隱藏右側座標軸
  - `ax.spines['bottom'].set_position(('data',-1))`，設定下方的線，移動至 data 為 -1 的地方
  - `ax.spines['left'].set_position(('axes', 0.5))`， 設定左方的線，移動至坐標軸中心
  - `ax.xaxis.set_ticks_position('bottom')`，x軸刻度顯示在下方

- 分隔線
  - `plt.minorticks_on()`，顯示次要分隔線，若有設置次要分隔線才需要
  - `plt.grid(axis=顯示哪一軸的分隔線,which=分隔線類型, color=分隔線顏色)`，繪製格線
    - axis = x、y、both(default)
    - which = major顯示主要格線、minor顯示次要格線、both(default)
    - x軸分隔線和y軸分隔線可以分開設置

## 範例集

- 最小圖表
  ```py
  import numpy as np
  import matplotlib.pyplot as plt

  x = np.arange(-10,10, 2)
  y = x*x +1

  plt.scatter(x,y)
  plt.show()
  ```

- 建立多個圖表
  ```py
  import numpy as np
  import matplotlib.pyplot as plt

  x = np.arange(-10,10,1)
  y1 = 2*x+1
  y2 = x*x
  y3 = x^3

  fig = plt.figure()

  # 在視窗中添加圖表多個圖表
  # fig.add_subplot(多圖表的長, 多圖表的長, 當前位置)

  ax_1 = fig.add_subplot(1, 3, 1)   # 建立1x3張子圖，當前為第一張
  ax_1.set_title('ax1')               # 建立子圖標題
  ax_1.plot(x,y1)                       # 建立子圖中的圖表
  ax_1.plot(x,y1+3)

  ax_2 = fig.add_subplot(1, 3, 2)
  ax_2.set_title('ax2')
  ax_2.scatter(x,y2)

  ax_3 = fig.add_subplot(1, 3, 3)
  ax_3.set_title('ax3')
  ax_3.plot(x,y3)

  plt.show()
  ```

- 顯示特殊字符
  ```python
  plt.scatter(x, dy_complex, c='b', marker='v', label='$\sigma\'(x) complex$')
  ```

- 分隔線範例
  ```py
  #y軸的分隔線，只顯示主要分隔線，以直線顯示，顏色設置為藍色
  plt.grid(axis='y', which='major', linestyle='-', color='b')
  #y軸的分隔線，只顯示次要分隔線，以虛線顯示，顏色設置為灰色
  plt.grid(axis='y', which='minor', linestyle='--', color='grey')
  #x軸的分隔線，只顯示主要分隔線，以實線顯示
  plt.grid(axis='x', which='major', linestyle='-')
  #開啟次要分隔線的顯示
  plt.minorticks_on()
  ```

## Trouble-Shooting

- Matplotlib is currently using agg, which is a non-GUI backend
  - 原因: 未安裝GUI庫，matplotlib 預設使用 agg 庫，agg庫無法用於顯示圖表
  - 解決: pip install pyqt5

## Ref 

- [Matplotlib基本教學](https://zwindr.blogspot.com/2017/04/python-matplotlib.html)
- [matplotlib 圖表教學](https://steam.oxxostudio.tw/category/python/example/matplotlib-index.html)