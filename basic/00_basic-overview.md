## 神經網路概述

- 資料前處理，參考，[dataset](dataset.md)

- 神經網絡的組成，主要分為
  - `輸入層`: 接收原始數據

  - `隱藏層`:
    - 每個節點都是一個激勵函數，每個激勵函數都有自己的權重(weight)和偏置(bias)

    - 第一層的隱藏層節點的輸出會成為第二層隱藏層的輸入，依此類推，
      直到最後一層隱藏層的輸出連接到輸出層。

    - 每一層`隱藏層`，都是對某個`特徵的提取器`
      
      複雜的特徵通常由多個子特徵組成，對於同一層隱藏層中的每個節點，負責提取當前特徵的一部分或者多個部分，通過多個神經元節點的組合來實現對整個特徵的描述
      
  - `輸出層`: 産生最終的輸出結果

  - `神經元`: 隱藏層中的節點(neural)是一個感知器 
    ```mermaid
    flowchart LR

    id1[輸入\nX<sub>1</sub>] -- x1帶入X --> 
    id2["節點0\nZ<sub>0</sub> = W<sub>0</sub>X + b<sub>0</sub>\n輸出 σ(Z<sub>0</sub>)"]

    id2 -- "σ(Z<sub>0</sub>)帶入X" --> 
    id3["節點1\nZ<sub>1</sub> = W<sub>1</sub>X + b<sub>1</sub>\n輸出 σ(Z<sub>1</sub>)"]

    id3 -- "σ(Z<sub>1</sub>)輸出" --> id4[輸出\nY<sub>1</sub>]

    %% 設置樣式
    classDef leftAlign text-align:left;
    class id2 leftAlign;
    class id3 leftAlign;
    ```

    - 注意， 
      - 前一級的輸出是後一級的輸入，依照此方式形成數據的傳播鏈
      - $\sigma$ 為激勵函數，
      - Z為激勵函數的輸入參數，由 W 和 b 組成，
        - 在神經網路中，可以有多個隱藏層所組成，每個隱藏層都有自己的w和b，
        - 整個神經網路中，所有的隱藏層的Z可以寫成矩陣的形式，又稱為權重矩陣
      - W 為權重，b 為偏移
    
    - 節點1
      - 輸入 = $X_1$
      - 輸出 = $\sigma(Z_0) = \sigma(W_0 \cdot X_1 + b_0)$
    
    - 節點2
      - 輸入 = $\sigma(Z_0)$
      - 輸出
        
        $\sigma(Z_1) = \sigma(W_1 \cdot \sigma(Z_0) + b_1)$
        
        將 $\sigma(Z_0)$ 展開後

        $\sigma(Z_1) = \sigma(W_1 \cdot \sigma(W_0 \cdot X_1 + b_0) + b_1)$

- 神經網路的作用，是將有特定特徵的輸入，映射到一組特定的輸出，
  - 透過神經網路的`隱藏層`，記住某個特定的`特徵` (越多層隱藏層可以記住更多的特徵)
  - 透過隱藏層的`神經元`，描述該特徵的其中一小部分 (越多個神經元可以描述越完整的特徵)
  - 神經網路記住的特徵，是由每個隱藏層神經元的權重矩陣(權重+偏置)進行描述
  - 特徵記得的越多，預設值輸出的結果越接近實際值

- 神經網路的設計
  - 增加層數和節點數可以提高模型的擬合能力，但也容易導緻過擬合
  - 需要綜合考慮模型的`複雜度`、`數據的規模`和`特徵`等因素

## 訓練過程概述

- 基本概念
  
  神經網路類似線性回歸中的回歸方程式，是由多條線性方程式所組成的函數矩陣，例如，
  
  $z_1 = a_1x_1 + a_2y_1 + .... + a_4$

  $z_2 = b_1x_2 + b_2y_2 + .... + b_4$
  
  $z_3 = c_1x_3 + c_2y_2 + .... + c_4$
  
  所有符合該方程式的點，都是函數的解，並且可以繪製在座標空間中，
  權重和偏置相當於多項式中，分別對應每個變數(x1-x3,y1-3)的係數(a1-a3、b1-b3、c1-c3)和常數(a4、b4、c4)

  在訓練初期，神經網路所建構的回歸方程式必定和實際數據曲線必定有誤差，
  透過輸入的數據，不斷調整回歸方程式中每個變數的係數和常數值，使得回歸方程式貼近實際數據曲線的結果，
  稱為神經網路的訓練過程，其中，

  神經網路的正向傳播，相當於提供回歸方程式輸入，並取得回歸方程式的`預測值`y，

  神經網路的反向傳播，相當於更新回歸方程式的係數和常數
    - step1，`預測值`y和`實際值`$\hat{y}$ 的差異，
    - step2，計算函數的導數
    - step3，梯度下降法，取得回歸方程式新的係數和常數
  
  不斷重複上述的過程，使得神經網路所建構的回歸方程式，貼近於提供訓練的數據

- 神經網絡的訓練過程，分為`前向傳播`和`反向傳播`

  - `前向傳播`: 提取當前輸入的`特徵值`

    - 如何透過前向傳播獲得輸出

      每個隱藏層都是對某個特定特徵的提取，利用隱藏層中`所有節點的權重和偏置的組合`，形成對該特徵的描述，
      
      透過神經網路的訓練，會不斷地調整權重和偏置的值，
      最終，會找到一組權重和偏置的值，讓該特徵被描述得最為準確

      <img src="../doc/basic/forward-propagation.png" width=800 height=auto>

    - [神經元的激勵函數(輸出函數)](active-function.md)

  - `反向傳播`: 利用更新權重尋找最小誤差

    - 反向傳播的目的是`找到損失函數的最小值`，`更新權重矩陣`是尋找`損失函數最小值`的過程

    - 反向傳播是將誤差值，從輸出端傳遞到輸入端(由右向左)，與前向傳播的方向相反(由左向右)
    
    - 通過多次叠代訓練，`神經網絡的權重和偏置逐漸趨於最優值`，使得神經網絡對訓練數據的預測誤差最小
      
      神經網路的好壞，取決於`映射的輸出`(神經網路的輸出或稱預測值)與`實際值`的差異，且與`權重矩陣有關`

      預測值和實際值的差異稱為誤差值，誤差值的多寡，取決於`神經網路能記住的輸入特徵`與`實際值`之間的差異
      - 記住的輸入特徵越多，輸出的預設值與實際值差異越小，誤差越小
      - 記住的輸入特徵越少，輸出的預設值與實際值差異越大，誤差越大

      因此，一個好的神經網路要找到誤差的最小值，就需要找到合適的權重矩陣，使得輸出的預測值與實際值盡可能地接近

## [反向傳輸] 計算損失

- 損失是`真實值與預測值差值的平方`，損失函數是`權重組成的函數`
  
- [一般函數如何計算誤差](error-and-grandient-descent.md)

- 神經網路如何計算誤差

## [反向傳輸] 損失函數(loss-function)的選擇

- ??loss function: L(θ) 等於所有的training data的loss總和

- 每一層的計算損失的方式都不同
  - 輸出端誤差`(反向傳播的第一層)` = 實際值$\hat{y}$ - 預測值$y$
  - 隱藏層的誤差`(反向傳播的中間層)` = 所有(上一層反向傳播層的誤差 * 該節點的權重)之和
  - 計算完所有節點後，利用梯度下降法，計算新的權重值

- 在代碼中計算損失的流程
  - step1，根據結果計算`預測值`y和`實際值`$\hat{y}$之間的誤差
  - step2，根據誤差和梯度下降法，計算出新的權重矩陣
  - step3，套用新的權重矩陣

## [反向傳輸] 優化器(optimizer) 和 損失函數

- 使用場景: 先計算損失函數，再進行參數優化

  當神經網絡的權重被初始化後，`optimizer 被用於調整神經網路中每個節點的權重參數`，
  以`最小化loss-function`並提高神經網絡的性能，使神經網絡的輸出更`接近期望的目標`

  optimizer可使用不同的優化策略來更新參數，例如，
  - SGD，基於梯度下降法 (gradient-descent)的優化器
  - Adam，結合`梯度下降`和`動量`的優化器，它可以更快地收斂並提高準確率
  - RMSProp，是一種適應性優化器，它可以`根據梯度的大小`，自動調整學習率

  loss-function用來衡量神經網路`預測值`和`真實值`之間的差異的函數。損失函數的值越小，表示神經網路預測結果越接近真實值，

- 常見的 loss-function 有
  - MSE
  - MAE
  - 交叉熵損失法(Cross-antrophy)

- loss-function 依照使用場景分類
  - 回歸問題的 loss-function
    - RMSE
    - MAE
  
  - 分類問題的 loss-function
    - cross-entropy

- 優化器和損失函數的搭配
  - MSE + SGD : 因為梯度下降法可以最小化均方誤差
  - Cross-antrophy + Adam : 動量可以幫助模型更快地收斂

- 優化方法1，梯度下降法(Gradient Descent)
  - 參數: 學習率(Learn Rate)
    - 若學習率太高(動力太大)就會找不到最低點
    - 若是動力太小找到最低點則會非常的緩慢

- 原理
  - step1，先計算誤差
    - 誤差函數如何選擇
    - 如何計算誤差，
    - 如何評估誤差(如何決定是否繼續訓練，$R^2$)
  
  - step2，進行參數優化
    - 參數優化流程
    - 如何計算 delta
    - 如何利用 delta 得到新的參數

## 用於更新權重的梯度下降法

- [梯度下降基礎](grandient-descent.md)

- 在微積分說將要找極大值或極小值的方程式，需要對該曲線方程式做微分等於0找解
  - 若解帶入方程式得到的值 > 0，找到極小值
  - 若解帶入方程式得到的值 < 0，找到極大值

- 對沒有唯一解的方程式，可以使用`梯度下降法(gradient descent)`，找近似解的方式去逼近極值

  在神經網路中，需要找到損失函數的極小值，一般可以透過`回歸分析法`或`梯度下降法`尋找極小值，

  但回歸分析法需要計算反矩陣，計算複雜度相對梯度下降法來的更複雜，
  因此一般使用梯度下降法來尋找損失函數的最小值


## 訓練神經網路容易遇到的問題

- 過擬合 (無法舉一反三)

  在數學上合適的擬合，允許擬合線與實際數據有些許的差距

  在神經網路中，過擬合代表神經網路對於`訓練數據集有高度的準確性`，但對於`新數據或稍有差異的數據`時就造成準確率大幅下降

  <img src="../doc/basic/overfitting.png" width=800 height=auto>

  造成過擬合的原因
  - 模型過於複雜: 以至於可以很好地擬合訓練數據中的噪聲和隨機誤差，但卻不能泛化到新的數據，
  - 過度依賴某些特徵或模式
  
  例如，DNN，對輸入數據的依賴性較強，對輸入數據的微小變化非常敏感，導緻DNN在處理一些噪聲較大或不規則的數據時性能下降

  常見的解決方式
  - 增加訓練數據
  - 簡化模型
  - 使用正則化

## 模型分類

- 用於產生圖像的模型
  - dnn: deep-neural-network，深度神經網路，
    參考，[dnn基礎](model/01_dnn.md)

  - cnn: Convolutional neural network，卷積神經網路，
    參考，[cnn基礎](model/01_cnn.md)

- 用於`自然語言處理`和`電腦視覺`的模型
  - rnn (lstm)
  - transformer 架構
    - 預訓練模型，gpt(Generative Pre-trained Transformer)
    - 預訓練模型，BERT(Bidirectional Encoder Representations from Transformers)
    - 預訓練模型，XLNet
    - 預訓練模型，RoBERTa

- 用於生成高質量的圖像、文本和音頻等內容
  - 使用場景: 文生圖、圖生圖、圖片補全、
  
  - latent-diffusion-model 架構
    - 預訓練模型，stable-diffusion
    - 預訓練模型，dalle
    - 詳見，[diffusion-model](model/diffusion.md)

## ref

- [自學機器學習 @ 王木頭](https://www.youtube.com/playlist?list=PLxIHUhMHF8okwhq8poRuiHBChWjkVUHLL)
  
- [A Step by Step Backpropagation Example](https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/)
  
- [Backpropagation in Python (no 3rd module, pure math)](https://github.com/mattm/simple-neural-network/blob/master/neural-network.py)
  
- [Backpropagation Visualization](https://www.geogebra.org/m/dyq2rcup)
- [back-propagation 推導過程+圖例](http://galaxy.agh.edu.pl/~vlsi/AI/backp_t_en/backprop.html)
  
- [深度學習基礎+進階+應用+實踐+面試](https://paddlepedia.readthedocs.io/en/latest/tutorials/deep_learning/index.html)
- [machine learning 學習筆記系列](https://ithelp.ithome.com.tw/users/20120424/ironman/2522)

- [How Neural Networks Work](https://www.youtube.com/playlist?list=PLVZqlMpoM6kaJX_2lLKjEhWI0NlqHfqzp)
  - [How Deep Neural Networks Work](https://www.youtube.com/watch?v=ILsA4nyG7I0)
  - [RNN and LSTM](www.youtube.com/watch?v=WCUNPb-5EYI)

- [書]打下最紮實AI基礎不依賴套件 :手刻機器學習神經網路穩健前進
  - [介紹](https://www.books.com.tw/products/0010913397)
  - [源碼](https://github.com/hwdong-net/Anatomy_Deep_Learning)
