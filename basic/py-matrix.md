
## 在 python 中建立矩陣

- [矩陣基礎](../math/matrix.md#矩陣基礎)

- 利用 list 建立矩陣
  ```python
    >>> a = [[[1,2],[3,4]],[[5,6],[7,8]]]   # 建立 2x2x2 的三維矩陣
    >>> a[0]          # 存取面(2d，2x2)
    [[1, 2], [3, 4]]

    >>> a[0][1]       # 存取列(1d, 1x2)
    [3, 4]

    >>> a[0][1][0]    # 存取元素(element)
    3
  ```

  存取 $a_{ijk}$ 的值，可利用矩陣的索引值存取，$a[i][j][k]$，其中，
  - `[i]`，代表最外層的索引，存取指定面
  - `[i][j]`，代表中層的索引，存取指定列
  - `[i][j][k]`，代表最內層的索引，存取指定元素

## 利用 numpy 操作矩陣

- 建立 ndarray 類型物件
  - np.array()，若輸入是ndarray物件，返回`非引用類型`的ndarray物件(深拷貝)

  - np.asarray()，若輸入是ndarray物件，返回`引用類型`的ndarray物件(淺拷貝)
    
    範例，array()和asarray()的差異
    ```python
    >>> a = np.array([1,2,3,4,5])
    >>> a = [1,2,3,4,5]
    >>> b = np.array(a)   # array()的輸入，是 ndarray 類型值
    >>> b
    array([1, 2, 3, 4, 5])
    >>> a[0] = 10         # 修改輸入a
    >>> b                 # a的修改不影響b
    array([1, 2, 3, 4, 5])

    >>> x = [1,2,3,4,5]   
    >>> y = np.array(x)   # array()的輸入，是 list 類型值
    >>> x[0] = 10         # 修改輸入x
    >>> y                 # x的修改不影響y
    array([1, 2, 3, 4, 5])

    >>> x = [1,2,3,4,5]   
    >>> y = np.asarray(x) # asarray()的輸入，是 list 類型值
    >>> x[0] = 10         # 修改輸入x
    >>> y                 # x的修改不影響y
    array([1, 2, 3, 4, 5])

    >>> i = np.asarray([1,2,3,4,5])
    >>> j = np.asarray(i) # asarray()的輸入，是 ndarray 類型值
    >>> i[0] = 10         # 修改輸入i
    >>> j                 # 注意，i的修改會影響j
    array([10,  2,  3,  4,  5])
    ```

  - np.reshape()，改變 ndarray 物件的形狀

    先建立新形狀，再依序填入，注意，兩者的數量需一致
    ```python
    >>> a = np.array([1,2,3,4,5,6])
    >>> b = np.reshape(a,[1,2,3])
    >>> b
    array([[[1, 2, 3],[4, 5, 6]]])
    >>> b[0]
    array([[1, 2, 3],[4, 5, 6]])
    ```

- 快速產生數列
  - np.arange(起始值，停止值，間隔)，透過`等差數列`產生一維矩陣
    
  - np.linspace(起始值，停止值，個數)，透過`等距`的等差數列產生一維矩陣

  - np.logspace(起始值，停止值，個數)，透過等比數列產生一維矩陣
    - step1，透過起始值，停止值，個數，產生等差數列
    - step2，以等差數列中的值為指數，建立以10為基底的等比數列

- 填滿特定值
  - np.full()，填滿指定值
  - np.empty()，填滿空數據
  - np.zeros()，填滿0
  - np.ones()，填滿1
  - np.eye()，對角線為1，其餘為0

- 填滿隨機數
  - 參考，[正態分布基礎](../math/normal-distribution.md)

  - np.random.rand(1d個數，2d個數，3d個數，...)，
    - 值在[0,1]區間內
    - `均勻取樣`的隨機數陣列
    - 需手動指定每一維的個數

  - np.random.random(形狀)
    - 值在[0,1]區間內
    - `均勻取樣`的隨機數陣列
    - 透過 tuple 指定每一維的個數

  - np.random.randn(1d個數，2d個數，3d個數，...)
    - 值在[0,1]區間內
    - `正態分布取樣`的隨機數陣列，且符合標準正態分布的陣列

  - np.random.normal()
    - 值在[0,1]區間內
    - `正態分布取樣`的隨機數陣列，不一定是符合標準正態分布，但可以轉換為符合標準正態分布
  
  - np.random.randint(low，high，size，dtype)
    - 值在[low,high]區間內的隨機數陣列

  - 範例，將一般正態分布的隨機矩陣，轉換為標準正態分布的隨機矩陣
    ```python
    >>> import numpy as np

    # 建立標準正態分布的隨機矩陣
    >>> s = np.random.randn(2,3)
    >>> s
    array([[ 0.7260026 , -1.47953704, -0.59344456],
          [ 0.1907083 , -0.42416682,  0.63206323]])

    # 建立一般正態分布的隨機矩陣(非標準)
    # np.random.normal(mu, sigma, size)
    >>> n = np.random.normal(2,0.3,size=(2,3))
    >>> n
    array([[1.69240291, 2.4956801 , 1.38276674],
          [2.76046899, 1.86854513, 2.37392757]])

    # 將一般正態分布的隨機矩陣，轉換為標準正態分布的隨機矩陣
    # z = (x - mu)/sigma
    >>> t = (n-2)/0.3
    >>> t
    array([[-1.02532363,  1.65226701, -2.05744419],
          [ 2.53489665, -0.43818289,  1.24642523]])
    ```

- 合併操作
  - np.append()，增加新陣列
  - np.concatenate()，多個陣列合併，將元素添加到陣列中，不會增加新維度

  - np.stack()，堆疊多個陣列，或沿指定軸累加特定位置的元素，多個一維陣列堆疊會成二維陣列，會增加新維度

  - np.column_stack()，取出column方向的元素，並放入一維矩陣中，
    並將不同column中的元素，放在不同的一維矩陣，最終形成一個二維矩陣
    ```python
    >>> a = np.array([1,2,3])
    >>> b = np.array([4,5,6])
    >>> c = np.column_stack((a,b))
    >>> c
    array([[1, 4],
          [2, 5],
          [3, 6]])
    ```

    $\begin{bmatrix}
    1 & 2 & 3 \\
    \end{bmatrix}$ a為1行3列

    $\begin{bmatrix}
    4 & 5 & 6 \\
    \end{bmatrix}$ b為1行3列

    $\begin{bmatrix}
    1 & 4 \\
    2 & 5 \\
    3 & 6 \\
    \end{bmatrix}$ c為3行2列

    注意，np.column_stack() 有類似轉置的效果

  - np.hstack()，將一維矩陣沿水平方向(axis=1)進行堆疊
    ```python
    >>> a = np.array([[1],[2],[3]])
    >>> b = np.array([[4],[5],[6]])
    >>> np.hstack((a,b))
    array([[1, 4],
          [2, 5],
          [3, 6]])
    ```

    $\begin{bmatrix}
    1 \\
    2 \\
    3 \\
    \end{bmatrix}$ a為3行1列

    $\begin{bmatrix}
    4 \\
    5 \\ 
    6 \\
    \end{bmatrix}$ b為3行1列

    $\begin{bmatrix}
    1 & 4 \\
    2 & 5 \\
    3 & 6 \\
    \end{bmatrix}$ c為3行2列

    注意，沿水平方向堆疊時，垂直方向的元素數量不會增加

  - np.vstack()，將一維矩陣沿垂直方向(axis=0)進行堆疊
    ```python
    >>> a = np.array([1,2,3])
    >>> b = np.array([4,5,6])
    >>> c = np.vstack((a,b))
    >>> c
    array([[1, 2, 3],
          [4, 5, 6]])
    ```

    $\begin{bmatrix}
    1 & 2 & 3 \\
    \end{bmatrix}$ a為1行3列

    $\begin{bmatrix}
    4 & 5 & 6 \\
    \end{bmatrix}$ b為1行3列

    $\begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6\\
    \end{bmatrix}$ c為2行3列

    注意，沿垂直方向堆疊時，水平方向的元素數量不會增加

- 分割操作
  - python的切片操作是深拷貝，numpy的切片操作是淺拷貝
    ```python
    >>> a = [1,2,3]
    >>> b = a[1:]
    >>> b         # b為a的切片
    [2, 3]

    >>> a[2]=666  # 修改a值
    >>> a
    [1, 2, 666]
    >>> b         # 修改a值，b值不會改變(深拷貝)
    [2, 3]

    # =============================
    >>> x = np.array([1,2,3])
    >>> y = x[1:]   # y為x的切片
    >>> y
    array([2, 3])

    >>> x[2] = 666  # 修改x值
    >>> x
    array([  1,   2, 666])
    >>> y
    array([  2, 666])   # 修改x值，y值會跟著改變(淺拷貝)
    ```

  - 對numpy使用`整數陣列索引`，可以得到新的陣列(深拷貝)，改善numpy切片是淺拷貝的問題
    
    > 1d使用方式: a = b[ [col-0, col-1, col-2, ...]]
    > 1d實際元素: [ b[0, col-0], b[0, col-1], b[0, col-3], b[0, col-n], ...]

    > 2d使用方式: a = b[ [row-0, row-1, row-2, ...], [col-0, col-1, col-2, ...] ]
    > 2d實際元素: [ b[row-0, col-0], b[row-1, col-1], b[row-3, col-3], b[row-n, col-n], ...]

    ```python
    >>> x = np.array([1,2,3])
    >>> y = x[[1,2]]    # 使用(整數陣列索引)建立切片
    >>> y
    array([2, 3])

    >>> x[2] = 666      # 修改x值
    >>> y           
    array([2, 3])       # 修改x值，y值不會改變(深拷貝)

    # =========================================
    >>> a = np.array([[1,2],[3,4],[5,6]])

    >>> b=a[[0,1,2],[0,1,0]]  # 取(0,0)=1, (1,1)=4, (2,0)=5 
    >>> b
    array([1, 4, 5])

    >>> c = a[[0,0],[1,1]]    # 取(0,1)=2, (0,1)=2
    >>> c
    array([2, 2])
    ```

  - np.split(array, 個數) 或 np.split(array, [個數1, 個數2, 個數3, ...])
    ```python
    >>> a = np.arange(9)
    >>> np.split(a, 3)    # 每3個元素切割成一個新陣列
    [array([0, 1, 2]), array([3, 4, 5]), array([6, 7, 8])]

    >>> a
    array([0, 1, 2, 3, 4, 5, 6, 7, 8])

    # 注意，每次切割都會從index0的位置開始切分，且個數包含已經切割的元素
    # 最後一個陣列是剩餘未切割的元素陣列，若沒有未切割元素，則返回空陣列

    >>> np.split(a, [2,2,2,2])  
    [array([0, 1]), array([], dtype=int32), array([], dtype=int32), array([], dtype=int32), array([2, 3, 4, 5, 6, 7, 8])]
    
    >>> np.split(a,[3,5,6,10])  
    [array([0, 1, 2]), array([3, 4]), array([5]), array([6, 7, 8]), array([], dtype=int32)]
    ```
  
  - np.hsplit(array, 水平移動次數)，沿著水平移動後，進行垂直切割

  - np.vsplit(array, 垂直移動次數)，沿著垂直移動後，進行水平切割
    ```python
    >>> x = np.arange(16)
    >>> x = np.arange(16).reshape(4,4)
    >>> x
    array([[ 0,  1,  2,  3],
          [ 4,  5,  6,  7],
          [ 8,  9, 10, 11],
          [12, 13, 14, 15]])

    >>> a = np.hsplit(x,2)    # 水平移動，垂直切割
    >>> a
    [array([[ 0,  1],[ 4,  5],[ 8,  9],[12, 13]]),
     array([[ 2,  3],[ 6,  7],[10, 11],[14, 15]])]

    >>> b = np.vsplit(x,2)    # 垂直移動，水平切割
    >>> b
    [array([[0, 1, 2, 3],[4, 5, 6, 7]]), 
    array([[ 8,  9, 10, 11],[12, 13, 14, 15]])]
    ```
  
- 邊緣填充
  - 使用方式
    
    np.pad(
      array,
      ((`垂直方向頭部`填充數量, `垂直方向尾部`填充數量),(`水平方向頭部`填充數量, `水平方向尾部`填充數量)), 
      mode=填充模式, 
      constant_values=(頭部填充值, 尾部填充值)
    )

    <font color=blue>注意，水平方向填充的是值，垂直方向填充的是陣列</font>

  - 範例，`mode=constant` 模式，
    
    可指定填充值，填充值莫認為0
    ```python
    >>> a = np.array([[1,2,3],[4,5,6]])
    >>> a
    array([[1, 2, 3],
          [4, 5, 6]])
    >>> np.pad(a,((0,0),(0,0)),'constant', constant_values=0)   # 填充數量皆為0，代表不填充任何值
    array([[1, 2, 3],
          [4, 5, 6]])

    >>> np.pad(a,((0,0),(0,1)),'constant', constant_values=0)   # 列方向(水平方向)的尾部填充1個0
    array([[1, 2, 3, 0],
          [4, 5, 6, 0]])

    >>> np.pad(a,((0,0),(2,1)),'constant', constant_values=0)   # 列方向(水平方向)的尾部填充1個0，頭部填充兩個0
    array([[0, 0, 1, 2, 3, 0],
          [0, 0, 4, 5, 6, 0]])

    >>> np.pad(a,((1,0),(2,1)),'constant', constant_values=0)   # 行方向(垂直方向)的頭部填充1個0的陣列，
    array([[0, 0, 0, 0, 0, 0],
          [0, 0, 1, 2, 3, 0],
          [0, 0, 4, 5, 6, 0]])

    >>> np.pad(a,((0,2),(0,0)),'constant', constant_values=0)   # 行方向(垂直方向)的尾部填充2個0的陣列，
    array([[1, 2, 3],
          [4, 5, 6],
          [0, 0, 0],
          [0, 0, 0]])

    # 頭部填充2個，尾部填充3個，填入常數，頭部填入常數1，尾部填入常數-1
    >>> np.pad([4,5,6], (2,3), mode='constant', constant_values=(1,-1))
    array([ 1,  1,  4,  5,  6, -1, -1, -1])
    ```

  - 範例，`mode=edge` 模式，
    
    不可指定填充值，自動`取得最靠近的元素`作為填充值 
    ```python
    # 頭部填充2個，尾部填充3個，頭部填入常數4，尾部填入常數6
    >>> np.pad([4,5,6], (2,3), mode='edge')
    array([4, 4, 4, 5, 6, 6, 6, 6])
    ```

  - 範例，`mode=minimum / mode=maximum` 模式，

    不可指定填充值，自動取得輸入陣列的最小值或最大值做為填充值
    ```python
    >>> np.pad([4,5,6], (2,3), mode='minimum')
    array([4, 4, 4, 5, 6, 4, 4, 4])

    >>> np.pad([4,5,6], (2,3), mode='maximum')
    array([6, 6, 4, 5, 6, 6, 6, 6])
    ```

- 增加軸
  - np.expand_dims(array, axis=新增軸的位置)

    ```python
    >>> x = np.arange(6).reshape(2,3)
    >>> x
    array([[0, 1, 2],
          [3, 4, 5]])
    >>> x.shape
    (2, 3)        # 未增加軸

    >>> y = np.expand_dims(x, axis=0)   # 在 axis=0 的位置增加軸
    >>> y
    array([[[0, 1, 2],
            [3, 4, 5]]])
    >>> y.shape
    (1, 2, 3)

    >>> y = np.expand_dims(x, axis=1)   # 在 axis=1 的位置增加軸
    >>> y
    array([[[0, 1, 2]],
          [[3, 4, 5]]])
    >>> y.shape
    (2, 1, 3)
    ```

  - 透過 np.newaxis 新增軸
    ```python
    >>> x = np.arange(6).reshape(2,3)
    >>> x
    array([[0, 1, 2],
          [3, 4, 5]])
  
    >>> y = x[:, np.newaxis]
    >>> y
    array([[[0, 1, 2]],

          [[3, 4, 5]]])
    >>> y.shape
    (2, 1, 3)

    >>> z = x[:, :, np.newaxis]
    >>> z
    array([[[0],
            [1],
            [2]],

          [[3],
            [4],
            [5]]])
    >>> z.shape
    (2, 3, 1)
    ```

- 交換軸
  - np.swapawes(array, axisA, axisB)，將axisA的數據和axisB的數據位置互換
    ```python
    >>> a = np.random.random((2,3,4,5))
    >>> b = np.swapaxes(a, 0, 2)  # 將 axis-0 和 axis-2 的數據互換

    >>> a.shape     # 交換軸之前
    (2, 3, 4, 5)
    >>> b.shape     # 交換軸之後
    (4, 3, 2, 5)
    ```

  - np.rollaxis(array, 目標軸位置, start軸位置)，
    - 將目標軸的數據`向前移動`到`start`軸的位置
    - 將目標軸的數據`向後移動`到`start-1`軸的位置
    - 以上兩種方法最終的結果，都會使目標軸的數據，移動到start軸之前
    - 此方法可以由 np.moveaxis() 取代

    ```python
    >>> a = np.random.random((2,3,4,5))
    >>> a.shape
    (2, 3, 4, 5)

    >>> b = np.rollaxis(a,2,0) # 目標軸(2)在後，start軸(0)在前，目標軸(往前)移動到(目標軸)的位置
    >>> b.shape
    (4, 2, 3, 5)

    >>> c = np.rollaxis(a,0,2) # 目標軸(0)在前，start軸(2)在前，目標軸(往後)移動到(目標軸-1)的位置
    >>> c.shape
    (3, 2, 4, 5)
    ```
  
  - np.moveaxis(array, sourceAxis, distAxis)，將 sourceAxis 的數據與distAxis的數據進行交換

  - np.transpose(array, axes=(自定義排列順序))

    若只有兩軸，axes可設置為None，會自動將兩軸進行反向
    ```python
    >>> a = np.arange(8).reshape(2,4)
    >>> b = np.transpose(a)
    >>> b.shape
    (4, 2)
    # ====================
    >>> a = np.random.random((2,4,3,5))
    >>> b = np.transpose(a,(2,0,3,1))     # 依照 index2=3, index0=2, index3=5, index1=4 排列
    >>> b.shape
    (3, 2, 5, 4)
    ```

- 其他常用操作
  - np.repeate()，將輸入的矩陣，沿著指定軸複製多次
  - np.tile()，類似np.repeate()，但可以多個軸進行複製

## numpy 利用廣播，對不同形狀的矩陣進行逐元素計算

- 廣播指 numpy 對不同形狀的矩陣進行算術運算時，會`將小形狀的矩陣或數值進行擴展`，使兩個矩陣的形狀相同後，再進行逐元素運算

- 廣播(元素擴展)的方法
  - 若 A 為 m x n 的矩陣，B 為 k x p 的矩陣
  - 擴展後的矩陣 C 為 i x j 的矩陣
  - 矩陣C為行個數i=Max(m, k)
  - 矩陣C為列個數j=Max(n, p)

- 可擴展的狀況
  - 其中一個操作數是`常量`，常量可擴展成任意形狀(範例1)
  - 某個維度的`個數出現1`，其他維度的個數相同(範例2)
  - 少一個維度，但剩下兩個維度需要是`個數相同`(範例3)，或是個數為1(範例4)
  - 可擴展範例
    ```
    # 範例1
    A : 3 x 4 x 5
    B : 常量5       A 和 B 是可廣播的

    # 範例2
    A : 3 x 4 x 5
    B : 3 x 1 x 5   A 和 B 是可廣播的

    # 範例3
    A : 3 x 4 x 5
    B :     4 x 5   A 和 B 是可廣播的

    # 範例4
    A : 3 x 4 x 5
    B :     1 x 5   A 和 B 是可廣播的
    ```

- 範例，2x3矩陣 + 數值

  $\begin{aligned}
  \begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6 \\
  \end{bmatrix} 
  +
  \textcolor{blue}{3}
  & = 
  \begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6 \\
  \end{bmatrix} + 
  \textcolor{blue}{
    \begin{bmatrix}
      3 & 3 & 3 \\
      3 & 3 & 3 \\
    \end{bmatrix}
  } \\
  & = 
  \begin{bmatrix}
    4 & 5 & 6 \\
    7 & 8 & 9 \\
  \end{bmatrix}
  \end{aligned}
  $

- 範例，3x1矩陣 + 1x2矩陣

  $\begin{aligned}
  \textcolor{green}{
    \begin{bmatrix}
    1 \\
    2 \\
    3
    \end{bmatrix} 
  }
  +
  \textcolor{blue}{
    \begin{bmatrix}
    4 & 5 \\
    \end{bmatrix}
  }
  & = 
  \textcolor{green}{
    \begin{bmatrix}
    1 & 1 \\
    2 & 2 \\
    3 & 3 \\
    \end{bmatrix}
  } + 
  \textcolor{blue}{
    \begin{bmatrix}
      4 & 5 \\
      4 & 5 \\
      4 & 5 \\
    \end{bmatrix}
  } \\
  & = 
  \begin{bmatrix}
    5 & 6 \\
    6 & 7 \\
    7 & 8 \\
  \end{bmatrix}
  \end{aligned}
  $

- 範例，不能進行廣播的狀況
  ```py
  >>> a = np.array([[1,2],[3,4],[5,6]])
  >>> b = a.reshape(2,3)
  >>> b       # array([[1, 2, 3],[4, 5, 6]])

  >>> a+b     # 錯誤，不是常量、對應軸的個數不一致，也沒有出現1，
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ValueError: operands could not be broadcast together with shapes (3,2) (2,3)
  >>>
  ```

## 利用矩陣執行點積運算

- [點積運算基礎](../math/matrix.md#利用矩陣計算點積)

- 利用 `np.dot()` 或 `np.matmul()` 或 `@` 計算內積
  ```py
  >>> a = [[1,2,3],[4,5,6]]
  >>> a = np.array([[1,2,3],[4,5,6]])
  >>> b = np.array([2,5])
  >>> a.shape   #(2, 3)
  >>> b.shape   #(2,)
  
  # 方法1
  # 狀況1，np.dot(b,a) = (2 x 1).(2 x 3)，錯誤，不符合矩陣內積的限制，(m x n).(n x p)
  # 狀況2，
  # b是一維矩陣，可以寫成行矩陣， b=[2,5]，b^T=[[2,5]]，形狀為 (1,2)
  # np.dot(b,a) = (b^T).(a) = (1 x 2).(2 x 3)，可進行計算，m=1,n=2,p=3
  >>> np.dot(b,a)       
  
  # np.dot(b,a) = (b^T).(a)=([[2,5]]).([[1,2,3],[4,5,6]]]) = [c11, c12, c13]
  #c11 = sum(a_1n * b_n1) = (a_11*b_11)+(a_12*b_21)+(a_13*b_31) = (2*1)+(5*4)=22
  #c12 = sum(a_1n * b_n2) = (a_11*b_12)+(a_12*b_22)+(a_13*b_32) = (2*2)+(5*5)=29
  #c13 = sum(a_1n * b_n3) = (a_11*b_13)+(a_12*b_23)+(a_13*b_33) = (2*3)+(5*6)=36
  array([22, 29, 36])

  # np.dot(a,b) = a.b = (2 x 3).(2 x 1)，錯誤，不符合矩陣內積的限制，(m x n).(n x p)
  # np.dot(a,b) = a.b^T = (2 x 3).(1 x 2)，錯誤，不符合矩陣內積的限制，(m x n).(n x p)
  >>> np.dot(a,b)         # Error，無論 b 如何轉置都無法法滿足矩陣內積的限制

  >>> np.matmul(b,a)      # 方法2
  array([22, 29, 36])

  >>> b@a                 # 方法3
  array([22, 29, 36])
  ```
