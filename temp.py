import numpy as np
import matplotlib.pyplot as plt

def sigma(x):
  return 1 / (1 + np.exp(-x))

def sigma_derivatived_simple(x):
  # 導函數，公式已簡化的版本
  return sigma(x) * (1-sigma(x))
  
def sigma_derivatived_complex(x):
  # 導函數，公式未簡化的版本
  return np.exp(-x) / np.square((1+np.exp(-x)))

x = np.arange(-5, 5, 0.05)

y = sigma(x)
dy_simple = sigma_derivatived_simple(x)   # 簡化導函數公式的y值
dy_complex = sigma_derivatived_complex(x) # 未簡化導函數公式的y值

plt.plot(x,y, label='$\sigma(x)$')
plt.scatter(x, dy_simple, c='r', marker='o', label='$\sigma\'(x) simple$')
plt.scatter(x, dy_complex, c='b', marker='v', label='$\sigma\'(x) complex$')
plt.legend()

# plt.grid(axis='y', which='major', linestyle='-', color='b')
# plt.grid(axis='y', which='minor', linestyle='--', color='grey')
# plt.grid(axis='x', which='major', linestyle='-')
# plt.minorticks_on()

plt.show()