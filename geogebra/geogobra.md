## GeoGebra 的使用

- 常用函數
  - 獲取座標中的 x 值，`$ x(座標物件)`
  - 獲取座標中的 y 值，`$ x(座標物件)`
  - 設置某個物件的顏色，`$ SetColor(物件名, 顏色)`
  - 指定座標，`$ 變數名 = (x值, y值)`

- [api查詢](https://wiki.geogebra.org/en/Category:Commands)

## 常用操作

- 建立按鈕: 點擊上方slider的按鈕 > 選擇 button

- 開啟左側方程式面板
	- 打開右上角面板(帶函數符號的)圖示左側的選項
	- 選擇 "Algebra"

- 開啟網路範例
	- [範例](https://www.geogebra.org/m/dq4449tr#material/qztwmfsu)
	- 點擊右上角 > Open in App > 會在新網頁中開啟

## GeoGebra-Script 範例

- 範例，按下按鈕後，更新某個變數值
  ```
  RunTime = RunTime +1
  X_{cur} = X_{next}
  ```

- 範例，按下 reset 按鈕後，初始化變數值
  ```
  RunTime = 1
  X_{cur} = 0
  r = 0.1
  ```

- 範例，根據值變更物件顏色
  ```
  If(r<0,SetColor(r, "red"))
  If(r>=0,SetColor(r, "blue"))
  ```

- 範例，隱藏物件
  - 建立方程式 eq 物件
  - 建立 button
  - 建立變數 showEQ = false
  - 在 button > script > on-click-tab 中
    ```
    SetValue(showEQ, If(showEQ,false,true))
    ```
  - 在 eq 的配置中 > Advance > condition to object，輸入要綁定的變數，此處為 showEQ (若要反向綁定，!showEQ)
	
## ref

- [GeoGebra basic * 26](https://www.youtube.com/playlist?list=PLqZ0eZtMcAlugmcomSSvjPBfewVbX35L7)
- [GeoGebra Examples](https://www.youtube.com/playlist?list=PLqZ0eZtMcAltqU5rj_eW4DPzqz1MHxUiq)
- [Messing around in GeoGebra](https://www.youtube.com/playlist?list=PL6z-JSgFPRBQJbxvhvoLpgZHp2HSe8SnM)
- [GeoGebra Scripts](https://www.geogebra.org/m/dq4449tr)
- [學習 geogebra 系列影片@ y2b-Dane Ehlert](https://www.youtube.com/playlist?list=PL1Rai_9xlWWb-H3-8PsQ8OYA_EamE5iTY)

- Script
  - [References for Programmers](https://wiki.geogebra.org/en/References_for_Programmers)
  - [腳本指令查詢](https://wiki.geogebra.org/en/Scripting_Commands)
  - GeoGebra Javascript API
    - [official](https://wiki.geogebra.org/en/Reference:GeoGebra_Apps_API)
    - [on github](https://geogebra.github.io/integration/example-api.html)
