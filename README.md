## neural-network 學習紀錄

- [neural-network 基礎]((basic/00_basic-overview.md))
- [神經網路模型](model/00_model-overview.md)
- [範例代碼](examples/)

- [geogebra的使用](geogebra/geogobra.md)

- [神經網路需要的基礎數學](https://www.youtube.com/playlist?list=PLvcbYUQ5t0UG5v62E_QO7UihkfePakzNA)
  