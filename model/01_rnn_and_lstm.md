## RNN 和 LSTM
  
- RNN 適合與時序相關的數據，例如，
  
  <img src="../doc/rnn/rnn-use-case-1.png" width=50% height=auto>

  利用輸入或前一次都輸出結果，都可以用來預測下一步

  <img src="../doc/rnn/rnn-use-case-2.png" width=100% height=auto>

  RNN 示意圖

  <img src="../doc/rnn/rnn-use-case-3.png" width=800 height=auto>

  - 另一個 RNN 的使用範例
    一個具有以下三句話的童書
      ```
      Doug saw Jane.
      Jane saw Spot.
      Spot saw Doug.
      ```
    
    數據集為: `Doug`、`Jane`、`Spot`、`.`
    
    RNN 示意圖

    <img src="../doc/rnn/rnn-use-case-4.png" width=100% height=auto>

    RNN 的短期記憶造成的錯誤，
    - 例如，輸出` Doug saw Doug` 的結果
    - 例如，輸出 `Doug saw Jane saw Spot saw Doug.` 的結果
    - RNN 的短期記憶，使得在預測輸出時，無法考慮那些字詞已經出現過

    利用 LSTM 改善 RNN
    - 添加`忽略功能(ignore)`、`記憶功能(memory)`、`選擇輸出功能(selection)`
      - 忽略功能: 忽略不符合規則的字詞
      - 記憶功能: 記憶已經出現過的字詞
      - 選擇輸出功能: 選擇最符合的結果輸出 (不是所有已記憶的詞都要輸出)
  
    - 每一個添加的新功能，實際上都是一個RNN神經網路，利用神經網路實現對應的功能
  
    - 示意圖
  
      <img src="../doc/rnn/rnn-use-case-5.png" width=80% height=auto>

    - 執行範例

      <img src="../doc/rnn/rnn-use-case-6.png" width=80% height=auto>

      <img src="../doc/rnn/rnn-use-case-7.png" width=80% height=auto>

## Ref

- [RNN and LSTM](https://www.youtube.com/watch?v=WCUNPb-5EYI&list=PLVZqlMpoM6kaJX_2lLKjEhWI0NlqHfqzp&index=5)

- [利用LSTM預設股票價格](https://www.youtube.com/playlist?list=PLIukI3z0rdkxys524n-B0m6w5Ph_12Ukd)
  

