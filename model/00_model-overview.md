
## 神經網路模型

- 基礎神經網路模型
  - [dnn](./01_dnn.md)
  - [cnn](./01_cnn.md)
  - [rnn](./01_rnn.md)
  - [LSTM](./01_LSTM.md)

- Transformer 系列
  - [transformer模型概述](./02_transformer.md)
  - [gpt](./02_transformer-gpt.md)
  - [bert](./02_transformer-bert.md)
  
- Diffusion 系列
  - [diffusion模型概述](./03_diffusion.md)
  - [stable-diffusion](./03_diffusion-stablediffusion.md)
