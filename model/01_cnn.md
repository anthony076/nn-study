## CNN 基礎

- CNN 可解決 DNN 準確率無法提升的問題
  - [DNN 的問題](../model/01_dnn.md)
  - CNN 解決 DNN 模型過於複雜造成的問題，透過`卷積層`和`池化層`減少隱藏層神經元的數量(減少參數量)，從而簡化神經網路的模型
    - 卷積層: 用於提取主要特徵，用於將輸入影像轉換為特徵圖，藉此縮小輸入影像的大小
    - 池化層: 用於進一步縮小特徵圖的尺寸

- CNN 架構

  <img src="../doc/cnn/cnn.png" width=800 height=auto>

  - 池化層必定出現在卷積層後方
  - CNN 會經過多次的 卷積層 + 池化層

- CNN 的優缺點
  - 需要大量的训练数据才能获得较好的性能，如果数据集较小，容易出现过拟合的情况
  - CNN的特征提取是基于滤波器的，如果输入数据与训练数据不够相似，準確度就會下降
  - CNN 只提取特徵，不保留物體的位置訊息

- CNN 網路的改進

  - 殘差網絡（ResNet）：ResNet是一種通過跳躍連接（shortcut connection）解決深度卷積神經網絡退化的問題的模型。
    
    通過將前麵層的輸出直接添加到後麵層的輸入中，可以避免模型深度增加而出現性能下降的情況。

  - 稠密連接網絡（DenseNet）：DenseNet是一種通過將每一層的輸入與前麵所有層的輸出連接起來的方式增加網絡的參數共享，從而提高模型性能的模型。

  - Inception網絡：Inception網絡採用了不同大小的卷積核和池化操作，同時將它們的輸出合並在一起，從而能夠處理不同尺寸的特徵。

  - 轉移學習：轉移學習是一種通過將已經訓練好的模型的參數作為初始化參數，然後繼續在新的數據集上進行微調，從而提高模型性能的方法。

  - 卷積神經網絡自動機（CNN-AutoML）：CNN-AutoML是一種通過自動化神經網絡架構搜索和超參數優化，從而提高模型性能的方法。

## 卷積層(Convolutional layer)

- 在進入神經網路的隱藏層之前，透過卷積層`提取輸入影像的主要特徵`，而不是所有的影像都輸入到隱藏層中，
  透過只提取主要特徵的方式，實現減少輸入的目的
  
- 將輸入的圖像透過`卷積運算`，將輸入圖像轉換為尺寸較小的`特徵圖`

- 卷積運算，是透過 `輸入影像` 與 `卷積核(kernel)` 進行點積運算

  <font color=blue>什麼是卷積核</font>

  卷積核代表影像中的某一種特徵，例如，邊緣、紋理、形狀

  例如，要提取輸入影像的垂直邊緣特徵，則卷積核為 
  $$ \begin{matrix}
    1 & 0 & -1 \\
    1 & 0 & -1 \\
    1 & 0 & -1 \\
  \end{matrix}
  $$
  
  <font color=blue>卷積運算示意圖</font>
  <img src="../doc/cnn/convolution-operator.png" width=800 height=auto>

  卷積運算會沿著輸入影像的邊緣移動，每移動一次就進行點積運算，
  當移動的步長太小時，`有可能會重複覆蓋先前運算的區域`

- 卷積層的填充
  - 使用場景
    - 需要在卷積過程中保留更多的信息
    - 例如，不想讓特徵圖縮小太快
    - 例如，希望卷積操作之後的輸出特徵圖的尺寸與輸入圖像的尺寸相同等等

  - 填充方法
    - 對輸入影像的周圍，以固定的值填充一圈
      
      <img src="../doc/cnn/cnn-padding.png" width=800 height=auto>

      例如，一張 28x28 的輸入圖像，和 一個 3x3 的卷積核進行卷積運算，
      
      若不進行填充，卷積層輸出尺寸 = (28-3+1, 28-3+1) = (26 x 26)，水平和垂直各少了兩個像素

      為了使卷積層輸出的尺寸仍然維持 28*28，對輸入影像的周圍填充一圈0 (水平軸共增加兩個像素，垂直軸共增加兩個像素)，得到 (28+1 * 2, 28+1 * 2) = (30 * 30)

      30x30 的輸入影像和 3x3 的卷積核進行卷積運算，得到 28x28 的特徵圖

- 計算卷積層的輸出(特徵圖)尺寸
  - 若不考慮步長和填充，特徵圖尺寸 = `輸入影像長 - 卷積核長 + 1`
  - 若考慮步長和填充，特徵圖尺寸 = `((輸入影像長 - 卷積核長 + 2(填充長度)) / 步長) +1`

- 卷積核是透過訓練過程的學習得到的

  每個卷積核都是`由一組權重和偏置組成的`，在網絡訓練期間，卷積核的權重會通過反嚮傳播算法進行調整

  在建立CNN的神經網路時，通常不會指定卷積核的具體內容，只會指定卷積核的數量和大小等參數

  在神經網路的框架中，keras/tensorflow/pytorch，預設會使用 `Glorot均匀分布初始化方法`，
  根據輸入影像和卷積層輸出的尺寸，來決定合適的卷积核內容

  例如，卷積層的輸入為 28x28x3，輸出為 26x26x32，
  根據Glorot 均匀分布初始化方法，卷积核初始值的取值範圍在 -0.155 ~ 0.155之間，
  
  若取值為 0.1，則卷積核的內容為
  ```python
  [ 0.1, 0.1, 0.1, 
    0.1, 0.1, 0.1,
    0.1, 0.1, 0.1, ]
  ```

## 池化層(Pooling layer)
- 池化層是對卷積層的輸出進行下採樣(subsampling)，以減少特徵圖的尺寸，從而降低模型的複雜度和計算量。

- 池化層也是特徵的提取，但和卷積層不同的是，池化層將卷積層輸出分為不同的區域後，只獲取該區域內特徵的最大值，從而減少特徵圖的尺寸

  <img src="../doc/cnn/cnn-max-pooling.png" width=800 height=auto>

## 常用的卷積核

- 垂直边缘检测卷积核
  ```python
  [ -1, 0, 1, 
    -1, 0, 1,
    -1, 0, 1, ]
  ```

- 水平边缘检测卷积核
  ```python
  [ -1,-1,-1, 
     0, 0, 0,
     1, 1, 1, ]
  ```

- Sobel 卷积核：可以同时检测水平和垂直方向上的边缘
  ```python
  [ -1, 0, 1, 
    -2, 0, 2,
    -1, 0, 1, ]
  ```

- Laplacian 卷积核：可以检测图像中的高频部分，对于边缘检测效果较好
  ```python
  [ 0,  1, 0,
    1, -4, 1,
    0,  1, 0, ]
  ```

- 高斯模糊卷积核：可以模糊图像，减少图像中的噪声
  ```python
  [ 1/16, 1/8, 1/16,
    1/8,  1/4, 1/8,
    1/16, 1/8, 1/16, ]
  ```

## 常用的池化方法

- 最大池化（Max Pooling）: 快，但丟失次優特徵
  - 方法，在一個區域內選取`最大值`作為輸出
  - 優，可以保留局部最顯著的特徵，而且計算簡單，不易出現過擬合
  - 缺，可能會丟失一些次優的特徵，無法保留區域內的其他重要信息
  
- 平均池化（Average Pooling）: 降低噪聲，但容易失去細節
  - 方法，在一個區域內`計算平均值`作為輸出
  - 優，可以平滑特徵圖，減少噪聲幹擾
  - 缺，可能會丟失一些局部的明顯特徵，對於需要區分細節的任務不太適用

- L2池化（L2 Pooling）: 保留更多訊息，但容易失去細節，且計算量大
  - 方法，在一個區域內計算`L2範數（平方和再開根號）`作為輸出
  - 優，可以保留更多的信息，同時抑製過大的值，適用於一些要求更平滑的應用場景
  - 缺，計算量比較大，同時可能會丟失一些次優的特徵，對於一些需要區分細節的任務可能不太適用。
  
## 範例，以CNN實作MNIST手寫辨識

```python

# from https://ithelp.ithome.com.tw/articles/10288351

import numpy as np 

import tensorflow.keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential,load_model,model_from_json
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from tensorflow.keras.utils import to_categorical 

from PIL import Image

import matplotlib.pyplot as plt

# ==== 資料預處理 ====
# 讀取mnist的資料
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# 輸入給卷積層的數據，為 (長,寬,色彩)
# 色彩=1代表黑白，色彩=13代表彩色
x_train = x_train.reshape(60000, 28, 28, 1)
x_test = x_test.reshape(10000, 28, 28, 1)

# 透過 x_train[0] 可發現，可以個 pixel 的值介於 0-255 之間
#   在神經網路中數值越大收斂越慢，且會受到極端值的影響，使訓練效果不佳
#   因此將數值壓縮，將數值除255讓數值能夠壓縮在0~1之間
x_train = x_train/255
x_test = x_test/255

# 給圖片添加標籤(Label)，並將 Label 透過 one-hot-encoding 轉換為數值
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# ==== 建立神經網路 ====

# 神經網路實例
model = Sequential()

# 添加卷積層，卷積層的輸入為28*28*1，
#   kernel_size參數，卷積核尺寸，注意，卷積核是透過訓練學習得到，只需要配置大小，不需要配置內容
#   input_shape參數，輸入影像尺寸
#   padding參數，same，填充0 | valid，不填充，注意，填充時會使得卷積層的輸出尺寸與輸入相同
model.add(Conv2D(32, kernel_size = 3, input_shape = (28,28,1), padding="same", activation = 'relu'))

# 添加池化層
model.add(MaxPooling2D(pool_size = 2))

# 攤平池化層的輸出，將池化層的輸出轉換為一維
model.add(Flatten())

# 建立隱藏層(全連接層)
#   Dense()，用於建立密集連接層（Dense Layer，全連接層）
#   units參數，指定神經元的數量
#   input_dim參數，輸入的數量
model.add(Dense(16, activation = 'relu'))

# 建立輸出層
model.add(Dense(10, activation = 'softmax'))

# ==== 其他配置 ====

# 將神經網路和其他組件一起打包(compile)
# 宣告 loss-function 和 optimizer 和 metrics (指定要追蹤的指標)
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

history = model.fit(x_train, y_train,
  batch_size=128,
  epochs=10,
  verbose=1,
  validation_data=(x_test, y_test)
)

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# ==== 保存並還原cnn ====

# 儲存model(包含神經網路)
model.save('model.h5')

# 讀取整個model
model = load_model('model.h5')

#只儲存權重
model.save_weights('model_weights.h5')

# 還原cnn

# 重新定義沒有權重的神經網路
model = Sequential()
model.add(Conv2D(32, kernel_size = 3, input_shape = (28,28,1),padding="same", activation = 'relu'))
model.add(MaxPooling2D(pool_size = 2))
model.add(Flatten())
model.add(Dense(16, activation = 'relu'))
model.add(Dense(10, activation = 'softmax'))

# 讀取權重
model.load_weights('model_weights.h5')
```

## 參考

- [捲積神經網路](https://ithelp.ithome.com.tw/articles/10288351)
- [Convolutional Neural Networks (CNNs)](https://anhreynolds.com/blogs/cnn.html)