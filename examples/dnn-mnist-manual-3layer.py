# 範例，手寫一個3層的DNN神經網路

import os
import numpy as np
import gzip

# 讀取 MNIST 數據集
def load_data():
    data_dir = './data/mnist/'

    train_images_path = os.path.join(data_dir, 'train-images-idx3-ubyte.gz')
    train_labels_path = os.path.join(data_dir, 'train-labels-idx1-ubyte.gz')
    test_images_path = os.path.join(data_dir, 't10k-images-idx3-ubyte.gz')
    test_labels_path = os.path.join(data_dir, 't10k-labels-idx1-ubyte.gz')

    with gzip.open(train_images_path, 'rb') as f:
        train_images = np.frombuffer(f.read(), dtype=np.uint8, offset=16).reshape(-1, 784)
    with gzip.open(train_labels_path, 'rb') as f:
        train_labels = np.frombuffer(f.read(), dtype=np.uint8, offset=8)
    with gzip.open(test_images_path, 'rb') as f:
        test_images = np.frombuffer(f.read(), dtype=np.uint8, offset=16).reshape(-1, 784)
    with gzip.open(test_labels_path, 'rb') as f:
        test_labels = np.frombuffer(f.read(), dtype=np.uint8, offset=8)

    return train_images, train_labels, test_images, test_labels

# 激勵函數
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# 激勵函數的導數
def sigmoid_derivative(x):
    return sigmoid(x) * (1 - sigmoid(x))

# 輸出層的激勵函數
def softmax(x):
    exp_x = np.exp(x)
    return exp_x / np.sum(exp_x, axis=1, keepdims=True)

# 損失函數
def cross_entropy_loss(y, y_pred):
    return -np.sum(y * np.log(y_pred))

# 定義DNN
class NeuralNetwork:
    def __init__(self, input_size, hidden_size1, hidden_size2, hidden_size3, output_size):
        # 初始化輸入層權重
        self.W1 = np.random.randn(input_size, hidden_size1)
        self.b1 = np.zeros(hidden_size1)

        # 初始化第1層隱藏層權重
        self.W2 = np.random.randn(hidden_size1, hidden_size2)
        self.b2 = np.zeros(hidden_size2)

        # 初始化第2層隱藏層權重
        self.W3 = np.random.randn(hidden_size2, hidden_size3)
        self.b3 = np.zeros(hidden_size3)

        # 初始化第3層隱藏層權重
        self.W4 = np.random.randn(hidden_size3, output_size)
        self.b4 = np.zeros(output_size)

    # 前向傳播
    def forward(self, x):
        self.a1 = np.dot(x, self.W1) + self.b1
        self.z1 = sigmoid(a1)   # 輸入層輸出值

        self.a2 = np.dot(z1, self.W2) + self.b2
        self.z2 = sigmoid(a2)   # 第1層隱藏層輸出值

        self.a3 = np.dot(z2, self.W3) + self.b3
        self.z3 = sigmoid(a3)   # 第2層隱藏層輸出值

        self.a4 = np.dot(z3, self.W4) + self.b4
        y_hat = softmax(a4)     # 第3層隱藏層(輸出層)輸出值

        return y_hat

    # 反向傳播 
    def backward(self, x, y_hat, y_true):

        delta4 = y_hat - y_true     # 輸出層誤差
        delta3 = np.dot(delta4, self.W4.T) * sigmoid_derivative(self.a3)    # 第3層隱藏層的誤差
        delta2 = np.dot(delta3, self.W3.T) * sigmoid_derivative(self.a2)    # 第2層隱藏層的誤差
        delta1 = np.dot(delta2, self.W2.T) * sigmoid_derivative(self.a1)    # 第1層隱藏層的誤差

        # 更新权重和偏置
        self.W4 -= learning_rate * np.dot(z3.T, delta4)
        self.biases4 -= learning_rate * np.sum(delta4, axis=0)

        self.W3 -= learning_rate * np.dot(z2.T, delta3)
        self.biases3 -= learning_rate * np.sum(delta3, axis=0)

        self.W2 -= learning_rate * np.dot(z1.T, delta2)
        self.biases2 -= learning_rate * np.sum(delta2, axis=0)

        self.weights1 -= learning_rate * np.dot(x.T, delta1)
        self.biases1 -= learning_rate * np.sum(delta1, axis=0)

    def predict(self, x):
        y_hat = self.forward(x)
        return np.argmax(y_hat, axis=1)

    def evaluate(self, x, y):
        y_pred = self.predict(x)
        accuracy = np.mean(y_pred == y)
        return accuracy

    def train(self, x, y, learning_rate=0.1):
        # 前向传播
        a1 = np.dot(x, self.weights1) + self.biases1
        z1 = sigmoid(a1)

        a2 = np.dot(z1, self.W2) + self.biases2
        z2 = sigmoid(a2)

        a3 = np.dot(z2, self.W3) + self.biases3
        z3 = sigmoid(a3)

        a4 = np.dot(z3, self.W4) + self.biases4

if name == "__main__":
    # 加載數據集
    train_images, train_labels, test_images, test_labels = load_data()

    # 將圖像數據標準化，將像素值從[0, 255]縮放到[-1, 1]
    train_images = (train_images / 127.5) - 1
    test_images = (test_images / 127.5) - 1

    # 將label轉換為 one-hot-coding
    train_labels_one_hot = np.eye(10)[train_labels]
    test_labels_one_hot = np.eye(10)[test_labels]

    # 初始化神經網絡
    nn = NeuralNetwork(input_size=784, hidden_size1=512, hidden_size2=256, hidden_size3=128, output_size=10)

    # 訓練神經網絡
    epochs = 10
    batch_size = 64
    num_batches = len(train_images) // batch_size
    for epoch in range(epochs):
        for batch in range(num_batches):
            batch_start = batch * batch_size
            batch_end = (batch + 1) * batch_size
            x_batch = train_images[batch_start:batch_end]
            y_batch = train_labels_one_hot[batch_start:batch_end]
            y_hat = nn.forward(x_batch)

            nn.backward(x_batch, y_hat, y_batch)
        accuracy = nn.evaluate(test_images, test_labels)
        print(f'Epoch {epoch+1}/{epochs}, accuracy: {accuracy*100:.2f}%')