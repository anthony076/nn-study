
# 範例，以DNN實作MNIST手寫辨識
# from https://ithelp.ithome.com.tw/articles/10288343

import tensorflow.keras
import numpy as np 

# 包含著一些著名的資料集例如:nmist、IMDB影評
from tensorflow.keras.datasets import mnist

# 用於建構神經網路
from tensorflow.keras.models import Sequential

# 用於建構神經網路中的層
from tensorflow.keras.layers import Dense, Activation

# 資料正規化
from tensorflow.keras.utils import to_categorical

# ==== 資料預處理 ====
# 讀取mnist的資料
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# x_train.shape = (60000, 28, 28) = (60000張, 長28個向素, 寬28個向素)
# 將28x28(2維)的資料變成784(1維)的資料
x_train = x_train.reshape(60000, 784)
x_test = x_test.reshape(10000, 784)

# 透過 x_train[0] 可發現，可以個 pixel 的值介於 0-255 之間
#   在神經網路中數值越大收斂越慢，且會受到極端值的影響，使訓練效果不佳
#   因此將數值壓縮，將數值除255讓數值能夠壓縮在0~1之間
x_train = x_train/255
x_test = x_test/255

# 給圖片添加標籤(Label)，並將 Label 透過 one-hot-encoding 轉換為數值
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# ==== 建立神經網路 ====

# 建立模型
model = Sequential()

# 建立輸入層
#   Dense()，用於建立密集連接層（Dense Layer，全連接層）
#   units參數，指定神經元的數量
#   input_dim參數，輸入的數量
model.add(Dense(units=256,input_dim=784, activation='relu'))

# 隱藏層
model.add(Dense(units=128, activation='relu'))

# 輸出層
model.add(Dense(units=10,activation='softmax'))

# ==== 其他配置 ====

# 將神經網路和其他組件一起打包(compile)
# 宣告 loss-function 和 optimizer 和 metrics (指定要追蹤的指標)
model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])

# 開始訓練model 
#   - batch_size: 每次訓練要投餵多少資料
#   - epochs: 總共要訓練幾次
history = model.fit(x_train, y_train,
  batch_size=128,epochs=10,           
  verbose=1,                          # 輸出訊息
  validation_data=(x_test, y_test)    # 驗證準確性
)
