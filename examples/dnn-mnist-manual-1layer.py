# 範例，手寫一個單層的DNN神經網路

import numpy as np
import gzip
import struct

# 讀取 MNIST 數據集
def read_mnist(images_path, labels_path):
  with gzip.open(labels_path, 'rb') as labels_file, gzip.open(images_path, 'rb') as images_file:
      labels_file.read(8)
      images_file.read(16)

      labels = np.frombuffer(labels_file.read(), dtype=np.uint8)
      images = np.frombuffer(images_file.read(), dtype=np.uint8).reshape(len(labels), 28*28) / 255.

  return images, labels

# Sigmoid
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# 定義DNN
class NeuralNetwork:
  def __init__(self, input_size, hidden_size, output_size):

      # 初始化輸入層權重
      self.weights1 = np.random.randn(input_size, hidden_size)
      
      # 初始化隱藏層權重
      self.weights2 = np.random.randn(hidden_size, output_size)

  # 前向傳播      
  def forward(self, X):
      self.hidden = sigmoid(np.dot(X, self.weights1)) # 計算隱含層的輸出
      self.output = sigmoid(np.dot(self.hidden, self.weights2)) # 計算輸出層的輸出
      return self.output
  
  # 反向傳播 
  def backward(self, X, y, output):
      
      error = y - output  # 計算輸出層誤差
      delta_output = error * sigmoid(output) * (1 - sigmoid(output))  # 計算輸出層誤差的變化率

      error_hidden = delta_output.dot(self.weights2.T)  # 計算隱藏層的誤差
      delta_hidden = error_hidden * sigmoid(self.hidden) * (1 - sigmoid(self.hidden)) # 計算隱藏層誤差的變化率

      self.weights2 += self.hidden.T.dot(delta_output)  # 更新隱含層的權重矩陣
      self.weights1 += X.T.dot(delta_hidden)  # 更新輸入層的權重矩陣
      
  def train(self, X, y):
      # 進行前向傳播
      output = self.forward(X)
      # 進行反向傳播更新權重
      self.backward(X, y, output)

# 讀取數據
X_train, y_train = read_mnist('train-images-idx3-ubyte.gz', 'train-labels-idx1-ubyte.gz')
X_test, y_test = read_mnist('t10k-images-idx3-ubyte.gz', 't10k-labels-idx1-ubyte.gz')

# 初始化神經網絡
input_size = 784  # 定義輸入層的數據個數
hidden_size = 100 # 定義隱藏層的神經元個數
output_size = 10  # 定義輸出層的數據個數
nn = NeuralNetwork(input_size, hidden_size, output_size)

# 訓練神經網絡
epochs = 5
batch_size = 100
batch_count = len(X_train) // batch_size

for epoch in range(epochs):
  for batch_index in range(batch_count):
      batch_start = batch_index * batch_size
      batch_end = batch_start + batch_size
      X = X_train[batch_start:batch_end]
      y = np.zeros((batch_size, output_size))
      y[np.arange(batch_size), y_train[batch_start:batch_end]] = 1
      nn.train(X, y)

  # 輸出每個 epoch 的精度
  predictions = np.argmax(nn.forward(X_test), axis=1)
  accuracy = np.mean(predictions == y_test)
  print(f'Epoch {epoch+1} - Test Accuracy: {accuracy*100:.2f}%')