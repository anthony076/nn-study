## ai 相關應用

- 寫作類
  - [readwise](https://readwise.io/): 基於 GPT 的寫作軟體，需要付費
  - [cubox](https://help.cubox.pro/): 閱讀器，一站式信息收集和管理服務
  - [notion-ai](https://www.notion.so/product/ai): 基於 GPT 的寫作軟體

- chrome 插件類
  - [Glarity](https://chrome.google.com/webstore/detail/glarity-summary-for-googl/cmnlolelipjlhfkhpohphpedmkfbobjc): 
    自動為谷歌搜索結果及YouTube視頻生成摘要

- 語言學習類
  - [Duolingo Max](https://www.youtube.com/watch?v=F-oLLqILsAU)

- 影片剪輯類
  - [剪映專業版](https://www.capcut.cn/): 自動識別字幕 + 智能摳圖 (去除不必要的物件或場景)
  - [Video Summarization](https://smart-ai-transformations.cloudinary.com/): 將長視頻總結後，轉換為短視頻

- 圖片類
  - [pixelicious](https://www.pixelicious.xyz/): 將圖片進行像素化

  - [Lexica Art](https://lexica.art/): text2img，利用提示詞產生影像，可瀏覽其他作品並查看提示詞
  - [playgroundai](https://playgroundai.com/): text2img，利用提示詞產生影像，可瀏覽其他作品並查看提示詞

  - [imglarger](https://imglarger.com/): 圖片自動補全 + 畫質優化
  - [codeFormer](https://shangchenzhou.com/projects/CodeFormer/#method): 圖片自動補全 + 畫質優化，開源

  - [bigjpg](http://bigjpg.com): 畫質優化
  - [clipdrop](https://clipdrop.co/): 畫質優化

  - [blockade labs](https://www.blockadelabs.com/): text to 3d-object，利用提示詞建立3D模型，或添加物理動畫
  - [luma labs](https://lumalabs.ai/): 根據圖片或錄影，製作建立3D模型或製作環繞動畫，有 unreal 插件可直接導入
  

- 聲音類
  - [Enhance Speech](https://podcast.adobe.com/enhance): 去噪 + 人聲加強 + 增強音質

## 推薦網站

- [GPT學習寶典](https://gpt.candobear.com/)
  - prompt大全
  - gpt工具箱
  - 學習資料

- [使用GeoGebra對神經網路進行可視化](https://www.youtube.com/watch?v=mODrZ2o3qzE)