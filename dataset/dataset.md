## dataset 數據處理

- 對於屬於非數值的數據，需要轉換為數值才能進行訓練，稱為 Categorical Feature
  - 例如，Sunday、Monday、Tuesday、Wednesday、Thursday、Friday、Saturday
  - 常見處理 Categorical Feature 的方法
    - 方法1，Drop Categorical Feature: 直接把所有Categorical Feature丟棄，只拿Numerical的部分進行訓練跟預測
    - 方法2，Ordinal Encoding(或稱Label Encoding): 把Categorical Feature裡面的類別，轉換成0~N-1的數值。
    - 方法3，One Hot Encoding: 新增N個Columns，每個Columns利用0與1表示原本的Feature是不是這個類別
    - 例如，

      非數值的數據
      |     | boro      | vegan |
      | --- | --------- | ----- |
      | 0   | Manhattan | No    |
      | 1   | Queens    | No    |
      | 2   | Manhattan | No    |
      | 3   | Brooklyn  | Yes   |
      | 4   | Brooklyn  | Yes   |
      | 5   | Bronx     | No    |

      <font color="blue">使用 Ordinal Encoding 轉換為數值</font>
      - 將boro欄中的數值進行編碼，Bronx=0，Brooklyn=1，Manhattan=2，Queens=3

      |     | boro      | boro_index | vegan |
      | --- | --------- | ---------- | ----- |
      | 0   | Manhattan | 2          | No    |
      | 1   | Queens    | 3          | No    |
      | 2   | Manhattan | 2          | No    |
      | 3   | Brooklyn  | 1          | Yes   |
      | 4   | Brooklyn  | 1          | Yes   |
      | 5   | Bronx     | 0          | No    |
      
      <font color="blue">使用 One Hot Encoding 轉換為數值</font>
      - 將boro欄中的數值轉換為新的 Columns

      |     | vegan | boro_Bronx | boro_Brooklyn | boro_Manhattan | boro_Queens |
      | --- | ----- | ---------- | ------------- | -------------- | ----------- |
      | 0   | No    | 0          | 0             | 1              | 0           |
      | 1   | No    | 0          | 0             | 0              | 1           |
      | 2   | No    | 0          | 0             | 1              | 0           |
      | 3   | Yes   | 0          | 1             | 0              | 0           |
      | 4   | Yes   | 0          | 1             | 0              | 0           |
      | 5   | No    | 1          | 0             | 0              | 0           |

- 為什麼不建議使用 One-Hot-Encoding
  - 缺點1，會增加數據的維度，每一個新增的 Columns 都是一個新的維度，會增加 feature 的數量
    - 大維度會造成模型不容易收斂(Curse Of Dimensionality)
    - 大維度會使得距離函數變的無意義
    - 大維度會使用訓練需要的資源上升，訓練時間變長
  
  - 缺點2，數據中出現大量的0，容易算不出梯度
  - 缺點3，容易出現大量的冗餘資訊

- One-Hot-Encoding 的替代方法
  - 方法1，Frequency-Encoding
    
    把Categorical Feature裡面每個類別出現的數量，當成他的數值
  
  - 方法2，Target-Encoding(或稱mean encoding)
    
    把同樣類別的資料對應「Target值」全部抓出來，並且將這些Target值的平均當成Encoding的值

    注意，在算Target Encoding的時候，應該要去除忽略極值，
    
    例如，mean(1, 1, 1, 1, 100)，會變成20.8，跟實際的數值特性不相同

    | id  | 顏色 | 價格 |
    | --- | ---- | ---- |
    | 0   | 紅色 | 50   |
    | 1   | 藍色 | 40   |
    | 2   | 紅色 | 60   |
    | 3   | 黃色 | 80   |
    | 4   | 紅色 | 70   |
    | 5   | 黃色 | 90   |

    - 紅色平均 = mean(50, 60, 70) = 60
    - 藍色平均 = mean(40) = 40
    - 黃色平均 = mean(80, 90) = 85

    可得到以下表格

    | id  | 顏色 | 價格 | Target-Encoding |
    | --- | ---- | ---- | --------------- |
    | 0   | 紅色 | 50   | 60              |
    | 1   | 藍色 | 40   | 40              |
    | 2   | 紅色 | 60   | 60              |
    | 3   | 黃色 | 80   | 85              |
    | 4   | 紅色 | 70   | 60              |
    | 5   | 黃色 | 90   | 85              |



## ref

- [不要再做One Hot Encoding](https://axk51013.medium.com/%E4%B8%8D%E8%A6%81%E5%86%8D%E5%81%9Aone-hot-encoding-b5126d3f8a63)