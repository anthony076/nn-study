
## 函數

- 函數是從輸入映射到輸出的轉換過程，
  - `x` : 輸入，或稱`引數`，引發轉換過程的起始要素
  - `f` : 函數，為輸入轉換為輸出的關係
  - `f(x)` : 將x輸入f後的輸出值，或稱`因變數`，因輸入而改變的數
  - 三者的依賴關係為，$x \rightarrow f \rightarrow f(x)$
    - 函數f，依賴輸入x
    - 輸出f(x)，依賴函數f

- 透過`四則運算`產生新函數
  - $f(x) = x^2$
  - $g(x) = e^x$
  - 新函數 $n(x) = f(x) + g(x) = x^2 + e^2$

- 透過`複合函數`產生新函數
  - $f(x) = e^x$
  - $g(x) = x^2$
  - 新函數 $n(x) = (e^x)^2$
  - 三者的依賴關係，$x \rightarrow f \rightarrow f(x) \rightarrow g \rightarrow g(x)$，
    - 將輸入x帶入f函數中，得到f(x)值
    - 將f(x)帶入g函數中，得到g(x)值

## 函數、數列、極限、連續性

- 函數的極限值

  以 $f(x)=1 + \frac{1}{1+e^{-x}}$ 的函數為例

  <img src="./pics/limit-of-function.png" width=70% height=auto>

  極限，函數中當`輸入x趨近於某個量`時，`輸出y不會增加或減少`，而是`趨近於某個量`的行為，利用極限來描述函數的極限值

- 數列的極限

  - 數列，指一列有序符合某個映射關係的數值集合

  - 數列也是函數
    
    若數列 $\{a_n\}$ 為
    $\{ {1, \frac{1}{2}}, \frac{1}{3}, \cdots, \frac{1}{n}\}$

    - n，為傳遞給數列的自然引數(`輸入`)
    - 數列$a_n$，為自然引數映射到數值的集合(`輸出的集合`)
    - $\frac{1}{n}$，映射關係(`函數`)
  
  - 極限的表示方式

    若數列 $\{a_n\}$ 為
    $\{ {1, \frac{1}{2}}, \frac{1}{3}, \cdots, \frac{1}{n}\}$

    - 數列$a_n$的值是不斷收斂至0的
    
    - 只要n的數值夠大，函數值($\frac{1}{n}$)和極限值(0)的差值($\epsilon$)必定越來越小

    - 極限的表示方式 : 
      
      $$\lim_{n \to \infty} \frac{1}{n} = 0$$

      表示，當$n$趨近於正無窮時，函數 $\frac{1}{n}$ 的極限值 = 0

  - 一個數列可能沒有極限值，但如果有，極限必定是唯一的

- 函數的連續性 (單點位置的極限)

  - <font color=red>注意，函數值不一定等於極限值</font>，當函數在 $x=x_0$ 連續時，極限值 = 函數值

  - 如何判斷 $x=x_0$ 處是否連續
    - 條件1，$f(x_0)$ 有定義(有值)，代表函數值存在
  
    - 條件2，`左逼近的極限值` == `右逼近的極限值`，代表極限值存在

      $$\lim_{x \to x_0^-}{f(x)} = \lim_{x \to x_0^+}{f(x)} = f(x_0)$$

    - 條件3，`極限值 == 函數值`，$\lim_{x \to x_0}{f(x)} = f(x_0)$

  - 不連續函數曲線和連續函數曲線的差異
  
    <img src="pics/compare-continus-and-discontinus.png" width=70% height=auto>

  - 注意，<font color=blue>極限值不一定等於函數值，只有在連續的點，極限值才等於函數值</font>
    
    `極限值`表示當x靠近$x_0$時，函數在$x_0$附近的`趨勢`，而`函數值`則是在具體給定的 x 值處的`準確值`。

    - 如果某個函數在某個點 x=a 處`連續`，那麼在這一點的極限值通常等於函數值，即 
    
    $$\lim_{x \to x_a} {f(x)} = f(x)$$

    - 如果函數在 x=a 處`不連續`，或者存在間斷點，那麼函數值和極限值可以不同，即 
    
    $$\lim_{x \to x_a} {f(x)} \neq f(x)$$

  - 範例，$f(x) = \frac{x^2-4}{x-2}， x \neq 2$，在 $x=2$ 是否連續 ?

    - $$\lim_{x=2} \frac{x^2-4}{x-2} = \lim_{x=2} (x+2) = 4$$
      表示 $x=2$處，極限存在
    
    - $x\neq2$，因此，$f(2)$ 未定義，

    - $$\begin{cases}
      \lim_{x=2} \frac{x^2-4}{x-2} = 4 & \text{(1)} \\
      f(2) = 未定義 & \text{(2)} \\
      \end{cases}$$
      
      $\therefore$ 在 $x=2$ 不連續

  - 範例，$f(x) = x^2$，在 $x=3$ 是否連續 ?

    - $$\lim_{x \to 3^-} (x^2) = 9，左逼近的極限值為9
    - $\lim_{x \to 3^+} (x^2) = 9$，右逼近的極限值為9
    - $f(3) = 9$
    - $$
        \begin{cases}
        \lim_{x \to 3^-} (x^2) = \lim_{x \to 3^+} (x^2) = 9，\text{極限存在} \\
        \lim_{x \to 3} (x^2) = f(3) = 9，\text{極限值=函數值}
        \end{cases}
      $$
  
      $\therefore x=3$，是連續的

## 導數和微分

- 比較: 極限 $\rightarrow$ 導數 $\rightarrow$ 微分

  |                | 定義                                                    |
  | -------------- | ------------------------------------------------------- |
  | 單點極限       | $x_0$連續時，當x逼近$x_0$時，極限值=$f(x_a)$            |
  | 區間極限(導數) | 在x區間中($\Delta x$)，f(x)的變化量($\Delta y$)         |
  | 微分           | 利用導函數，計算導函數在非常小的x區間內(dx)的變化率(dy) |

- 導數
  - 主要用於求`斜率`

  - 利用極限(limit)進行計算

  - 為什麼稱為導數

    指在無限小的X區間內(dx)，曲線函數值(y)的變化率($\Delta y = y_1-y_0$)，
    若將函數上的兩個點連線，可以得到一段直線，該直線可以`指明函數值y變化的方向`

    由於可以`導引`我們了解函數在不同x位置上，y變化率或斜率，因此稱為`導數`

    用來`計算導數的函數`稱為導函數，導函數所得的值稱為`導數`，
    
    導函數是透過`計算極限`所得，和原函數不同，利用導函數與原函數可以進行區隔

  - 定義

    $$\begin{aligned}
    導函數 f^\prime(x_0)
    & = \lim_{\Delta{x \to 0}} {\frac{\Delta{y}}{\Delta{x}}} \\
    & = \lim_{\Delta{x \to 0}} {\frac{f(x_0 + \Delta{x}) - f(x)}{\Delta{x}}}
    \end{aligned}$$

- 微分
  - `導數`用於求斜率，`導函數`用於求微分

    微分實際上就是對導函數進行運算，因此，導函數，$f'(x)$，也稱為`微分`

  - 計算`導函數`在微小的x區間內(dx)，造成的微小變化率(dy)$
    
    對任何一個可微(可導)的位置$x_0$，都可以在$x_0$的位置`畫一條導函數的曲線逼近原函數曲線`，
    由於該位置十分逼近$x_0$，因此可以透過導函數計算$x_0$位置的變化率

    注意，微分繪出的導函數曲線，只有在$x_0$的位置逼近$f(x_0)$的位置，
    在遠離$x_0$的位置，導函數曲線與原曲線相距太遠，用來計算$\Delta y$時會產生不同程度的誤差

  - 定義

    ${\rm d}y = f'(x) \cdot {\rm d} x$，利用`導函數`和`x變化量`的乘積

    或寫成

    $\frac{\rm d}{{\rm d}x} f(x) = 0$

- 範例，計算 $f(x)=x^2$，在 x=0，1，3處的導數

  $f'(x) = 2x$
  
  $f'(0) = \frac{dy}{dx} = 0$，得到 $dy = 0dx$

  $f'(1) = 2(1) = 2$，得到 $dy = 2dx$，
  代表x有微小增量時，y的變化率是x的2倍
  
  $f'(3) = 2(3) = 6$，得到 $dy = 6dx$，
  代表x有微小增量時，y的變化率是x的6倍

## 微分(導函數)運算

- 注意，微分公式都是特定類型的函數，經由`導函數`推導後，直接利用推導後的結果，快速進行運算，
  沒有公式時，也可以利用以下的導函數進行計算
  
  $$\begin{aligned}
    導函數 f^\prime(x_0)
    & = \lim_{\Delta{x \to 0}} {\frac{\Delta{y}}{\Delta{x}}} \\
    & = \lim_{\Delta{x \to 0}} {\frac{f(x_0 + \Delta{x}) - f(x)}{\Delta{x}}}
  \end{aligned}$$


- 常見公式

  <img src="pics/calculus-formuler.png" width=800 height=auto>

  - 加減法
    
    $(f(x) + g(x))' = f'(x) + g'(x)$

  - 係數

    $( a \cdot f(x))' = a \cdot f'(x)$

  - 乘法

    $(f(x)\cdot g(x))' = f'(x) \cdot g(x) + f(x) \cdot g'(x)$

  - 除法

    $(\frac{f(x)}{g(x)})' = \frac{f'(x) \cdot g(x) - f(x) \cdot g'(x)}{g(x)^2}$

  - 連鎖律 (chain rule)

    $\frac{df(x)}{dx} = \frac{df(x)}{dg(x)} \cdot \frac{dg(x)}{dx}$

    $f(g(x))' = f'(g(x)) \cdot g'(x)$

- 多項式微分證明

  $\begin{aligned}
  f(x) = x^2
  \end{aligned}$

  $\begin{aligned}
  導函數 f^\prime(x)
  & = \lim_{\Delta{x \to 0}} {\frac{f(x + \Delta{x}) - f(x)}{\Delta{x}}} \\
  & = \lim_{\Delta{x \to 0}} {\frac{(x + \Delta{x})^2 - x^2}{\Delta{x}}} \\
  & = \lim_{\Delta{x \to 0}} {\frac{(x^2 + 2x\Delta{x}+\Delta{x^2})-x^2}{\Delta{x}}} \\
  & = \lim_{\Delta{x \to 0}} {\frac{\Delta{x}(2x+\Delta{x})}{\Delta{x}}} \\
  & = \lim_{\Delta{x \to 0}} 2x+\Delta{x}
  \because \Delta{x} \approx0 \\
  & = 2x
  \end{aligned}$

## 利用 python 繪製微分曲線
- 注意，微分方程可以使用 sympy 來取得

  要查看 sympy 的推導過程，
    - 使用 `hint參數`，指定求解方法
    - 使用 `explain參數`，印出推導過程的詳細步驟
  
  例如，

  ```python
  import sympy as sp

  # 定義符號變數
  x = sp.Symbol('x')
  y = sp.Function('y')

  # 定義微分方程
  eq = sp.Eq(y(x).diff(x) + y(x), x + 1)

  # 求解微分方程，並指定求解方法和列印推導過程
  sol = sp.dsolve(eq, y(x), hint='separable', explain=True)

  # 列印推導過程和最後的方程式
  print(sp.latex(sol))
  ```
- 範例，若 $\sigma(x)=\frac{1}{1+e^{-x}}$，繪製 $\sigma'(x)$的曲線

  $$\begin{aligned}
  & \sigma'(x) = (\frac{1}{1+e^{-x}})' \\
  
  & = \frac{1'\times (1+e^{-x}) - 1 \times (1+e^{-x})'}{(1+e^{-x})^2} \\

  & = \frac{-(1+e^{-x})'}{(1+e^{-x})^2} 
  = \frac{-0-(e^{-x})'}{(1+e^{-x})^2}
  = \frac{e^{-x}}{(1+e^{-x})^2} \\

  & = \frac{(1+e^{-x})-1}{(1+e^{-x})^2}
  = \frac{(1+e^{-x})}{(1+e^{-x})^2} - \frac{1}{(1+e^{-x})^2}
  = \frac{1}{1+e^{-x}} - \frac{1}{(1+e^{-x})^2} \\
  
  & = \sigma(x)(1-\sigma(x))
  \end{aligned}$$

  ```python
  import numpy as np
  import matplotlib.pyplot as plt

  def sigma(x):
    return 1 / (1 + np.exp(-x))

  def sigma_derivatived_simple(x):
    # 導函數，公式已簡化的版本
    return sigma(x) * (1-sigma(x))
    
  def sigma_derivatived_complex(x):
    # 導函數，公式未簡化的版本
    return np.exp(-x) / np.square((1+np.exp(-x)))

  x = np.arange(-5, 5, 0.05)

  y = sigma(x)
  dy_simple = sigma_derivatived_simple(x)   # 簡化導函數公式的y值
  dy_complex = sigma_derivatived_complex(x) # 未簡化導函數公式的y值

  plt.plot(x,y, label='$\sigma(x)$')
  plt.scatter(x, dy_simple, c='r', marker='o', label='$\sigma\'(x) simple$')
  plt.scatter(x, dy_complex, c='b', marker='v', label='$\sigma\'(x) complex$')
  plt.legend()

  plt.grid(axis='y', which='major', linestyle='-', color='b')
  plt.grid(axis='y', which='minor', linestyle='--', color='grey')
  plt.grid(axis='x', which='major', linestyle='-')
  plt.minorticks_on()

  plt.show()
  ```

  <img src="pics/sigma-example.png" width=70% height=auto>

## Ref

- 微積分
  - [線上微積分基礎課程 @ 中華大學李柏堅](http://aca.cust.edu.tw/online/calculusI/02/02_01_01.html)
  - [導數與微分到底有什麼區別](https://zhuanlan.zhihu.com/p/145620564)
  - [導數、微分、積分之間的區別與聯繫](https://kknews.cc/zh-tw/education/ljkxnn2.html#google_vignette)
  - [微分和積分的簡介](https://www.stat.nuk.edu.tw/cbme/math/calculus/cal/Main207.pdf)
  - [常用的微分和積分公式](https://www.zetria.org/view.php?subj=physics&chap=utjuzkv06v)

- 利用 sympy 解數學
  - [SymPy：使用 Python 幫你導煩人的數學公式](https://blog.gtwang.org/useful-tools/sympy-python-library-for-symbolic-mathematics/3/)
  - [利用sympy計算微分方程](https://zhuanlan.zhihu.com/p/79195356)
  - [利用sympy推導公式](https://zhuanlan.zhihu.com/p/491831883)