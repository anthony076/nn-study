## 定理 | 引理 | 推論 | 命題 的區別

- 命題(proposition)
  - 命題是陳述語句，它要么是真的，要么是假的，但不可能既真又假。
  - 命題可以是簡單的，也可以是複合的，即由多個命題組成的複雜語句。

- 引理(lemma)
  - 引理是一個輔助性的結果，通常是用來證明更大的定理
  - 引理通常比定理小，但在證明其他結果時非常有用

- 定理(theorem)
  - 定理是一個具有重要意義的數學陳述，它通常需要嚴格的證明
  - 定理是對某種問題或命題的正式而全面的陳述，並且被接受為真

- 推論(corollary)
  - 推論是一個從先前已經被證明的定理或結果中直接得出的結果
  - 推論通常是相對較容易從先前的定理中推導出的，而不需要像證明定理那樣的詳細步驟
  
- `命題`是基本的真假語句，`引理`是證明定理時使用的輔助性結果，`定理`是具有重要意義的全面結果，而`推論`是直接從已經證明的定理中得出的結果，

  $$命題 \rightarrow 引理 \rightarrow 定理 \rightarrow 推論$$

## 常用的數學符號

- ∈，屬於，例如，x ∈ R，x屬於實數，在 Latex 中為 `\in`
- ∀，對所有或任取，例如，∀x ∈ R，對所有x 都屬於實數，在 Latex 中為 `\forall`
- ∃，存在至少一個，例如，∃ x:P(x)，表示有至少一個x存在，使得P(x)成立，在 Latex 中為 `\exist`
- ∃!，存在且唯一，，在 Latex 中為 `\nexists`
- s.t，使得，such that
- C，複數集合，complex-set，在 Latex 中為 `\Complex` 或 `\mathbb{C}`
- R，實數集合，real-set，在 Latex 中為 `\reals` 或 `\mathbb{R}`
- N，自然數集合，nature-set，在 Latex 中為 `\mathbb{N}`
- $R^n$，$R^n$ 空間，例如，$R^2$ = R x R，指實數軸 cross 實數軸，代表二維空間
- ∪，聯集，在 Latex 中為 `\bigcup`
- ∩，交集，在 Latex 中為 `\bigcap`
- 子集(subset)
  - ⊆，子集(包含或等於)，例如，$A \subseteq B$，代表A是B的子集，`A和B有可能相等`，代表兩個集合具有相同的元素個數，
    在 Latex 中為 `\subseteq`

  - ⊂，子集(僅包含，稱真子集)，例如，$A \subset B$，代表A是B的子集，A的元素個數必定小於B，A、B不能相等，
    在 Latex 中為 `\subset`

- iff，若且為若 (if and only if)

## 什麼是域(Field)

- 域是代數結構中的一種，不同的代數結構的定義和性質有所不同
  - 群和環是另外兩種代數結構
  - 例，群不一定需要滿足乘法的交換律
  - 例，環不要求乘法逆元素的存在，也不要求乘法的交換律
  
- 在抽象代數中，域是一個具有兩種基本運算（加法和乘法）的代數結構，滿足一定的性質。
  - 一個域是一個包含了加法單位元（零元素）和乘法單位元（1元素）的集合，並且滿足一係列性質，如結合律、分配律等。
  - 常見的域有理數域、實數域、複數域

## 什麼是封閉性原則

- 以加法的封閉性為例

  加法封閉性是指在一個數學結構中，對於任意兩個元素進行加法運算後，運算後的值仍然屬於該結構。
  代表運算後的結果，仍然封閉在該結構之中，並不會跳出該結構定義的範圍內，因此，稱為封閉性原則

  例如，有的奇數集合（包括正奇數和負奇數），即 $\{ \cdots, -3, -1, 1, 3, 5, \cdots\}$

  若選擇兩個奇數，比如 1 和 3，它們屬於奇數集合。但對它們進行加法運算時，1+3 = 4，
  4 不是奇數，因此不屬於奇數集合。因此，此數學結構的加法運算不符合封閉性

  在運算中`跳出代數結構的範圍`，就會`失去代數結構的性質`，封閉性原則可以更好的定義可操作運算的`限製範圍`
