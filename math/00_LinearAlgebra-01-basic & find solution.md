## 矩陣基礎

- 矩陣的三種表示方式

  $A_{mn} = \begin{bmatrix} 
  a_{11} & a_{12} & \cdots & a_{1n} \\
  a_{21} & a_{22} & \cdots & a_{2n} \\
  a_{m1} & a_{m2} & \cdots & a_{mn} \\
  \end{bmatrix}$  一般表示方式

  = $\begin{bmatrix} 
  a_{1,:} \\
  a_{2,:} \\
  \vdots \\
  a_{m,:} \\
  \end{bmatrix}$  只標記`行索引(m)`，列索引以通用符號 `:` 取代

  = $\begin{bmatrix} 
  a_{:,1} & a_{:,2} & a_{:,n} \\
  \end{bmatrix}$  只標記`列索引(n)`，行索引以通用符號 `:` 取代

- 矩陣可用於線性方程式的求解，線性方程式指的是輸入和輸出的關係是線性的

- 矩陣可以用於求解線性方程式，但更多是用於研究多維度的性質，線性方程式是多維度的一個簡化

## 矩陣的計算

- 矩陣的運算分為兩種形式
  - 形式1，逐元素計算: 例如，加減乘除，結果矩陣形狀不變
    - 由多個陣列的`對應元素`進行`加減乘除`的操作，
      操作後得到與操作的陣列形狀相同的結果陣列

    - 逐元素計算的Hadamard乘積(或稱Schur乘積)

  - 形式2，累積計算: 例如，最大值/最小值/平均值，結果矩陣形狀縮減

- 矩陣相乘

  - 若矩陣A為 m x n 的矩陣，矩陣B為 n x p 的矩陣，則矩陣相乘AB會得到 m x p 大小的矩陣
  - 在矩陣相乘中，矩陣A的column數必須等於矩陣B的row數(都是n)，兩個矩陣才能相乘
  - AB的結果矩陣中，任意位置的結果可表示為 

    $\begin{aligned}
      [AB]_{i,j} 
      & = 矩陣A的第i個row，和矩陣B的第j個column中，n個元素相乘相加 \\
      & = A_{i1}B_{1j} + A_{i2}B_{2j} + \cdots + A_{in}B_{nj} 
      = \sum_{r=1}^n A_{ir}B_{ri}
    \end{aligned}$

    以下圖為例，
      - $AB_{12} = a_{11}b_{12} + a_{12}b_{22}$
      - $AB_{33} = a_{31}b_{13} + a_{32}b_{23}$
    <br><br>
    <img src="./pics/matrix-multiplication.png" width=50% height=auto>
  
  - 範例，
    $\begin{aligned}
      \begin{bmatrix}   %item1
        1 & 0 & 2 \\
        -1 & 3 & 1 \\
      \end{bmatrix} \times
      \begin{bmatrix}   %item2
        3 & 1 \\
        2 & 1 \\
        1 & 0
      \end{bmatrix} = 
      \begin{bmatrix}   %item3
        (1\times3 + 0\times2 + 2\times1) & (1\times1 + 0\times1 + 2\times0) \\
        (-1\times3 + 3\times2 + 1\times1) & (-1\times1 + 3\times1 + 1\times0) \\
      \end{bmatrix} = 
      \begin{bmatrix}   %item4
        5 & 1 \\
        4 & 2 \\
      \end{bmatrix}
    \end{aligned}$


- 範例，兩個向量相乘，可以視為是兩個一維矩陣相乘
  
  若有向量$A = [a_1, a_2, \cdots, a_m]$，向量$B = [b_1, b_2, \cdots, b_m ]$，
  計算AB

  $\begin{aligned}
    AB 
    & = AB^T \qquad 向量相乘時，需要\color{blue}{將後面的向量轉換為列向量}後才能相乘 \\
    & = [a_1, a_2, \cdots, a_m]
    \begin{bmatrix}
      b_1 \\ b_2 \\ \vdots \\ b_m
    \end{bmatrix} \\
    & = a_1b_1 + a_2b_2 + \cdots +a_mb_m \\
    & = \sum_{i=1}^m a_ib_i
  \end{aligned}$

- 範例，以Hadamard乘積為例
  $$\begin{bmatrix}
  \color{blue}{a_{11}} & 
  \color{green}{a_{12}} & 
  \color{red}{a_{13}} \\
  a_{21} & a_{22} & a_{23} \\
  a_{31} & a_{32} & a_{33} \\
  \end{bmatrix} 

  \times

  \begin{bmatrix}
  \color{blue}{b_{11}} & 
  \color{green}{b_{12}} & 
  \color{red}{b_{13}} \\
  b_{21} & b_{22} & b_{23} \\
  b_{31} & b_{32} & b_{33} \\
  \end{bmatrix}$$

  $$ = \begin{bmatrix}
  \color{blue}{a_{11} \times b_{11}} & 
  \color{green}{a_{12} \times b_{12}} & 
  \color{red}{a_{13} \times b_{13}} \\
  a_{21} \times b_{21} & 
  a_{22} \times b_{22} & 
  a_{23} \times b_{23} \\
  a_{31} \times b_{31} & 
  a_{32} \times b_{32} & 
  a_{33} \times b_{33} \\  
  \end{bmatrix}
  $$

  $$ = \begin{bmatrix}
  \color{blue}{a_{ij} \times b_{ij}} & 
  \color{green}{a_{ij+1} \times b_{ij+1}} & 
  \color{red}{a_{ij+2} \times b_{ij+2}} \\
  a_{i+1j} \times b_{i+1j} & \cdots & \cdots \\
  a_{i+2j} \times b_{i+2j} & \cdots & \cdots \\
  \end{bmatrix}
  $$

## 矩陣的性質

- 矩陣乘法沒有交換率，$A_{2 \times 3} \times C_{3 \times 2} \neq C_{3 \times 2} \times A_{2 \times 3}$

  $$A_{2 \times 3} \times C_{3 \times 2} = AC_{2 \times 2} \\ 
  C_{3 \times 2} \times C_{2 \times 3} = CA_{3 \times 3}$$

- 若 矩陣A x 矩陣B = 0，不能得到 A 或 B 至少有一個為零矩陣的結論
  - 在一般代數中，AB = 0，則 A=0 或 B=0
  - 在矩陣中，AB=0，，則 A=0 或 B=0 不成立

    $\begin{aligned}
    & A = \begin{bmatrix}
    0 & 1 \\
    0 & 1 \\ 
    \end{bmatrix}，且 
    B = \begin{bmatrix}
    0 & 1 \\
    0 & 1 \\ 
    \end{bmatrix}
    \end{aligned}
    $

    則 

    $AB = \begin{bmatrix} 
    0 & 0 \\
    0 & 0 \\ 
    \end{bmatrix} 為零矩陣，但 A \neq 零矩陣，B \neq 零矩陣$
  
- 矩陣乘法沒有消去率
  - 在一般代數中，若 AB = AC，A 可以消去，則 $B=C$
  - 在矩陣中，若 AB = AC，A 不能消去，因此，$B \neq C$
  
    若
    $\begin{aligned}
    & A = \begin{bmatrix}
    0 & 1 \\
    0 & 1 \\ 
    \end{bmatrix}，
    B = \begin{bmatrix}
    1 & 1 \\
    0 & 0 \\ 
    \end{bmatrix}，
    C = \begin{bmatrix}
    3 & 8 \\
    0 & 1 \\ 
    \end{bmatrix}
    \end{aligned}$

    $\begin{aligned}
    AB = \begin{bmatrix}
    0 & 0 \\
    0 & 0 \\ 
    \end{bmatrix} = AC，但 B \neq C
    \end{aligned}$

- 若 IA = AI = A，則 I 為單位矩陣

  $\begin{aligned}
  I = \begin{bmatrix}
  1 & 0 \\
  0 & 1 \\ 
  \end{bmatrix} 或
  I = \begin{bmatrix}
  1 & 0 & 0 \\
  0 & 1 & 0 \\ 
  0 & 0 & 1 \\ 
  \end{bmatrix}
  \end{aligned}$

## 特殊矩陣

- 零矩陣
  > 所有元素都為零的矩陣

- 方塊矩陣
  > 行數與列數相同的矩陣稱為方塊矩陣，簡稱方陣

- 單位矩陣
  > 也被稱為恆等矩陣，指方陣的主對角線上的元素全為1，而其他元素均為0。
  
  > 單位矩陣乘以任何矩陣都會保持原矩陣不變

- 對稱矩陣
  - 滿足 $A^T = A$，對稱矩陣通常也是方陣
  - 特性
    - $(A+\alpha \cdot B)^T = A^T + \alpha \cdot B^T$

- 逆矩陣(反矩陣)

  - 對於一個方陣 A，存在一個矩陣 B，使得矩陣 A 與矩陣 B 相乘等於`單位矩陣I`，例如，$ AB = BA = I$

  - 若轉換矩陣A會將輸入向量順時鐘轉動90度，A的逆矩陣代表會將輸入向量逆時鐘轉動90度

  - 輸入矩陣經過A變換位置後，若再乘上逆矩陣，會使得輸入矩陣回到原來的位置，
    $\begin{aligned}
      A^{-1}A =
      \begin{bmatrix}
        1 & 0 \\
        0 & 1 \\
      \end{bmatrix}
    \end{aligned}$

  - 逆矩陣常用於解方程，https://youtu.be/uQhTuRlWMxw?si=UTTxCUyiQGY1CIZn&t=318

  - 只有在 $det(A) \neq 0$ 才能找到逆矩陣，並利用逆矩陣求解，
    
    若 $det(A) = 0$，轉換後的面積被壓縮成一條線，就找不到逆矩陣，也無法利用逆矩陣求解，

    但方程式仍然有解，只是解位於直線上，但無法利用逆矩陣求解

- [擴增矩陣](#gaussian-elimination)
  > 將方程式組的係數和值都寫在同一個矩陣中

## 應用，利用矩陣計算向量點積

- 什麼是內積
  
  一個向量在另一個向量上的投影向量的長度

- 方法1，向量計算點積: 
  
  - 兩個向量(對應元素)(乘積)的(和)，是一個`純量`
    - 向量x = $(x_1,x_2, \cdots, x_n)$
    - 向量y = $(y_1,y_2, \cdots, y_n)$
    - 內積 $x \cdot y = x_1y_1 + x_2y_2 + \cdots + x_ny_n = |x||y|cos\theta$
  
  - 兩個向量的夾角為0度，點積的結果最大 ($cos0^\circ = 1$)
  - 兩個向量的夾角為90度，點積的結果為0 ($cos90^\circ = 0$)
  - 兩個向量的夾角為180度，點積的結果最小 ($cos180^\circ = -1$)

- 方法2，利用矩陣計算點積
  - 注意，
    - 矩陣相乘屬於逐元素計算，最後得到的是一個矩陣
    - 矩陣`轉置後相乘`，才是計算向量的內積，最後得到的是一個純量

    - 範例
      ```python
      >>> a = np.array([1,3])
      >>> b = np.array([2,5])
      >>> a * b = [2, 15]   # 矩陣相乘，得到矩陣
      np.dot(a,b) = 17      # 矩陣內積，得到純量
      ```

  - 兩個向量的點積，可以`寫成矩陣乘法`的方式表示，
    - 向量$x$ = $(x_1,x_2, \cdots, x_n)$，列矩陣形式為 $\begin{bmatrix}x_1 & x_2 & \cdots & x_n\end{bmatrix}$

    - 向量$y$ = $(y_1,y_2, \cdots, y_n)$，列矩陣形式為 $\begin{bmatrix}y_1 & y_2 & \cdots & y_n\end{bmatrix}$
    
    - 矩陣點積寫成矩陣形式時，其中一個向量要`轉置為行矩陣`
      
      $x \cdot y$
      
      $$= x^T \cdot y = 

      \begin{bmatrix}
      x_1 \\
      x_2 \\
      \vdots \\
      x_n 
      \end{bmatrix}

      \cdot

      \begin{bmatrix}
      y_1 & y_2 & \cdots & y_n
      \end{bmatrix} 
      =\sum_{i=1}^{n} x_iy_i
      $$

      $$= x \cdot y^T = 

      \begin{bmatrix}
      x_1 & x_2 & \cdots & x_n
      \end{bmatrix}
      
      \cdot

      \begin{bmatrix}
      y_1 \\
      y_2 \\
      \vdots \\
      y_n 
      \end{bmatrix}
      =\sum_{i=1}^{n} x_iy_i
      $$

  - 限制: 因為轉置的需求，`矩陣的形狀必須滿足一定的形狀`，才能執行矩陣內積的運算，
    
    規則: 若矩陣 $A_{m \times n}$ 和矩陣 $B_{n \times p}$，
    若要進行矩陣點積運算，必須滿足`A的列向量個數(n)` == `B的行向量個數(n)`，且兩者相乘的結果 $C$，為大小 $m \times p$ 的矩陣 $C_{m \times p}$，結果矩陣C的元素為 $c_{ij}$ (i最大值為m，j最大值為p)，每一個$c_{ij}$由n個a和b的乘積項相加而成
    
    $C_{m \times p} = 
    \begin{bmatrix}
    c_{11} & c_{12} & \cdots & c_{1p} \\
    c_{21} & c_{22} & \cdots & c_{2p} \\
    \vdots & \vdots & \vdots & \vdots \\
    c_{m1} & c_{m2} & \cdots & c_{ij}
    \end{bmatrix}
    $

    矩陣C中的元素($c_{ij}$)可表示為 

    $c_{ij} = \sum_{k=1}^n(a_{ik} b_{kj})$
  
  - 範例

    矩陣$A_{2 \times 3}$ 和矩陣$B_{3 \times 3}$做點積運算，得到 $m=2，n=3，p=3$，

    得到結果矩陣 $C_{(m=2) \times (p=3)}$，其中，矩陣C中的元素$c_{ij}$，都是由$(n=3)$個ab乘積項相加而成

    $$
    \begin{bmatrix}
    a_{11} & a_{12} & a_{13} \\
    a_{21} & a_{22} & a_{23} \\
    \end{bmatrix}

    \cdot
    
    \begin{bmatrix}
    b_{11} & b_{12} & b_{13} \\
    b_{21} & b_{22} & b_{23} \\
    b_{31} & b_{32} & b_{33} \\
    \end{bmatrix}

    = 

    \begin{bmatrix}
    c_{11} & c_{12} & c_{13} \\
    c_{21} & c_{22} & c_{23} \\
    \end{bmatrix}
    $$

    $$
    c_{11} = \sum_{k=1}^3 a_{1k}b_{k1} = a_{11}b_{11} + a_{12}b_{21} + a_{13}b_{31} \\
    c_{12} = \sum_{k=1}^3 a_{1k}b_{k2} = a_{11}b_{12} + a_{12}b_{22} + a_{13}b_{32} \\
    c_{13} = \sum_{k=1}^3 a_{1k}b_{k3} = a_{11}b_{13} + a_{12}b_{23} + a_{13}b_{33} \\
    c_{14} = \sum_{k=1}^3 a_{1k}b_{k4} = a_{11}b_{14} + a_{12}b_{24} + a_{13}b_{34} \\

    c_{21} = \sum_{k=1}^3 a_{2k}b_{k1} = a_{21}b_{11} + a_{22}b_{21} + a_{23}b_{31} \\
    c_{22} = \sum_{k=1}^3 a_{2k}b_{k2} = a_{21}b_{12} + a_{22}b_{22} + a_{23}b_{32} \\ 
    c_{23} = \sum_{k=1}^3 a_{2k}b_{k3} = a_{21}b_{13} + a_{22}b_{23} + a_{23}b_{33} \\ 
    c_{24} = \sum_{k=1}^3 a_{2k}b_{k4} = a_{21}b_{14} + a_{22}b_{24} + a_{23}b_{34} \\
    $$

- 內積性質
  - 兩個互相垂直的向量，其內積結果為0，

    $\vec{a} = <x_1, x_2, x_3>，\vec{b} = <y_1, y_2, y_3>$
    
    若向量a與向量b互相垂直，則
    $\vec{a} \cdot \vec{b} = x_1y_1 + x_2y_2 + x_3y_3 = 0$

## 應用，求解方程式的解

- 基本概念
  - $x+y$ 和 $x+y=0$ 的差異

    在數學上，$x+y$ 具有兩個變數，若沒有對方程式的有特別的限制，任何的 $x、y$值帶入方程式中都會有一個對應的結果，因此，未加限制(沒有賦值)的雙變數方程式，會形成一個2D的平面空間

    若方程式添加了限制，例如，$x+y=5$，所有符合$x、y$相加等於5這個限制的集合，會被限縮成一條1D的直線

    <img src="pics/different-eq.png" width=50% height=auto>

  - 自由變數(free-variable)的基本概念

    以 $x+y=5$ 為例，在 2d 平面上是一個斜率為的斜線 ，此時，<font color=red>係數不為零的變數，依賴於其他變數才能決定正確值</font>
      - $x$ 的值依賴於$y$，因為 $x = 5-y$
      - $y$ 的值依賴於$x$，因為 $y = 5-x$

    以 $x+\color{blue}{0y}=5$ 為例，當方程式中的某個`變數的係數為0`，則該變數稱為自由變數，該變數可以是任何值，不依賴於其他變數
      - x 的值`依賴於其他變數`或者`為定值`，因為 x = 5
      - y 是自由變數，可以是任何值，也是獨立變數，因為不被其他變數所依賴

  - 變數和解數量的關係 
    - <font color=blue>1個方程式/2個變數</font>的狀況
      - 以 $x+y=5$ 為例 : `無窮多解`，且解分布在一條`1d`直線上

      - 以 $x+0y=5$ 為例 : x是確定值，y的係數為0，代表y是可以為任意值的`自由變量`，
        因此，方程式有`無窮多解`，且分布在一條`1d`直線上

      - 以 $0x+0y=5$ 為例 : `無任何解`

    - <font color=blue>2個方程式/2個變數</font>的狀況

      - 兩個`不平行`的直線`會有交點`，該焦點即為兩個函數解，只有`單一解`
      - 兩個`平行`的直線`不會有交點`，`無任何解`
      - 兩個`重合`的直線`有無限多交點`，有`無窮多解`，例如，$x=5$ 和 $5x=25$
        > 將原方程式乘上倍數後得到新方程式，並不會影響解的位置，兩者是`重合`的

    - <font color=blue>3個方程式/3個變數</font>的狀況
      - 1個方程式/3個變數，在3維的繪圖中為一個2d平面
        因為一個方程式，可以將其中一個變數固定，由剩下兩個變數構成一個平面，
        由兩個變數構成組合成`無限多解`

      - 2個方程式/3個變數，在3維的繪圖中，2個方程式為2個2d平面，2個平面有機會會
        - 相交於一條線，有`無限多解`，且解集合是一個`1d`的直線
        - 兩個平面平行，`無任何解`，
        - 兩個平面重合，有`無限多解`，且解集合是一個`2d`的平面

      - 3個方程式/3個變數，在3維的繪圖中，3個方程式為3個2d平面，3個平面有機會會
        - 3個平面相交於一點，有`單一解`，
        - 2個平面相切，第三個平面與其中一個平面重合(只剩下兩個相切平面)，有`無限多解`，且解集合是一個`1d`的直線
        - 2個平面相切，第三個平面其中一個平面平行(相切和平行同時存在)，`無任何解`
        - 3個平面重合，有`無限多解`，且解集合是一個`2d`的平面
        - 3個平面平行，`無任何解`

    - 結論，在線性系統中，只會有`無任何解`、`單一解`、`無窮多解`，三種狀況產生
      - 不會有n個解的狀況產生，n個解必須需有n個交點，必須是直線和曲線相交於一點才會發生的狀況
      - 在M個方程式和N個變數所組成的線性系統中，只會有`無任何解`、`單一解`、`無窮多解`
        - 若有n個變數，就需要n個方程式才`有機會`相交於一點，
        - 若有n個變數，但不足n個方程式，就只會有`無任何解`或`無限多解`的狀況
        - 無限多解時，`解集合的維度`和是否有`自由變數的數量`有關，
          - 沒有自由變數，代表沒有平行，代表有機會相交，可能會有解
          - 有自由變數，表示重合，代表沒有機會相交

- 計算方程式組的解
  - `方法1`，利用基本列運算(elementary row operations)，計算方程式的解
    - 概念，利用基本列運算的過程，是找出其中一個變數值後，將該值帶回其他方程式中，從而找到其他變數的解
      因此，此過程也稱為反向代回法(Back Substitution)

    - 使用規則: 以下操作不會改變方程式組的解
      - `規則1`，若`係數乘以不為零`的數，會建立重合的執行，但不會影響方程式組的解 (可乘以不為0的數)

      - `規則2`，藉著`和其他row相加減`，可以替換掉當前row，而不影響方程式組的解 (可與其他行相加後，取代當前行)
        - 將其他row乘以一定的倍數後，再與當前行相加減，也不影響方程式組的解 (可`先進行乘法運算後，再相加`)
        - 當前核可與多個row進行加減後，再取代當前和 (可合併多行)
        - <font color=red>注意</font>，任意兩的row相加減，是為了`消除變數`，最後找出`位於交點`的變數值，
          > 方程式相加減會得到另一個新的且通過交點的方程式，會建立新的直線，但不會影響解的位置

      - `規則3`，可以交換任意兩row的位置
    
    - 缺點: 沒有明確的操作順序，也沒有明確的畫簡目標

    - 範例

      若方程式為
      $\begin{cases}
        3x_1 + 3x_2 = 3 \\
        x_1 - x_2 = 0
      \end{cases}$，寫成矩陣形式為
      $\left[
        \begin{array}{cc|c}
        3 & 3 & 3 \\
        1 & -1 & 0 \\
        \end{array}
      \right]$

      - step1，$R_1 = R_1 / 3$，將1th-row除以3後，得到新的1th-row

        $\left[
          \begin{array}{cc|c}
          \color{blue}{1} & \color{blue}{1} & \color{blue}{1} \\
          1 & -1 & 0 \\
          \end{array}
          \right]$
      
      - step2，$R_2 = R_1 - R_2$，將1th-row 減去 2th-row，得到新的 $R_2$

        $\left[
          \begin{array}{cc|c}
          1 & 1 & 1 \\
          \color{blue}{0} & \color{blue}{2} & \color{blue}{1} \\
          \end{array}
          \right]$

        - 由2th-row得到，$2x_2 = 1 \implies x_2 = \frac 1 2$
        - 將 $x_2$ 帶入 1th-row 的 $x_1 + x_2 = 1 \implies x_1 = \frac 1 2$

  - `方法2`，<a id="gaussian-elimination">高斯消去法(Gaussian-Elimination)</a>
    
    - 優點，基於基本列運算，但比基本列運算有更明確的目標和步驟，且更容易判斷是否有解

    - 流程總覽
      - step1，將方程式組寫成`增廣矩陣(augmented-matrices)`的形式

      - step2，將增廣矩陣化簡為`行階梯形矩陣(Row-Echelon-Form，REF)`或`簡化行階梯行矩陣(Reduced-Row-Echelon-Form，RREF)`
        - `化簡類型1`，列階梯形矩陣(REF)
          - 特徵，全零row(zero-row-vector，所有係數都是0的row)會出現在最下方
          - 特徵，每一個row的第一個非零項，必須是1
          - 特徵，第一個非零項(1)的下方都是0，上方可以是任何值
          - 特徵，矩陣的第一個非零項，會呈現`左上右下`的`階梯狀`排列
            - 上方row的第一個非零項一定會比下方row的第一個非零項更靠左
            - 下方row的第一個非零項一定會比上方row的第一個非零項更靠右
            - 第一個非零項會在(上方row第一個非零項)的右側，第一個非零項會在(下方row第一個非零項)的左側

          - 例如，
            $\left[
              \begin{array}{ccc|c}
                1 & a_1 & a_2 & b_1 \\
                0 & 2 & a_3 & b_2 \\
                0 & 0 & 1 & b_3 \\
              \end{array}
            \right]$

        - `化簡類型2`，簡化列階梯行矩陣(RREF)
          - 特徵，不是零列向量的row，每個row`第一個非零項`的`上方和下方`，都必須為0，
            每個row的`第一個非零項`，必須是`當前column`中的`唯一非零項`
          - 特徵，上述列階梯形矩陣的所有特徵
          - 特徵，每個row的第一個非零項必須為1

          - 例如，

            $\left[
              \begin{array}{cccc|c}
                1 & 0 & 0 & 1 & 0 \\
                0 & 1 & 0 & -3 & 6 \\
                0 & 0 & 1 & 0 & 1 \\
                0 & 0 & 0 & 0 & 0 \\
              \end{array}
            \right]$

        - 行階梯形矩陣或簡化行階梯行矩陣的判斷範例

            $\left[
              \begin{array}{ccc|c}
                1 & 0 & 0 & -8 \\
                0 & 1 & 0 & 0 \\
                0 & \color{red}{4} & 1 & 26 \\
              \end{array}
            \right]$，非零項的下方必須為0，不是行階梯型矩陣

            $\left[
              \begin{array}{ccc|c}
                1 & -29 & 3 & 1 \\
                \color{red}{0} & \color{red}{0} & \color{red}{0} & \color{red}{0} \\
                0 & 0 & 1 & 5 \\
              \end{array}
            \right]$，全零row必須在最底下，不是列階梯型矩陣

            $\left[
              \begin{array}{ccc|c}
                0 & 1 & -9 & 0 \\
                0 & 0 & 0 & 1 \\
                0 & 0 & 0 & 0 \\
                0 & 0 & 0 & 0 \\
              \end{array}
            \right]$，是簡化列階梯行矩陣
        
        - 列階梯形矩陣和簡化列階梯形矩陣的性質
          - (列階梯形矩陣)和(<font color=red>簡化</font>列階梯形矩陣)，和原矩陣都是列等價的
          - (列階梯形矩陣)的條件較為寬鬆，
            - (<font color=red>簡化</font>列階梯形矩陣)也屬於(列階梯形矩陣)的一種
            - (<font color=red>簡化</font>列階梯形矩陣)是(列階梯形矩陣)的嚴格且唯一的，
          - 矩陣可以有多個(列階梯形矩陣)，但是只會有一個(<font color=red>簡化</font>列階梯形矩陣)
          - 化簡的過程中會先得到矩陣的(列階梯形矩陣)，再進一步化簡才會得到<font color=red>簡化</font>列階梯形矩陣

      - step3，判讀(行階梯形矩陣)或(簡化行階梯行矩陣)的結果

    - <font color=blue>step1</font>，將方程式組寫成增廣矩陣的形式

      增廣矩陣是將方程式組的`係數和值`寫在一起，並以垂直線加以區隔，例如，

      方程式組為
      $\begin{cases}
        -x -2y +2z = -1 \\
        2x +4y -z  = 5 \\
        x + 2y = 3 \\
      \end{cases}$

      寫成增廣矩陣的形式
      $\left[
        \begin{array}{ccc|c}
          -1 & -2 & 2 & -1 \\
          2 & 4 & -1 & 5 \\
          1 & 2 & 0 & 3 \\
        \end{array}
      \right]$

    - <font color=blue>step2</font>，將增廣矩陣化簡為(列階梯形矩陣)或(簡化列階梯行矩陣)，例如，

      $\left[
        \begin{array}{cccc|c}
          0 & 0 & 0 & 0 & 0 \\
          0 & 0 & 2 & 0 & 0 \\
          0 & 1 & 0 & 1 & 1 \\
          0 & 4 & 3 & 4 & 0 \\
        \end{array}
      \right]$，注意，~ 代表等價於後方的矩陣 <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          0 & 0 & 0 & 0 & 0 \\
          0 & 0 & 2 & 0 & 0 \\
          0 & 1 & 0 & 1 & 1 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{3} & \color{blue}{0} & \color{blue}{-4} \\
        \end{array}
      \right] \qquad R_4 = 4(R_3) - R_4$，去除 $a_{32}$ 底下的4 <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          \color{blue}{0} & \color{blue}{1} & \color{blue}{0} & \color{blue}{1} & \color{blue}{1} \\
          0 & 0 & 2 & 0 & 0 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} \\
          0 & 0 & 3 & 0 & -4 \\
        \end{array}
      \right] \qquad R_1、R_3 交換位置，將含有非零的row往上移$ <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          0 & 1 & 0 & 1 & 1 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{1} & \color{blue}{0} & \color{blue}{0} \\
          0 & 0 & 0 & 0 & 0 \\
          0 & 0 & 3 & 0 & -4 \\
        \end{array}
      \right] \qquad R_2 = \frac 1 2 R_2，將 a_{23} 的係數變更為1$ <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          0 & 1 & 0 & 1 & 1 \\
          0 & 0 & 1 & 0 & 0 \\
          0 & 0 & 0 & 0 & 0 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{-4} \\
        \end{array}
      \right] \qquad R_4 = -3R_2+R_4，將2th-row的非零項底下的數字清除為0$ <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          0 & 1 & 0 & 1 & 1 \\
          0 & 0 & 1 & 0 & 0 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{-4} \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} \\
        \end{array}
      \right] \qquad R_3、R_4 交換位置，將全零row移到底部$ <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          0 & 1 & 0 & 1 & 1 \\
          0 & 0 & 1 & 0 & 0 \\
          \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{0} & \color{blue}{1} \\
          0 & 0 & 0 & 0 & 0 \\
        \end{array}
      \right] \qquad R_3 = -\frac 1 4 ，將a_{35}的值設置為1$ <br><br>
      ~
      $\left[
        \begin{array}{cccc|c}
          \color{blue}{0} & \color{blue}{1} & \color{blue}{0} & \color{blue}{1} & \color{blue}{0} \\
          0 & 0 & 1 & 0 & 0 \\
          0 & 0 & 0 & 0 & 1 \\
          0 & 0 & 0 & 0 & 0 \\
        \end{array}
      \right] \qquad R_1 = -1 R_3 + R_1，將 a_{35}上方的數字清除為0$

    - <font color=blue>step3</font>，判讀(行階梯形矩陣)或(簡化行階梯行矩陣)的結果

- 判讀簡化行階梯矩陣的結果(樞軸位置對解的影響)

  - 什麼是軸樞(pivot)
    
    樞軸 指的是`簡化行階梯矩陣`中，每一row中`第一個非零項`(第一個數字1)出現的位置，例如，

    矩陣A為
    $\begin{bmatrix}
      2 & 4 & 6 & -1 \\
      -3 & 1 & 5 & 0 \\
      1 & 3 & 5 & 1 \\
    \end{bmatrix}$ 

    可化簡得到的簡化行階梯矩陣為
    $\left[
      \begin{array}{cccc|c}
        \color{red}{1} & 0 & -1 & 0 \\
        0 & \color{red}{1} & 2 & 0 \\
        0 & 0 & 0 & \color{red}{1} \\
      \end{array}
    \right]$，紅色標記的1為樞軸位置，指的是`每一個軸(column)最重要的樞紐位置`

    > 在矩陣中，每個 row 是線性組合的限制，也是解的集合，

  - 解線性方程式組(線性系統)時，最關鍵的兩個問題
    - 問題1，`存在性` : 線性系統的解存在嗎，若存在，稱為系統是一致的(consistent)
      > 一致，代表解帶入方程式中，會符合每一個方程式的結果
    - 問題2，`獨特性` : 如果有解，是`唯一解`還是`無限多解`

  - 判斷是否有解
    - 狀況1，若簡化行階梯矩陣的樞軸位置在`擴展矩陣的最右側`(1出現在值的位置)，代表方程式`無解`，例如

      $\left[
      \begin{array}{ccc|c}
        1 & 0 & * & 0 \\
        0 & 1 & * & 0 \\
        0 & 0 & 0 & \color{red}{1} \\
      \end{array}
      \right]$，

    - 狀況2，若係數矩陣的`每一個column都有一樞軸`(每一行都有樞軸)，代表`有唯一解`，例如，

      $\left[
      \begin{array}{ccc|c}
        \color{red}{1} & 0 & 0 & 4 \\
        0 & \color{red}{1} & 0 & 1 \\
        0 & 0 & \color{red}{1} & -2 \\
      \end{array}
      \right]$，

      每一個column都有一個樞軸(標記為紅色)，就可以寫成以下的式子
      $x_1 = 4 \\ x_2 = 1 \\ x_3 = -2$

      由上述可知，當`軸樞的數量 == column的數量`時，會有`唯一解`

    - 狀況3，若`軸樞的數量 < column的數量`時，會有無限多解 (此情況下解有可能是 1d 或 2d 或其他方程式組成)

      $\left[
      \begin{array}{ccc|c}
        \color{red}{1} & 0 & \color{blue}{2} & -2 \\
        0 & \color{red}{1} & \color{blue}{1} & 4 \\
        0 & 0 & 0 & 0 \\
      \end{array}
      \right]$，
      
      3th-column中，$a_{23}=1$的位置，不是第一個非零項，
      且$a_{13}=2$ 的位置必須是0，因此，$x_3$ column 沒有軸樞，

      沒有樞軸代表該軸(column)對應的變數為`自由變數`，代表`該變數並沒有其他約束`，在此範例中，$x_3$為自由變數

      對於有樞軸的變數，可以利用沒有樞軸的自由變數來表示，在此範例中

      - $x_1+2x_3 = -2  \qquad \implies {\color{red}{x_1}} = -2-2\color{blue}{x_3}$
      - $x_2+x_3 = 4  \qquad \implies {\color{red}{x_2}} = 4 - \color{blue}{x_3}$

      <font color=blue>結論</font>

      當`樞軸出現`時，代表該column(該變數)是`存在且唯一`，當每一個column的變數都是存在且唯一時(軸樞的數量 == column的數量)，代表此方程式組有唯一解

      當`樞軸沒有出現`時，說明該column對應的變數是`存在但不是唯一`的，代表該column對應的變數是自由變數，<font color=red>自由變數才會存在多種可能</font>

      當方程式組中，`軸樞的數量 < column的數量`時，代表有自由變數存在，此時有解，但是會出現`無限多解`


## 矩陣的跡(Trace)

## 範例

- 以矩陣解決路徑問題

  考慮以下路徑圖，並轉換為表格
  <img src="pics/matrix-example-path.png" width=100% height=auto>

  轉換為矩陣
  
  $\begin{aligned}
    M_1 = \begin{bmatrix}
    2 & 0 \\
    0 & 1 \\
    1 & 2 \\  
    \end{bmatrix}  \qquad
    M_2 = \begin{bmatrix}
    1 & 1 & 0\\
    0 & 1 & 1\\ 
    \end{bmatrix}  
  \end{aligned}$

  則 $M_1M_2$ 為
  $\begin{aligned}
    \begin{bmatrix}
    2 & 2 & 0\\
    0 & 1 & 1\\
    1 & 3 & 2\\  
    \end{bmatrix}  
  \end{aligned}$

  $M_1M_2$的結果，代表輸入變數(A、B、C、)和輸出變數(F、G、H)之間的關係，D、E 是中間變數

  |     | F   | G   | H   |
  | --- | --- | --- | --- |
  | A   | 2   | 2   | 0   |
  | B   | 0   | 1   | 1   |
  | C   | 1   | 3   | 2   |

  以 A -> F 的路徑為例，有以下幾種路徑可以從A到達F
  - `A -> D -> F`: AtoD 兩條 + DtoF 一條 = 2*1 = 2
  - `A -> E -> F`: AtoE 零條 + EtoF 零條 = 0
  - 因此，A -> F = 2 + 0 = 2
  
  以 C -> G 的路徑為例，有以下幾種路徑可以從C到達G
  - `C -> D -> G`: CtoD 一條 + DtoG 一條 = 1*1 = 1
  - `C -> E -> G`: CtoE 二條 + EtoG 一條 = 2*1 = 2
  - 因此，C -> G = 1 + 2 = 3

## Ref

- [矩陣的本質](https://www.youtube.com/watch?v=4csuTO7UTMo)

- [The Truth Behind Matrix Multiplication @ Data Science Basics](https://www.youtube.com/watch?v=P8jRlZtGbYo&list=PLvcbYUQ5t0UG5v62E_QO7UihkfePakzNA&index=3)

- [如何直觀的理解矩陣的秩](https://www.youtube.com/watch?v=IltCeRSLKy0)

- [視覺化反矩陣、列空間 、秩、零空間的本質](https://www.youtube.com/watch?v=uQhTuRlWMxw)

- 系列影片
  - [互動式淺顯易懂的教學網站 @ David Austin](https://understandinglinearalgebra.org/)
  - [線性代數1 @ 交大OCW課程](https://www.youtube.com/playlist?list=PLj6E8qlqmkFtjxknKFtdxc1_SxNBXgpbo)
  - [線性代數 @ 李宏毅](https://www.youtube.com/playlist?list=PLJV_el3uVTsNQkZFHfcdncAzoesmI6jju)
  - [Data Science Basics](https://youtube.com/playlist?list=PLvcbYUQ5t0UG5v62E_QO7UihkfePakzNA)

