## 矩陣微積分

- 從`鍊式法則`(chain-rule)推廣到`全微分鍊式法則`(total-derivative-chain-rule)

  - 對於一個 $u = f(g(x))$ 的函數，可以透過中間變數g(中間函數g)，計算u函數對x的微分

    $\begin{aligned}
      \frac{\partial{u}}{\partial{x}} = \frac{\partial{u}}{\color{blue}{\partial{g}}} \cdot 
      \frac{\color{blue}{\partial{g}}}{\partial{x}}
    \end{aligned}$

  - 考慮一個複合函數 $y=x+x^2$，同樣可以類似鍊式法則的方式，進行全微分的鍊式法則

    令 $u = x^2$，可以將 $y=x+x^2$ 改寫成以下

    $\begin{aligned}
      & y=x+x^2 = x + u = f(x,u)\\
      & \frac{\partial{y}}{\partial{x}} = 
      \frac{\partial{f(x,u)}}{\partial{x}} \cdot \color{blue}{\frac{\partial{x}}{\partial{x}}} 
      \color{black}{+} 
      \frac{\partial{f(x,u)}}{\partial{u}} \cdot \color{blue}{\frac{\partial{u}}{\partial{x}}}
    \end{aligned}$

  - 透過以上的範例，可以發現多變數的(全微分鍊式法則)有以下的形式

    $f = f(x(u,v), \,\, y(u,v), \,\, z(u,v), \cdots)$，其中，$x、y、z$ 為中間變數(中間函數)，$u、v$為實際變數

    - $df = \color{blue}{\frac{\partial{f}}{\partial{u}}} 
      \color{black}{+} 
      \color{green}{\frac{\partial{f}}{\partial{v}}}$
      
      對 $f$進行微分，可拆分為對每個實際變數($u、v$)微分的`疊加`
    
    - $\color{blue}{\frac{\partial{f}}{\partial{u}}} \color{black}{=}
      \frac{\partial{f}}{\color{red}{\partial{x}}} \cdot \frac{\color{red}{\partial{x}}}{\partial{u}} +
      \frac{\partial{f}}{\color{orange}{\partial{y}}} \cdot \frac{\color{orange}{\partial{y}}}{\partial{u}} +
      \frac{\partial{f}}{\color{purple}{\partial{z}}} \cdot \frac{\color{purple}{\partial{z}}}{\partial{u}}
      $

      透過中間變數($x、y、z$)和鍊式法則，對實際變數($u$)進行微分
    
    - $\color{green}{\frac{\partial{f}}{\partial{v}}} \color{black}{=}
      \frac{\partial{f}}{\color{red}{\partial{x}}} \cdot \frac{\color{red}{\partial{x}}}{\partial{v}} +
      \frac{\partial{f}}{\color{orange}{\partial{y}}} \cdot \frac{\color{orange}{\partial{y}}}{\partial{v}} +
      \frac{\partial{f}}{\color{purple}{\partial{z}}} \cdot \frac{\color{purple}{\partial{z}}}{\partial{v}}
      $

      透過中間變數($x、y、z$)和鍊式法則，對實際變數($v$)進行微分

    將上述整理為更通用的表達公式

    $\begin{aligned}
      \frac{\partial{f(u_1, \cdots, u_n)}}{\partial{x}} = 
      \sum_{i=1}^n \frac{\partial{f}}{\partial{u_i}} \frac{\partial{u_i}}{\partial{x}}
    \end{aligned}$，其中，
    
    - $u_i$ 都是中間變量
    - $x$ 為最末端的實際變數

  - 透過矩陣簡化

    全微分鍊式法則，將函數寫成`中間函數鍊式法則的疊加`後，就`更容易寫成矩陣`的形式進行偏微分，
    透過此方式，就可以利用`矩陣`簡化並計算更複雜函數的微分
    
    例如，
    $\begin{aligned}
      y = \begin{bmatrix}
        f_1(x) \\
        f_2(x) \\
      \end{bmatrix} = 
      \begin{bmatrix}
        \ln(x^2) \\
        sin(3x)
      \end{bmatrix}
    \end{aligned}$

    令 $g_1 = x^2，g_2=3x$，得到

    $\begin{aligned}
      y = 
      \begin{bmatrix}
        f_1(x) = \ln(x^2) \\
        f_2(x) = sin(3x) \\
      \end{bmatrix} = 
      \begin{bmatrix}
        f_1(g) = \ln(g_1) \\
        f_2(g) = sin(g_2) \\
      \end{bmatrix}
    \end{aligned}$

    $\begin{aligned}
     \frac{\partial{y}}{\partial{x}} = 
     \begin{bmatrix}
      \frac{\partial{f_1(g)}}{\partial{x}} \\
      \\
      \frac{\partial{f_2(g)}}{\partial{x}} \\
     \end{bmatrix} 
     & = 
    \begin{bmatrix}
      \frac{\partial{f_1}}{\partial{g_1}} \color{blue}{\frac{\partial{g_1}}{\partial{x}}} \color{black}{+} 
      \frac{\partial{f_1}}{\partial{g_2}} \color{blue}{\frac{\partial{g_2}}{\partial{x}}} \\
      \\
      \frac{\partial{f_2}}{\partial{g_1}} \color{blue}{\frac{\partial{g_1}}{\partial{x}}} \color{black}{+} 
      \frac{\partial{f_2}}{\partial{g_2}} \color{blue}{\frac{\partial{g_2}}{\partial{x}}}
    \end{bmatrix}
    \qquad 可拆分為兩個矩陣的乘積 \\
    & = 
    \begin{bmatrix}
      \frac{\partial{f_1}}{\partial{g_1}} & \frac{\partial{f_1}}{\partial{g_2}}  \\
      \\
      \frac{\partial{f_2}}{\partial{g_1}} & \frac{\partial{f_2}}{\partial{g_2}} 
    \end{bmatrix}
    \begin{bmatrix}
      \color{blue}{\frac{\partial{g_1}}{\partial{x}}} \\
      \\
      \color{blue}{\frac{\partial{g_1}}{\partial{x}}} \\
    \end{bmatrix} \\
    & = 
    \begin{bmatrix}
      \frac{1}{g_1} & 0 \\
      0 & cos(g_2)
    \end{bmatrix} 
    \begin{bmatrix}
      2x \\
      3 \\
    \end{bmatrix} = 
    \begin{bmatrix}
      \frac{2}{x} \\
      3cos(3x) \\
    \end{bmatrix}
    \end{aligned}$

- 求導結果的兩種表達方式

  透過矩陣求導，是將`求導後的結果`透過`矩陣`表達出來，
  
  對於`求導結果的排列方式`，是按照`行向量或列向量`都是可以的，但若是沒有規範，容易造成混淆

  因此，求導結果的排列方式，可分為`分子布局`和`分母布局`兩種
  - 分子布局(numerator-layout): 求導的結果的維度，以分子為主，又稱為 Jacobian-Formulation(雅可比形式)
  - 分母布局(de-nominator-layout) : 求導的結果的維度，以分母為主，又稱為 Hessian-Formlation(海森形式)
  
  注意，
  - 分子布局和分母布局`沒有好壞之分`，取決哪一種表達方式可以`更加簡潔`，且必須保持前後一致，以保持計算的連貫性
  - 若無法識別公式使用的是何種布局，可透過`求導結果矩陣的維度判斷`

## 分子布局與分母布局

- 導數(求導結果)可能出現以下幾種類型

  | 分母\分子 | 標量                                   | 向量                                        | 矩陣                                   |
  | --------- | -------------------------------------- | ------------------------------------------- | -------------------------------------- |
  | 標量      | $\frac{\partial{y}}{\partial{x}}$      | $\frac{\partial{\bf{y}}}{\partial{x}}$      | $\frac{\partial{Y}}{\partial{x}}$      |
  | 向量      | $\frac{\partial{y}}{\partial{\bf{x}}}$ | $\frac{\partial{\bf{y}}}{\partial{\bf{x}}}$ | $\frac{\partial{Y}}{\partial{\bf{x}}}$ |
  | 矩陣      | $\frac{\partial{y}}{\partial{X}}$      | $\frac{\partial{\bf{y}}}{\partial{X}}$      | $\frac{\partial{Y}}{\partial{X}}$      |

  - 標量，以`小寫字母`表示，例如，$y=1$
  - 向量，以`粗體字母`表示，例如，$\bf{y} = [y_1, y_2, \cdots, y_n]^T$，向量通常是一維矩陣，且`默認為列向量` <bp>
  - 矩陣，以`大寫字母`表示，例如，$Y$ = 
    $\begin{bmatrix} y_{11} & \cdots & y_{1p} \\ 
      y_{21} & \cdots & \vdots \\
      y_{m1} & \cdots & y_{mp} \\
    \end{bmatrix}$

- 矩陣微分的基本概念

  - 向量/矩陣的求導，本質上就是多元函數求導，僅僅是將求導的結果排列成矩陣的形式，以更簡潔方便的方式，表達求導結果

  - 狀況1，<font color=blue>對<font color=red>標量函數</font>中的<font color=red>向量</font>進行偏微分</font> (輸入變數為向量，輸出結果為數值)

    以 $y = f(x_1,x_2,x_3) = x_1 + 2x_2 +x_3$ 為例，$y$為標量，$\bf{x}$為列向量，$\bf{x} \rm{=} [x_1, x_2, x_3]^T = \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix}$
    
    - 若要對標量函數中的向量變數做偏微分，$\frac{\partial{y}}{\partial{\bf{x}}_{3\times1}} = \begin{bmatrix}
      \frac{\partial{y}}{\partial{x_1}} \\
      \\
      \frac{\partial{y}}{\partial{x_2}} \\
      \\
      \frac{\partial{y}}{\partial{x_3}} \\
    \end{bmatrix}$
      
      實際上是對標量函數$y$中的每個$x$分量($x_1、x_2、x_3$) 進行偏微分，
    
      默認依照分母$\bf{x}$向量的排列方向，將偏微分的結果`同樣依照列向量方式的排列`(分母布局，求導結果矩陣的排列方向與分母排列方向同)

    - 若`向量出現在分母`卻`使用分子布局`，則需要將求導結果`轉置`為行向量

      考慮分子和分母都是列向量，
      
      $\bf{x} = [x_1, \cdots, x_m]^T=\begin{bmatrix}x_1 \\ \vdots \\ x_m \end{bmatrix}$

      $\bf{y} = [y_1, \cdots, y_n]^T=\begin{bmatrix}y_1 \\ \vdots \\ y_n \end{bmatrix}$
      
      <font color=blue>若為分子布局，求導結果矩陣的方向與分子同，</font>

      - step1，將分子的列向量帶入，分母先不展開，暫時以 $\bf{x}$ 標記

        $\frac{\partial{\bf{y}}}{\partial{\bf{x}}} = \begin{bmatrix}
        \frac{\partial{y_1}}{\partial{\bf{x}}} \\
        \\
        \vdots \\
        \\
        \frac{\partial{y_n}}{\partial{\bf{x}}} \\
        \end{bmatrix}$

      - step2，將分母 $\bf{x}$ 展開，在列向量已經固定的情況下，分母 $\bf{x}$ 只能沿水平方向擴展

        $\frac{\partial{\bf{y}}}{\partial{\bf{x}}} = \begin{bmatrix}
        \frac{\partial{y_1}}{\partial{x_1}} & \cdots & \frac{\partial{y_1}}{\partial{x_m}}\\
        \\
        \vdots & \vdots & \vdots \\
        \\
        \frac{\partial{y_n}}{\partial{x_1}} & \cdots & \frac{\partial{y_n}}{\partial{x_m}}\\
        \end{bmatrix}$

      - step3，由上述可知，分子布局時，需要將分母轉置為行向量的方向

      <font color=blue>若為分母布局，求導結果矩陣的方向與分母同，</font>

      - step1，將分母的列向量帶入，分子先不展開，暫時以 $\bf{y}$ 標記

        $\frac{\partial{\bf{y}}}{\partial{\bf{x}}} = \begin{bmatrix}
        \frac{\partial{\bf{y}}}{\partial{x_1}} \\
        \\
        \vdots \\
        \\
        \frac{\partial{\bf{y}}}{\partial{x_m}} \\
        \end{bmatrix}$

      - step2，將分子 $\bf{y}$ 展開，在列向量已經固定的情況下，分子 $\bf{y}$ 只能沿水平方向擴展

        $\frac{\partial{\bf{y}}}{\partial{\bf{x}}} = \begin{bmatrix}
        \frac{\partial{y_1}}{\partial{x_1}} & \cdots & \frac{\partial{y_n}}{\partial{x_1}}\\
        \\
        \vdots & \vdots & \vdots \\
        \\
        \frac{\partial{y_1}}{\partial{x_m}} & \cdots & \frac{\partial{y_n}}{\partial{x_m}}\\
        \end{bmatrix}$

      - step3，由上述可知，分母布局時，需要將分子轉置為行向量的方向

    結論，從上面的例子可以看出，當分子和分母都是向量時，`分子布局需要將分母轉置，分母布局需要將分子轉置`，

    此規則同樣適用於`對標量函數中的向量進行微分`的狀況，因為可以`將標量函數視為是一個只有一個元素的向量函數`，
    
    因此，對標量函數中的向量進行微分同樣要遵守上述的轉置的規則

  - 狀況2，<font color=blue>對<font color=red>向量函數</font>中的<font color=red>標量</font>進行偏微分</font> (輸入是單一變數，輸出為向量)

    以 $\bf{y} = \begin{bmatrix} y_1 = 2x+1 \\ y_2 = 4x+3 \\ y_3 = 6x+5 \\ \end{bmatrix}$ 為例，
    $\bf{y}$ 為一維列向量的向量函數，$x$為標量函數，

    若要對向量函數$\bf{y}$中的標量$x$進行偏微分，$\frac{\partial{\bf{y}_{3\times1}}}{\partial{x}} = \begin{bmatrix}
      \frac{\partial{y_1}}{\partial{x}} \\ 
      \\
      \frac{\partial{y_2}}{\partial{x}} \\ 
      \\
      \frac{\partial{y_1}}{\partial{x}} \\ 
    \end{bmatrix}$，實際上是對向量函數$\bf{y}$中的標量$x$進行偏微分，

    並依照分子$\bf{y}$向量的排列方向，將偏微分的結果`同樣依照列向量方式的排列`(分子布局，求導結果矩陣的排列方向與分子排列方向同)，
    
    若`向量出現在分子`卻`使用分母布局`，則需要將求導結果`轉置`為行向量
  
- 狀況三，<font color=blue>向量對向量偏微分的<font color=red>分子布局</font></font> (雅可比矩陣)

  - 在`向量對向量`求導中，$\frac{\partial{\bf{y}}}{\partial{\bf{x}}}$，$\bf{y}$和$\bf{x}$都是`列向量` (默認為列向量)
  
  - $\bf{y} = [y_1, y_2, \cdots, y_m]^T = 
      \begin{bmatrix}
        y_1 \\ y_2 \\ \vdots \\ y_m
      \end{bmatrix}_{m\times1}$

  - $\bf{x} = [x_1, x_2, \cdots, x_n]^T = 
      \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_n
      \end{bmatrix}_{n\times1}$

  - $\frac{\partial{\bf{y}}}{\partial{\bf{x}}}$ 為 m x n 個元素的矩陣，寫成分子布局

    $\begin{aligned}
      \frac{\partial{\bf{y}}}{\partial{\bf{x}}} =
      \begin{bmatrix}
        \frac{\color{blue}{\partial{y_1}}}{\partial{x_1}} & \cdots & \frac{\color{blue}{\partial{y_1}}}{\partial{x_n}} \\
        \\
        \frac{\color{blue}{\partial{y_2}}}{\partial{x_1}} & \cdots & \frac{\color{blue}{\partial{y_2}}}{\partial{x_n}} \\
        \\
        \frac{\color{blue}{\partial{y_m}}}{\partial{x_1}} & \cdots & \frac{\color{blue}{\partial{y_m}}}{\partial{x_n}} \\
      \end{bmatrix}
    \end{aligned} = \frac{\partial{\bf{y}}}{\partial{\bf{x}^T}}$ 
    
    - 分子項為列向量，`求導結果的分子`垂直變化($y_1 ... y_m$)，水平方向不變
    - `求導結果的分母`水平變化($x_1 ... x_n$)，垂直方向不變，與原來的變化方向不同，分母的x被`強迫轉置`為水平，因此也寫成 $\frac{\partial{\bf{y}}}{\partial{\bf{x}^T}}$
    - $\frac{\partial{\bf{y}}}{\partial{\bf{x}^T}}$ 的結果為 m x n 大小的矩陣

- <font color=blue>向量對向量偏微分的<font color=red>分母布局</font></font>(海森矩陣)

  - $\frac{\partial{\bf{y}}}{\partial{\bf{x}}}$ 為 n x m 個元素的矩陣，寫成分母布局

    $\begin{aligned}
      \frac{\partial{\bf{y}}}{\partial{\bf{x}}} =
      \begin{bmatrix}
        \frac{\partial{y_1}}{\color{red}{\partial{x_1}}} & \cdots & \frac{\partial{y_m}}{\color{red}{\partial{x_1}}} \\
        \\
        \frac{\partial{y_1}}{\color{red}{\partial{x_2}}} & \cdots & \frac{\partial{y_m}}{\color{red}{\partial{x_2}}} \\
        \\
        \frac{\partial{y_1}}{\color{red}{\partial{x_n}}} & \cdots & \frac{\partial{y_m}}{\color{red}{\partial{x_n}}} \\
      \end{bmatrix}
    \end{aligned} = \frac{\partial{\bf{y}^T}}{\partial{\bf{x}}}$ 
    
    - 分母項為列向量，`求導結果的分母`垂直變化($x_1 ... x_n$)，水平方向不變
    - `求導結果的分子`水平變化($y_1 ... y_m$)，垂直方向不變，與原來的變化方向不同，分子的y被`強迫轉置`為水平，因此也寫成 $\frac{\partial{\bf{y}^T}}{\partial{\bf{x}}}$
    - $\frac{\partial{\bf{y}^T}}{\partial{\bf{x}}}$ 的結果為 n x m 大小的矩陣

- 範例，$A=
  \begin{bmatrix}
  1 & 2 \\
  3 & 5 \\  
  \end{bmatrix}，
  x = 
  \begin{bmatrix}
  x_1 \\
  x_2 \\
  \end{bmatrix}，計算 \frac{\partial{Ax}}{\partial{x}}$ 

  - $\frac{\partial{Ax}}{\partial{x}}$ 的分子項 $Ax = 
    \begin{bmatrix}
    1 & 2 \\
    3 & 4 \\
    \end{bmatrix}
    \begin{bmatrix}
    x_1 \\
    x_2 \\
    \end{bmatrix} = 
    \begin{bmatrix}
    x_1 + 2x_2 \\
    3x_1 + 4x_2 \\
    \end{bmatrix} =
    \begin{bmatrix}
    f_1 \\
    f_2 \\
    \end{bmatrix}
    $，為列向量

  - $\frac{\partial{Ax}}{\partial{x}}$ 的分母項 $x=
    \begin{bmatrix}
    x_1 \\
    x_2
    \end{bmatrix}
    $，為列向量

  - 寫成分子布局，
  
    $\begin{aligned}
    \frac{\partial{Ax}}{\partial{x}} = 
      \begin{bmatrix}
        \frac{\color{blue}{\partial{f_1}}}{\partial{x_1}} & \frac{\color{blue}{\partial{f_1}}}{\partial{x_2}} \\
        \\
        \frac{\color{blue}{\partial{f_2}}}{\partial{x_1}} & \frac{\color{blue}{\partial{f_2}}}{\partial{x_2}} \\
      \end{bmatrix} = 
      \begin{bmatrix}
        1 & 2 \\
        3 & 4
      \end{bmatrix} = A
    \end{aligned}$
    
    分子項 $Ax$ 為`列向量`，因此，
    - 求導結果的`分子`為垂直遞增 (與分子項同)，水平方向不變
    - 求導結果的`分母`為水平遞增，垂直方向不變

  - 寫成分母布局，  
  
    $\begin{aligned}
    \frac{\partial{Ax}}{\partial{x}} = 
      \begin{bmatrix}
        \frac{\partial{f_1}}{\color{red}{\partial{x_1}}} & \frac{\partial{f_2}}{\color{red}{\partial{x_1}}} \\
        \\
        \frac{\partial{f_1}}{\color{red}{\partial{x_2}}} & \frac{\partial{f_2}}{\color{red}{\partial{x_2}}} \\
      \end{bmatrix} = 
      \begin{bmatrix}
        1 & 3 \\
        2 & 4
      \end{bmatrix} = A^T
    \end{aligned}$
    
    分母項 $x$ 為`列向量`，因此，
    - 求導結果的`分母`為垂直遞增 (與分母項同)，水平方向不變
    - 求導結果的`分子`為水平遞增，垂直方向不變
  
  - 由上述範例可知，分子布局/分母布局的結果，會有一個轉置上的差異

- 狀況總表
  
  <img src="pics/matrix-caculs-table.png" width=800 height=auto>

  - 分子布局和分母布局的结果，兩者固定相差一個`轉置`
    - 若向量或矩陣出現在分子，`預設使用分子布局`，求導結果矩陣與分子方向同，若`改用分母布局`則需要進行轉置
    - 若向量或矩陣出現在分母，`預設使用分母布局`，求導結果矩陣與分母方向同，若`改用分子布局`則需要進行轉置

  - 對於有<font color="blue">兩個以上的向量或矩陣</font>，求導結果的排列方式，<font color="red">需要先判斷(先決定)是分子布局或是分母布局</font>  

  - 當向量或矩陣同時出現在分子和分母，
    - 若使用`分子布局`，則先取`分子`的個數做為`行數`，再取`分母`的個數做為`列數`
    - 所使用`分母布局`，則先取`分母`的個數做為`行數`，再取`分子`的個數做為`列數`

## 矩陣的求導概述

- 矩陣的求導的幾種方法
  - 方法1，[基本法則](#矩陣的求導之方法一基本法則)，適用`標量函數對向量`的求導

  - 方法2，[定義法](#矩陣的求導之方法二定義法)，適合簡單的求導
    - `標量函數對矩陣`的求導
    - `向量函數對向量`的求導
    - 缺點，對於複雜的求導式子，中間運算會很複雜

  - 方法3，[微跡法](#矩陣的求導之方法三微跡法)
    - `標量函數對向量`的求導
    - `標量函數對矩陣`

    - 使用條件，

      若標量函數𝑓是矩陣𝑋經由`加/減/乘法、逆、行列式、逐元素函數等運算構成`，就可以透過跡函數得到導數

    - 流程
      - step1，求微分
      - step2，跡函數變換
      - step3，得到求導結果

    - 優點，與定義法相比，不需要對矩陣中的單個標量進行求導後，再進行拼接
    - 缺點，需要熟練矩陣微分的基本規則，和跡函數的性質

  - 方法4，[鏈式法則](#矩陣的求導之方法四鏈式法則)
    - 使用條件，求導的輸入變數和輸出結果之間，有複雜的多層鏈式求導的關係

## 矩陣的求導之方法一，基本法則

- 對`標量函數`的`向量`求導的基本法則
  - <font color=blue>加減法法則</font>

    若 $f、g$都是實值函數，$c_1、c_2$ 為常數，則 

    $$\frac{\partial{( {c_1}{f(\bf{x})} + {c_2}{g(\bf{x})})}}{\partial{\bf{x}}} = 
    c_1 \frac{\partial{f(\bf{x})}}{\partial{\bf{x}}} + 
    c_2 \frac{\partial{g(\bf{x})}}{\partial{\bf{x}}}$$

  - <font color=blue>乘法法則</font>
    
    若 $f、g$都是實值函數，則 

    $$\frac{\partial{f(x) g(x)}}{\partial{\bf{x}}} = 
    f(x)\frac{\partial{g(\bf{x})}}{\partial{\bf{x}}} + 
    g(x)\frac{\partial{f(\bf{x})}}{\partial{\bf{x}}}$$

    <font color=red>注意，若$f、g$不是實質函數，就不能使用上述乘法法則</font>

  - <font color=blue>除法法則</font>

    若 $f、g$都是實值函數，且 $g(x) \neq 0$，則

    $$\frac{\partial{(\frac{f(\bf{x})}{g(\bf{x})})}}{\partial{\bf{x}}} = 
    \frac{1}{g^2(\bf{x})}(
      {g(\bf{x})}\frac{\partial{f(\bf{x})}}{\partial{\bf{x}}} -
      {f(\bf{x})}\frac{\partial{g(\bf{x})}}{\partial{\bf{x}}} 
    )$$

- 對`標量函數`的`矩陣`求導，以定義法求解

  若 $\bf{a}$ 為 m x 1 的列向量，$\bf{b}$ 為 n x 1 的列向量，$X$為 m x n 的矩陣，
  計算 $\frac{\partial{( {\bf{a}^T} {X} {\bf{b}} )}}{\partial{X}}$

  $X_{ij}$為$X$矩陣的元素，對$X$矩陣任一位置的$X_{ij}$求導可表示為

  $\begin{aligned}
    \frac{\partial{{\bf{a}^T} {X} {\bf{b}}}}{\partial{X_{ij}}} = 
    \frac{\partial{( \sum_{p=1}^m \sum_{q=1}^n {a_p} {X_{pq}} {b_j})}}{\partial{X_{ij}}} = 
    \frac{\partial{a_i X_{ij} b_j}}{\partial{X_{ij}}} = a_ib_j 
  \end{aligned} \qquad (此為單一位置的微分結果)$

  擃展到整個矩陣，

  $\begin{aligned}
    \frac{\partial{{\bf{a}^T} {X} {\bf{b}}}}{\partial{X}} = \bf{a}\bf{b}^T
  \end{aligned}$



## 矩陣的求導之方法二，定義法

- 範例1，$\begin{aligned}
  f(x) = A^Tx，A = \begin{bmatrix}
  a_1 \\
  a_2 \\
  \vdots \\
  a_p
  \end{bmatrix}，
  x = \begin{bmatrix}
  x_1 \\
  x_2 \\
  \vdots \\
  x_p
  \end{bmatrix}
  \end{aligned}$，則 $\frac{\color{blue}{\partial{A^Tx}}}{\partial{x}} = \frac{\color{red}{\partial{x^TA}}}{\partial{x}} \color{black}{=} A$

  $\begin{aligned}
    f(x) = A^Tx = [a_1, a_2, \cdots, a_p]
    \begin{bmatrix}
    x_1 \\
    x_2 \\
    \vdots \\
    x_p
    \end{bmatrix} = a_1x_1 + a_2x_2 + a_3x_3 + \cdots + a_px_p = \sum _{i=1}^p a_ix_i
  \end{aligned}$

  - 在矩陣中，`兩個矩陣相乘不具有交換性`，例如，矩陣A和矩陣B相乘，AB $\neq$ BA，但是<font color=blue>將矩陣展開並寫成 $\sum$ 的形式後就具有交換性</font>，
  
    因此，$f(x) = A^Tx = \sum_{i=1}^p \color{blue}{a_i x_i} \color{black}{=} \sum_{i=1}^p \color{red}{x_i a_i} \color{black}{=} x^TA$，
    寫成$\sum$形式後，`[轉置符號]被去除後就可以進行[位置交換]`，還原成轉置符號時，再依照`原來的位置添加轉置符號`

  - 採用分母布局，$\begin{aligned}
    \frac{\partial{f(x)}}{\partial{x}} = 
    \frac{\partial{A^Tx}}{\partial{x}} =
    \frac{\partial{x^TA}}{\partial{x}} = 
    \begin{bmatrix}
     \color{blue}{\frac{\partial{ (\sum _{i=1}^p a_ix_i) }}{\partial{x_1}} = a_1} \\
     \\
     \frac{\partial{ (\sum _{i=1}^p a_ix_i) }}{\partial{x_2}} = a_2 \\
     \\
     \vdots \\
     \\
     \frac{\partial{ (\sum _{i=1}^p a_ix_i) }}{\partial{x}} = a_p \\
    \end{bmatrix} = A
  \end{aligned}$
  
    以第一項的 $\frac{\partial{ (\sum _{i=1}^p a_ix_i) }}{\partial{x_1}} = a_1$ 為例，可以寫成更通用的表達方式，
    
    例如，第j項的求導結果為 $\color{blue}{\frac{\partial{ (\sum _{i=1}^p a_ix_i) }}{\partial{x_j}} = \frac{\partial{a_jx_j}}{\partial{x_j}} = a_j(\frac{\partial{x_j}}{\partial{x_j}}) = a_j(1) = a_j}$ 
    
    當$j=1$時，要計算對 $x_1$ 的偏微分，$f(x)=a_1x_1 + \cdots + a_px_p$ 中，只有$a_1x_1$的項才需要考慮，其他項都視為常數，其他項偏微分的結果為0，
    因此，$\sum _{i=1}^p a_ix_i$ 中，只保留了 $a_jx_j$ 這一項

  - 以微分的角度來看，A是X的係數，可以提取到外部，且 $\frac{\partial{x}}{\partial{x}}=1$，因此可改寫為

    $\begin{aligned}
      \frac{\partial{f(x)}}{\partial{x}} = 
      \frac{\partial{A^Tx}}{\partial{x}} =
      \frac{\partial{x^T\color{red}{A}}}{\partial{x}} = 
      {\color{red}{A}}{\color{blue}{\frac{\partial{x^T}}{\partial{x}}}} = A {\color{blue}{(1)}} = A
    \end{aligned}$

---

- 範例2，$\begin{aligned}
    f(x) = x^TAx，
    x = \begin{bmatrix}
    x_1 \\
    x_2 \\
    \vdots \\
    x_p
    \end{bmatrix}
    A = \begin{bmatrix}
    a_{11} & a_{12} & \cdots & a_{1p} \\
    a_{21} & a_{22} & \cdots & a_{2p} \\
    \vdots & \cdots & \cdots & \cdots \\ 
    a_{p1} & a_{p2} & \cdots & a_{pp} \\
    \end{bmatrix}
  \end{aligned}$，則 $\color{blue}{\frac{\partial{x^TAx}}{\partial{x}} = (A + A^T)x}$

  - 對於一個`相乘的向量或矩陣的結果為標量時`，可以利用 $\sum$ 來表示

    - 例如，$\bf{向量相乘}$

      $\begin{bmatrix}
        a_1 & a_2 & \cdots & a_m
      \end{bmatrix} _{1 \times m} 
      \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_m
      \end{bmatrix} _{m \times 1} = a_1x_1 + a_2x_2 + \cdots + a_mx_m = \sum_{i=1}^{m} a_{i}x_{i}$

    - 例如，$\bf{矩陣和向量相乘}$

      $\begin{bmatrix}
        a_{11} & a_{12} & \cdots & a_{1m} \\
        a_{21} & a_{22} & \cdots & a_{2m} \\
        \vdots & \cdots & \cdots & \vdots \\
        a_{n1} & a_{n2} & \cdots & a_{nm} \\
      \end{bmatrix} _{n \times m} 
      \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_m
      \end{bmatrix} _{m \times 1} = 
      \begin{bmatrix}
        (a_{11}x_1 + a_{12}x_2 +  \cdots + a_{1m}x_m) \\
        \\
        (a_{21}x_1 + a_{22}x_2 +  \cdots + a_{2m}x_m) \\
        \\
        \vdots \\
        \\
        (a_{n1}x_1 + a_{n2}x_2 +  \cdots + a_{nm}x_m) \\
      \end{bmatrix} _{n \times 1} = 
      \begin{bmatrix}
        \sum_{j=1}^m (a_{1j}x_j) \\
        \\
        \sum_{j=1}^m (a_{2j}x_j) \\
        \\
        \vdots \\
        \\
        \sum_{i=1}^n \sum_{j=1}^m (a_{ij}x_j) \\ \\
      \end{bmatrix} _{n \times 1}$
      
      其中，$i$為列方向(垂直)的index，$j$為行方向(水平)的index

  - 將題目的 $f(x) = x^TAx$ 展開

    $\begin{aligned}
      f(x) 
      & = x^TAx \\    %eq1
      & = {\color{blue}{[x_1, x_2, \cdots, x_p] _{1 \times p}}}   %eq2
      {\color{blue}{
        \begin{bmatrix}
          a_{11} & a_{12} & \cdots & a_{1p} \\
          a_{21} & a_{22} & \cdots & a_{2p} \\
          \vdots & \cdots & \cdots & \cdots \\ 
          a_{p1} & a_{p2} & \cdots & a_{pp} \\
        \end{bmatrix} _{p \times p}
      }}
      \begin{bmatrix} 
      x_1 \\
      x_2 \\
      \vdots \\ 
      x_p \\
      \end{bmatrix} _{p \times 1} \qquad ，注意，x^T和x兩個是不同的矩陣 \\
      & = \begin{bmatrix}   %eq3
        {\color{blue}{x_{1}a_{11} + x_{2}a_{21} + \cdots + x_{p}a_{p1}}}, & 
        {\color{green}{x_{1}a_{12} + x_{2}a_{22} + \cdots + x_{p}a_{p2}}}, & \cdots, &
        {\color{red}{x_{1}a_{1p} + x_{2}a_{2p} + \cdots + x_{p}a_{pp}}}
        \end{bmatrix} _{1 \times p}
      \begin{bmatrix} 
        x_1 \\
        x_2 \\
        \vdots \\ 
        x_p \\
      \end{bmatrix} _{p \times 1} \\
      & = \begin{bmatrix}   %eq4
        \sum_{i=1}^p x_ia_{i1}, & \sum_{i=1}^p x_ia_{i2}, & \cdots, & \sum_{i=1}^p x_ia_{ip}
        \end{bmatrix} _{1 \times p} 
        \begin{bmatrix} 
          x_1 \\
          x_2 \\
          \vdots \\ 
          x_p \\
        \end{bmatrix} _{p \times 1}，i \text{代表row-index} \\
      & = \sum_{i=1}^p \sum_{j=1}^p x_ia_{ij}
        \begin{bmatrix} 
          x_1 \\
          x_2 \\
          \vdots \\ 
          x_p \\
        \end{bmatrix} _{p \times 1}，j \text{代表column-index} \\
      & = {\color{blue}{\sum_{i=1}^p \sum_{j=1}^p {\color{red}{x_i}} a_{ij} {\color{red}{x_j}}}} \\ 
    \end{aligned}$
    
    - 為了與前方的 $x_i$ 區別，將後面的 $x$向量 定義為 $x_j$，實際上兩者是位於不同的向量內，在進行偏微分時，`應將兩者視為不同的變數`
    - $x_i$ 和 $x_j$ 的數量都是 p，不需要另外建立新變數，使用 $\sum$ 定義時，也不需要考慮方向

  - 對於雙變數的微分，若 $z=f(x,y)$，則 $\color{blue}{dz = \frac{\partial{z}}{\partial{x}} + \frac{\partial{z}}{\partial{y}}}$，因此，可以上式改寫為

    $f(x) = x^TAx = {\sum_{i=1}^p \sum_{j=1}^p x_i a_{ij} x_j}$

    $\begin{aligned}
      \frac{\partial{(x^TAx)}}{\partial{x}} 
      & = \frac{\partial{(\sum_{i=1}^p \sum_{j=1}^p x_i a_{ij} x_j)}}{\partial{x_k}}，\qquad (k 代表對\sum中特定的x項進行偏微分) \\
      & = (\sum_{i=1}^p \sum_{j=1}^p a_{ij} x_j) \frac{\partial{x_i}}{\partial{x_k}}，
      \qquad (對x_i偏微分時，其他項都是係數，可提至前方) \\
      & + (\sum_{i=1}^p \sum_{j=1}^p x_i a_{ij}) \frac{\partial{x_j}}{\partial{x_k}}，
      \qquad (對x_j偏微分時，其他項都是係數，可提至前方) \\
      & = (\sum_{j=1}^p a_{kj} x_j) \frac{\partial{x_k}}{\partial{x_k}}，\qquad (由範例1可知，只有i=k項的係數才需要保留下來，令i=k) \\
      & + (\sum_{i=1}^p x_i a_{ik} ) \frac{\partial{x_k}}{\partial{x_k}}，\qquad (由範例1可知，只有j=k項的係數才需要保留下來，令j=k) \\
      & = \sum_{j=1}^p a_{kj} x_j + \sum_{i=1}^p x_i a_{ik} = 
      \sum_{j=1}^p {\color{blue}{a_{kj} x_j}} + \sum_{i=1}^p {\color{blue}{a_{ik}x_i}} \qquad (i和j的數量相同，x_i = x_j，且 a_{kj} 和 a_{ik} 互為轉置關係) \\
      & = A^Tx + Ax = (A^T+A)x \qquad (還原為矩陣的型態) 
    \end{aligned}$

- 以微分的角度來看，
  
  $f(x) = x^TAx$，相當於一個係數和兩個變數相乘，例如，$g(x) = {a x_1 x_2}$，

  對$g(x)$進行偏微分可以得到，
  $\frac{\partial{g(x_1,x_2)}}{\partial{x}} = 
  \frac{\partial{g(x_1,x_2)}}{\partial{x_1}} + 
  \frac{\partial{g(x_1,x_2)}}{\partial{x_2}} = \color{blue}{ax_2 + ax_1}$

  對矩陣偏微分，也可以得到類似的結果，差別在於
  - 係數$A$矩陣無法合併為$2A$，只能相加為 $A^T + A$，
  - 具有同樣長度的向量x，才可以進行合併

  $\begin{aligned}
    \frac{\partial{(x^TAx)}}{\partial{x}} = {\color{blue}{A^Tx + Ax}} = (A^T+A)x
  \end{aligned}$

## 矩陣的求導之方法三，微跡法

參考，[對矩陣/向量求導之微分法](https://github.com/FelixFu520/README/blob/main/train/optim/matrix_bp.md#3-%E7%9F%A9%E9%98%B5%E5%90%91%E9%87%8F%E6%B1%82%E5%AF%BC%E4%B9%8B%E5%BE%AE%E5%88%86%E6%B3%95)

- 矩陣微分基本法則

  <img src="./pics/solve-derivative_differential-method_basic-rule.png" width=800 height=auto>

- 跡函數常用的規則

  <img src="./pics/solve-derivative_differential-method_trace-rule.png" width=800 height=auto>

## 矩陣的求導之方法四，鏈式法則

參考，[矩陣/向量求導鏈式法則](https://github.com/FelixFu520/README/blob/main/train/optim/matrix_bp.md#4-%E7%9F%A9%E9%98%B5%E5%90%91%E9%87%8F%E6%B1%82%E5%AF%BC%E9%93%BE%E5%BC%8F%E6%B3%95%E5%88%99)

## 範例集

- 範例1，
  $\begin{aligned}
    A = % item1
    \begin{bmatrix}
    1 & 2 \\
    3 & 4 \\
    \end{bmatrix}，
    \bf{x} =    % item2 
    \begin{bmatrix}
      x_1 \\
      x_2 \\
    \end{bmatrix}
  \end{aligned}$，計算 $\frac{\partial{Ax}}{\partial{x}}$
  
  $\begin{aligned}
    Ax = 
    \begin{bmatrix}
    1 & 2 \\
    3 & 4 \\
    \end{bmatrix}_{2\rm{x}2}
    \begin{bmatrix}
    x_1 \\
    x_2 \\
    \end{bmatrix}_{2\rm{x}1} = 
    \begin{bmatrix}
    x_1+2x_2=f1(x_1, x_2) \\
    3x_1+4x_2=f2(x_1, x_2) \\
    \end{bmatrix}
  \end{aligned}$

  對 $Ax$ 進行矩陣微分，

  寫為分子布局，
  $\begin{aligned}
    \frac{\partial{Ax}}{\partial{x}} =
    \begin{bmatrix}
    \frac{\partial{f_1}}{\partial{x_1}}=1 & 
    \frac{\partial{f_1}}{\partial{x_2}}=2 \\
    \\
    \frac{\partial{f_2}}{\partial{x_1}}=3 & 
    \frac{\partial{f_2}}{\partial{x_2}}=4 \\
    \end{bmatrix} = A
  \end{aligned}$

---

- 範例2，推導對稱矩陣的微分

  若 A 為對稱矩陣，A =
  $\begin{aligned}
    \begin{bmatrix}
    a_{11} & a \\
    a & a_{22} \\
    \end{bmatrix}
  \end{aligned}$，求 $\frac{\partial}{\partial{x}} (x^TAx)$ 的結果

  $\begin{aligned}
    x^TAx 
    & = 
    \begin{bmatrix}
    x_1 & x_2 \\
    \end{bmatrix}
    \begin{bmatrix}
    a_{11} & a \\
    a & a_{22} \\
    \end{bmatrix}
    \begin{bmatrix}
    x_1 \\
    x_2 \\
    \end{bmatrix} \\
    & =
    \begin{bmatrix}
    x_1 & x_2 \\
    \end{bmatrix}_{1{\rm x}2}
    \begin{bmatrix}
    a_{11}x_1 + ax_2 \\
    ax_1 + a_{22}x_2\\
    \end{bmatrix}_{2{\rm x}1} \\
    & = (x_1)(a_{11}x_1 + ax_2) + (x_2)(ax_1 + a_{22}x_2) \\
    & = a_{11}x_1^2 + ax_1x_2 + ax_1x_2 + a_{22}x_2^2 \\ 
    & = a_{11}x_1^2 + 2ax_1x_2 + a_{22}x_2^2 \\
    & = f(x_1,x_2)
  \end{aligned}$

  $\begin{aligned}
    \frac{\partial}{\partial{x}} (x^TAx) =
    \begin{bmatrix}
      \frac{\partial{f}}{\partial{x_1}} \\
      \\
      \frac{\partial{f}}{\partial{x_2}} \\
    \end{bmatrix} 
    & = 
    \begin{bmatrix}
      2a_{11}x_1 + 2ax_2 \\ 
      \\
      2ax_1 + 2a_{22}x_2
    \end{bmatrix} \\
    & = 2 
    \begin{bmatrix}
      a_{11} & a \\
      a & a_{22} \\
    \end{bmatrix}
    \begin{bmatrix}
    x_1 \\
    x_2
    \end{bmatrix} \\
    & = 2Ax
  \end{aligned}$

## Ref

- [Derivative of a Matrix 系列影片 @ ritvikmath](https://www.youtube.com/watch?v=e73033jZTCI)

- [Matrix Calculus 系列影片 @ Breathe Math](https://www.youtube.com/playlist?list=PLhcN-s3_Z7-YS6ltpJhjwqvHO1TYDbiZv)

- [深度学习中的Matrix Calculus](https://zhuanlan.zhihu.com/p/57160155)

- [分子及分母佈局:-(對矩陣求偏導的看法](https://www.cnblogs.com/Linkdom/p/16183282.html)

- [分子分母布局的公式整理](https://vincentqin.tech/posts/matrix-derivative/)

- 矩陣求導(含參考文獻)
  - [矩陣求導-上](https://www.youtube.com/watch?v=66mSyPxL9TU)
  - [矩陣求導-中](https://www.youtube.com/watch?v=k4OxkREiKMo)
  - [矩陣求導-下](https://www.youtube.com/watch?v=TLAcmK5J870)

- [矩陣向量求導 @ github-FelixFu520](https://github.com/FelixFu520/README/blob/main/train/optim/matrix_bp.md)