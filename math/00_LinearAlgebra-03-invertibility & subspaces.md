- 零空間(null-space)、行空間(row-space)、列空間(column-space)
  
  什麼是 null-space
  https://youtu.be/4csuTO7UTMo?si=miej2InKMVbEmtui&t=188

  將方程式加減，並不會影響解的位置(圍繞解轉動)，但轉動會使得平面`有機會`重合
  https://youtu.be/4csuTO7UTMo?si=LpMNbsR8P_QnKIGW&t=
  column-space

  null-space 代表 輸出向量為(0,0,0)
  根據內建，兩個內積的結果為 0，代表兩個向量戶向垂直，
  在方程式組中，矩陣相乘也可以寫程式 row-vector 和 輸入向量的內積，且結果為0

  因此，我們可以找到包含三個 row-vector 的平面，該平面就稱為 row-space，row-space 是一個 2D 的平面
  row-space 必定與 null-space 垂直 (因為內積為0)

  包含三個 column-vector 的平面，該平面就稱為 column-

## 跡相關

- 定義: 主對角線上所有元素的和

  $A = \begin{pmatrix}
    \color{blue}{a_{11}} & a_{12} & \cdots & a_{1n} \\
    a_{21} & \color{blue}{a_{22}} & \cdots & a_{2n} \\
    \vdots & \vdots & \vdots & \vdots \\
    a_{n1} & a_{n2} & \cdots & \color{blue}{a_{nn}} \\
  \end{pmatrix}$

  矩陣A的跡(trace of A)可表示為
  
  $tr(A) = a_{11} + a_{22} + \cdots + a_{nn} = \sum_{i=1}^n a_{ii}$


## 秩相關
  - 利用矩陣的秩(rank)評估解的數量
    https://zh.wikipedia.org/zh-tw/%E7%A7%A9_(%E7%BA%BF%E6%80%A7%E4%BB%A3%E6%95%B0)

  - rank on wiki
    https://zh.wikipedia.org/zh-tw/%E7%A7%A9_(%E7%BA%BF%E6%80%A7%E4%BB%A3%E6%95%B0)
    
  - 以 rank 評估 det(A) = 0

      https://youtu.be/uQhTuRlWMxw?si=np4Zx8vt4CJ7vTvT&t=485

  - 對於一個二維向量，det(A) = 0，代表線性轉換後的解空間只剩下一維(一條直線)，以 rank = 1 來表示
    
  - 對於一個三維向量，det(A) = 0，代表線性轉換後的向量`被降維`，若
    - rank 代表經過線性轉換後，解空間所形成的維度數量
    - rank = 0，解空間只剩下0維的點
    - rank = 1，解空間只剩下1維的直線
    - rank = 2，解空間只剩下2維的平面
    - rank = 3，代表線性轉換後沒有被降維，det(A) != 0

  - 對於線性轉換後，所有可能的輸出所形成的集合(解空間)，稱成列空間
    https://youtu.be/uQhTuRlWMxw?si=pNOJ6H9iE318p-t1&t=536

    線性轉換矩陣的列，實際上是輸入向量轉換後，最後停留的位置，
    所有可能的解，會出現在列的擴展中，因此，所有可能的輸出所形成的集合(解空間)，也被稱成列空間

    列空間是轉換矩陣的擴展，更精確地說，秩式列空間的維度數量

  - 若秩列的數量，稱為矩陣全秩 (full rank)

  - 線性轉換必須保持原點不動，因次 (0, 0) 必定在列空間之中，無論秩的維度是多少

  - 為什麼要有秩

    https://ccjou.wordpress.com/2010/03/02/%E4%BD%A0%E4%B8%8D%E8%83%BD%E4%B8%8D%E7%9F%A5%E9%81%93%E7%9A%84%E7%9F%A9%E9%99%A3%E7%A7%A9/

    https://youtu.be/BiPJnVN_1_8?si=OI4M3_tFJ8oJIeGX&t=53

  - 秩的物理意義

    秩可以用於数据降维和特征选择等，提供了有关矩陣的结构和信息的重要线索。

    用於計算矩陣最小的訊息量，或得到原矩陣最小方程式

    類似篩網的作用，利用秩來減少原矩陣的數據量，只保留必要的訊息

    秩可用於壓縮原始數據的數據量，藉著將原矩陣排列成XXX的形式後，
    用最小的空間儲存必要的數據量

    rank=1，
    rank=2，
    rank=3，
    https://youtu.be/BiPJnVN_1_8?si=sjBJo-DwP5XbpG8b&t=395

  - 什麼是秩
    - 指矩陣中`線性獨立的行／列向量`的`最大個數`
    - 指矩陣對應的線性轉換的像空間的維度
    - 線性獨立

  - 如何計算秩

    https://ccjou.wordpress.com/2010/01/21/%e5%88%a9%e7%94%a8%e8%a1%8c%e5%88%97%e5%bc%8f%e8%a8%88%e7%ae%97%e7%9f%a9%e9%99%a3%e7%a7%a9/

    https://www.youtube.com/watch?v=crTkSmRBFrU&t=1563s

    https://www.youtube.com/watch?v=BiPJnVN_1_8&t=51s

    另類計算 rank 的方法 (非高斯消去法)
    https://www.youtube.com/watch?v=2ogdwpHD3V8&t=50s

  - 在神經網路中的 rank，可以用來壓縮原始矩陣的大小

    https://www.youtube.com/watch?v=2ogdwpHD3V8&t=50s

  - 利用篩網理解秩相關的定理

    https://www.youtube.com/watch?v=IltCeRSLKy0

  - 範例

    https://www.youtube.com/watch?v=BiPJnVN_1_8