## 計算圖和正反向傳播

- 什麼是計算圖
  
  對於一個函數，利用箭頭表示該函數在計算過程中，`變數使用的順序關係`，則該過程圖稱為計算圖

  - 以一般函數為例 : $y = f(x) = x + 1$，計算圖為 
    
    > $x \rightarrow f \rightarrow f(x)$
    
    代表將x帶入函數f中，得到f(x)值

  - 以複合函數為例 : $f(g) = sin(g) 且 g(x)=x^2$，f(g(x)) 的計算圖為

    > $x \rightarrow g \rightarrow g(x)
    \rightarrow f \rightarrow f(x)$

    代表將x帶入函數g得到g(x)，再將g(x)帶入函數f得到f(x)

- 正向傳播是`求值`的過程，反向傳播是`求導`的過程

  以複合函數為例，$f(g) = sin(g) 且 g(x)=x^2$

  - 正向傳播，計算`函數值`的過程
    
    $f(g(x)) = sin(x^2)$

    > $x \rightarrow g \rightarrow g(x)
    \rightarrow f \rightarrow f(g(x))$

    將x傳遞給函數g得到g(x)，再將g(x)傳遞給函數f得到f(g(x))

  - 反向傳播 : 計算`導數`的過程

    $$\begin{aligned}
    & f'(g(x)) 
    = \frac{df}{dg} \cdot \frac{dg}{dx} \\
    & = cos(g) \cdot (x^2)'，step1，計算 \frac{df}{dg} \\
    & = cos(x^2) \cdot (x^2)'，step2，將g替換成g(x) \\
    & = cos(x^2) \cdot (2x)，step3，計算 \frac{dg}{dx}
    \end{aligned}$$

    > $f'(g(x)) \rightarrow f'(g) \rightarrow
    g \rightarrow g'(x) \rightarrow x$

    先計算$\frac{df}{dg}$後，將g替換成g(x)，再計算$\frac{dg}{dx}$，最後再將$x$帶入

  - 比較，從兩者的計算圖可以看出，兩者執行的順序是相反的

    <img src="pics/propage-direction.png" width=800 height=auto>

## 多元變數函數的導數和梯度

- 什麼是多元變數函數

  若函數的輸入為多個變數，例如，$f(x,y) = x +y^2$，此函數稱為`多元變數函數`

  - 定義為 $R^n \rightarrow R$，
  - 左側的$R^n$，代表多個輸入，且輸入為實數(Real)
  - 右側的$R$，代表只有一個輸出，且輸出為實數
  
- 多元變數函數的微分 (多元變數函數的全微分 = 各變數的偏微分相加)
  - <font color=blue>在單變數時</font>

    根據極限的定義，$f(x+\Delta{x})$ = 引數$x$造成$f(x)$的變化量 + 引數$\Delta{x}$造成的變化量

    $\begin{aligned}
      & f(x+\Delta{x}) \approx f(x) + \Delta{y} \\
      & f(x+\Delta{x}) \approx f(x) + f'(x)\Delta{x} \qquad (\because f' = \frac{dy}{dx} \implies dy=f'dx) \\
      & f(x+\Delta{x}) - f(x) \approx f'(x)\Delta{x} \qquad (將f(x)移到左側)
    \end{aligned}$    

  - <font color=blue>在雙變數時</font>

    若 $z = f(x,y)$ 時， 

    $\begin{aligned}
       & f(x+\Delta{x}, y+\Delta{y}) \approx f(x,y) + 
       \frac{\partial{f}}{\partial{x}}\Delta{x} +
       \frac{\partial{f}}{\partial{y}}\Delta{y} \\ 
       & f(x+\Delta{x}, y+\Delta{y}) -  f(x,y) \approx 
       \frac{\partial{f}}{\partial{x}}\Delta{x} +
       \frac{\partial{f}}{\partial{y}}\Delta{y} \qquad \qquad(f(x+\Delta{x}, y+\Delta{y}) -  f(x,y) =dz) \\ 
      & dz = 
      \frac{\partial{f}}{\partial{x}}dx + 
      \frac{\partial{f}}{\partial{x}}dy = f_xdx + f_ydy \qquad (z的變化量 = 所有變數變化量之和)
    \end{aligned}$ 

- 多元變數函數的連鎖律
  - <font color=blue>單變數時</font>
    
    $\begin{aligned}
      & 若 \, y = f(x(t))，\\
      & f'(x(t)) = \frac{df}{dt} = \frac{df}{dx} \times \frac{dx}{dt} = f'(x) \times x'(t)
    \end{aligned}$

  - <font color=blue>雙變數時</font>
    
    $\begin{aligned}
      & 若 z = f(x(t), y(t))，\\
      & z'(x(t), y(t)) = 
      \frac{\partial{z}}{\partial{x}} \frac{dx}{dt} +
      \frac{\partial{z}}{\partial{y}} \frac{dy}{dt} \\
      & = 
      <\frac{\partial{z}}{\partial{x}}, \frac{\partial{z}}{\partial{y}}> \cdot 
      <\frac{dx}{dt}, \frac{dy}{dt}> \qquad 寫成向量內積的形式 \\
      & = \nabla f(x) \cdot <\frac{dx}{dt}, \frac{dy}{dt}>
    \end{aligned}$

  - 範例，若 $w = x^2y+z^2$，且 $y=2t，y =t^2，z=3t$，求
    $\frac{dw}{dt}$

    $\begin{aligned}
      & \frac{dw}{dt} = \frac{\partial{w}}{\partial{x}}\frac{dx}{dt} +
      \frac{\partial{w}}{\partial{y}}\frac{dy}{dt} +
      \frac{\partial{w}}{\partial{z}}\frac{dz}{dt} \\
      & = (2xy \cdot 2) + (x^2 \cdot 2t) + (2z \cdot 3) \\
      & = 4(2t)(t^2) + (2t)^2\cdot2t + (6t)3 = 16t^3 + 18t
    \end{aligned}$

- 多元變數函數的切平面

  - 切平面的用途
    - 切平面是以指定點為中心，在指定位置近似於原函數的的近似函數，可以用於尋找最優解
    - 切平面可以快速逼近函数的根或最小值
    - 切平面可以幫助理解函數的局部行為，並為求解複雜問題提供一種有效的方法

  - 方法1，快速尋找切平面
    - <font color=blue>單變數時</font>，
      
      若 $y = f(x)$，在 $(a, f(a))$ 位置的`切線`可以表示為

      $\begin{aligned}
        y - f(a) = f'(a) (x - a)
      \end{aligned}$
      
    - <font color=blue>雙變數時</font>，切平面可以表示為

      若 $z = f(a,b)$，在 $(a, b, f(a,b))$ 位置的`切平面`可以表示為

      $\begin{aligned}
        z - f(a,b) = \frac{\partial{z}}{\partial{x}} (x - a) + 
        \frac{\partial{z}}{\partial{y}} (y - b)
      \end{aligned}$

      例如，若 $z = f(x,y) = 1 + x^2 + y^2$，在 $(1,2,f(1,2))$ 位置的`切平面`可以表示為
      
      $\begin{aligned}
        & z - f(1,2) = \frac{\partial{z}}{\partial{x}} (x - 1) + 
        \frac{\partial{z}}{\partial{y}} (y - 2) \\
        & \implies z - 6 = 2 \cdot (x - 1) + 4 \cdot (y - 2) \\
        & \implies 2x - 2 + 4y-8 =z-6 \\
        & \implies 2x+4y-z = 4 \qquad 為切平面方程式
      \end{aligned}$

  - 方法2，透過法向量尋找切平面
    
    <img src="pics/tangent-plane.png" width=50% height=auto>

    `法向量`是與切平面互相垂直的向量，可以透過`x軸切線向量`和`y軸切線向量`的`外積`可以得到法向量

    有法向量之後，因為法向量與切平面互相垂直，利用`法向量和切平面向量內積為零`，
    就可以取得切平面的方程式

    - x軸切線向量，是將Z函數沿著X軸方向切一刀後的曲線，並位於該曲線上的單位向量，$<1, 0, \frac{\partial{f}}{\partial{x}}>$
    
      (沿著X軸方向的單位向量，因此x軸的變化只取1，y軸的變化很小趨近於零，z軸的變化由 df/dx 決定)

    - y軸切線向量，是將Z函數沿著y軸方向切一刀後的曲線，並位於該曲線上的單位向量，$<0, 1, \frac{\partial{f}}{\partial{y}}>$

    - 法向量n，是x軸切線向量和y軸切線向量`外積`的結果，

      $\begin{aligned}
        & <1, 0, \frac{\partial{f}}{\partial{x}}> \times <0, 1, \frac{\partial{f}}{\partial{y}}>
        & = <-\frac{\partial{f}}{\partial{x}}, -\frac{\partial{f}}{\partial{y}}, 1>
      \end{aligned}$

    - 利用`切平面上任意一點`與`法向量`內積為0 (切平面與法向量垂直)，可以得到切平面方程式
      
      若切平面上的任意一點為 $<a, b, f(a,b)>$，法向量為
      $<-\frac{\partial{f}}{\partial{x}}, -\frac{\partial{f}}{\partial{y}}, 1>$，
      兩者的內積為 

      $\begin{aligned}
      & -\frac{\partial{f}}{\partial{x}} \cdot (x-a) -
      \frac{\partial{f}}{\partial{y}} \cdot (y-b) + 
      1 \cdot (z - f(a,b)) = 0 \\
      & z - f(a, b) = \frac{\partial{f}}{\partial{x}} \cdot (x-a) + 
       \frac{\partial{f}}{\partial{y}} \cdot (y-b) \qquad 得證
      \end{aligned}$

- 多元變數函數的方向導數(directional derivative)
  - 什麼是方向導數
    - `梯度`是一個`向量`，表示各個方向的變化率，`方向導數`是一個`標量`，代表`沿著指定方向的變化率`
    - 方向導數是`梯度向量投影到指定方向的量`，因此，用指定方向的向量與梯度向量的`內積`來計算
    - 以 $D_uf(x,y)$ 來表示方向導數，例如，$D_{(1,2)}f(x,y)$，代表 $f(x,y)$ 沿 (1,2) 方向的導數
  
  - 定義
    
    若 $z=f(x,y)$ 在 $(x_0, y_0)$ 的位置上有一個單位向量$\vec{u}$，
    方向導數 $D_uf(x_0, y_0)$ 為

    $\begin{aligned}
      D_uf(x_0, y_0) 
      & = \lim_{h \to 0} \frac{f(x_0+hu_1, y_0+hu_2) - f(x_0, y_0)}{h} \\
      & = \nabla f(x_0,y_0) \cdot \vec{u}
    \end{aligned}$

    其中，
    - $\vec{u}$ 是單位向量，$\vec{u} = <u_1, u_2>$ 且 $|\vec{u}|=1$
    - $h$ 為變數，是單位向量的放大倍數

  - <font color=blue>狀況1，若方向向量 u = <1,0> (沿x軸)</font>

    $\begin{aligned}
      & D_uf(x_0, y_0) = \lim_{h \to 0} \frac{f(x_0+h, y_0) - f(x_0, y_0)}{h} \qquad (u_1=1，u_2=0 \, 帶入)\\
      & = \frac{\partial{f}}{\partial{x}}(x_0, y_0) = f_x(x_0, y_0) = D_xf(x,y)
    \end{aligned}$

    當 u = <1,0> 時，相當於計算$(x_0,y_0)$位置對x的偏微分，換句話說，`對x的偏微分可以視為計算<1,0>方向的方向導數`，
    - x=1，代表方向向量指向x軸，只計算梯度在x方向的投影量，只考慮梯度在X軸移動時的狀況
    - y=0，代表方向向量沒有y的分量，不考慮梯度在y方向的投影量，因此不考慮梯度在y軸移動的狀況

  - <font color=blue>狀況2，若方向向量 u = <0,1> (沿y軸)</font>

    $\begin{aligned}
      & D_uf(x_0, y_0) = \lim_{h \to 0} \frac{f(x_0, y_0+u_2) - f(x_0, y_0)}{h} \\
      & = \frac{\partial{f}}{\partial{x}}(x_0, y_0) = f_y(x_0, y_0) = D_yf(x,y)
    \end{aligned}$

    當 u = <0,1> 時，相當於計算$(x_0,y_0)$位置對y的偏微分，換句話說，`對y的偏微分可以視為計算<0,1>方向的方向導數`，
    - x=0，代表方向向量沒有x的分量，不考慮梯度在x方向的投影量，因此不考慮梯度在x軸移動的狀況
    - y=1，代表方向向量指向y軸，只計算梯度在y方向的投影量，只考慮梯度在y軸移動時的狀況
      
  - 以向量內積表示方向導數，<font color=blue>方法1</font>

    - 已知 $f(X) = f(x_1, x_2, x_3)$為多變數函數
    - 一條在 x = a 的逼近f(X)的直線，$L(X) = f(a) + f'(X)(x-a)$，
    由於是逼近曲線，L(X)與f(X)具有相同的斜率，利用 L 代替f計算x點上f的方向導數
    
    $\begin{aligned}
      & D_uf(a) = D_uL(a) = \lim_{h \to 0} \frac{L(a+hu) - L(a)}{h} \qquad 將 L(x) 以 L(a+hu) 進行替換 \\
      & = \lim_{h \to 0} \frac{\color{blue}{f(a)+f'(a)(a+hu-a)} \color{black}{-} \color{red}{[f(a)+f'(a)(a-a)]}}{h} \\
      & = \lim_{h \to 0} \frac{\color{blue}{f(a)+f'(a)(hu)} \color{black}{-} \color{red}{f(a)-0}}{h} \\
      & = \lim_{h \to 0} f'(a)(u) \qquad 求 h 的極限時，f'(a) 和 u 都是常數，可提取到外面 \\
      & = f'(a)u  \qquad 
    \end{aligned}$

    $f'(a)$ 梯度，是一個 1 x n 的行向量，並且 $u$ 是一個 n x 1 的列向量，因此，改寫為

    $\begin{aligned}
      D_uf = \nabla f(a) \cdot \vec{u}
    \end{aligned}$

  - 以向量內積表示方向導數，<font color=blue>方法2</font>

    $\begin{aligned}
      D_uf(x_0, y_0) = \lim_{h \to 0} \frac{f(x_0+hu_1, y_0+hu_2) - f(x_0, y_0)}{h} \\
    \end{aligned}$

    令 $F(h) = f(x(h), y(h))$，其中，$x(h) = x_0+hu_1$ 且 $y(h) = y_0+hu_2$，

    $\begin{aligned}
      & \frac{dF}{dh} = \frac{\partial{F}}{\partial{x}} \frac{dx}{dh} + 
      \frac{\partial{F}}{\partial{y}} \frac{dy}{dh} \qquad 透過連鎖率
    \end{aligned}$
    
    - $x(h) = x_0+hu_1 \implies \frac{dx}{dh} = u_1$
    - $y(h) = y_0+hu_2 \implies \frac{dy}{dh} = u_2$
    
    $\begin{aligned}
      \therefore \frac{dF}{dh} 
      & = \frac{\partial{F}}{\partial{x}} \frac{dx}{dh} + \frac{\partial{F}}{\partial{y}} \frac{dy}{dh} \\
      & = \frac{\partial{F}}{\partial{x}} u_1 + \frac{\partial{F}}{\partial{y}} u_2 \\
      & = \, <\frac{\partial{F}}{\partial{x}}, \frac{\partial{F}}{\partial{y}}> \cdot <u_1, u_2>
    \end{aligned}$

- 多元變數函數的梯度
  
  - 定義
    由方向導數可知，
    $\begin{aligned}
      D_uf(x_0, y_0) = <\frac{\partial{F}}{\partial{x}}, \frac{\partial{F}}{\partial{y}}> \cdot <u_1, u_2>
    \end{aligned}$

    其中，$<\frac{\partial{F}}{\partial{x}}, \frac{\partial{F}}{\partial{y}}>$ 代表<font color=blue>各個方向的變化率</font>，因此，將其定義為梯度，

    $\begin{aligned}
      \nabla = <\frac{\partial{f}}{\partial{x_1}}, \frac{\partial{f}}{\partial{x_2}}, \cdots, \frac{\partial{f}}{\partial{x_n}}>
    \end{aligned}$

    將方向導數重新改寫成梯度的形式

    $\begin{aligned}
      D_uf(x_0, y_0) = <\frac{\partial{F}}{\partial{x}}, \frac{\partial{F}}{\partial{y}}> \cdot <u_1, u_2> = \nabla F \cdot \vec{u}
    \end{aligned}$

  - 斜率和梯度的差別

    |      | 斜率           | 梯度           |
    | ---- | -------------- | -------------- |
    | 輸入 | 單一變數       | 多個變數       |
    | 微分 | 對單一變數微分 | 對多變數偏微分 |
    | 值   | 斜率           | 向量           |

  - 梯度的性質
    - $\nabla f(x_0)$ 垂直於 $f$ 在 $x_0$ 的等位面，$f(x) =f(x_0)$，或
      $\nabla f(x_0)$ 平行於該等位面在 $x_0$ 的法向量
    
    - $\nabla f$ 的向量，指向函數值增加的方向，也是函数值上升最快的方向，反方向为下降最快的方向
    
    - 當前位置的梯度長度，為最大方向導數的值

- 方向導數的極值

  $\begin{aligned}
    D_uf(x_0, y_0) =  \nabla F \cdot \vec{u} 
    & = |\nabla{F}| |\vec{u}| cos\theta \qquad ( \vec{u} 為單位向量， |\vec{u}|=1) \\
    & = |\nabla{F}| cos\theta 
  \end{aligned}$

  - 方向導數的最大值，會出現在 $cos\theta=1$ (夾角為0) 時，即方向向量和梯度向量互相`水平`，
    
    因此，<font color=red>梯度向量的方向，同時也是指向方向導數最大值的方向</font>
  - 方向導數的最小值，會出現在 $cos\theta=0$ (夾角為90度) 時，即方向向量和梯度向量互相`垂直`

- 判斷雙變數函數的極值
  - 極值的特性
    - 極值的切線方程式為水平線，無論是水平切線或垂直切線，斜率都是0
    - 斜率為0，代表 $f'(x) = 0$，
      梯度為0，代表 $\frac{\partial{f}}{\partial{x}} = \frac{\partial{f}}{\partial{x}} = 0$

  - 極值判別式 : $D = f_{xx}f_{yy} - f_{xy}^2$
    - 若 $f_{xx}$ 和 $f_{yy}$ 具有同樣的正負號 ($f_{xx}f_{yy}$相乘為正)，則代表有極值 
    - 若 $f_{xx}$ 和 $f_{yy}$ 具有不同的正負號 ($f_{xx}f_{yy}$相乘為負)，則代表為鞍型(saddle)，
      > 鞍型曲線的極值，同時是最大值，也是最小值

    - 判斷是極大值或是極小值
      - 若 $D>0，且 \, f_{xx} \, 或 \, f_{yy} < 0$ (f(x)為下凹)，代表有極大值
      - 若 $D>0，且 \, f_{xx} \, 或 \, f_{yy} > 0$ (f(x)為上凹)，代表有極小值
      - 若 $D<0，代表為鞍型(saddle)
  
  - 範例，求 $f(x,y) = y^2-x^2$ 的相對極值

    $\begin{aligned}
      & f_x = -2x = 0 \implies x = 0 \\
      & f_y = 2y = 0 \implies y = 0 \\
      & 極值為 (0,0) \\
      & f_{xx} = -2 , f_{yy} = 2，f_{xy} = 0 \\
      & f_{xx} 和 f_{yy} 不同正負號，代表f(x)為鞍型 \\
      & D = f_{xx}f_{yy} - f_{xy}^2 = (-2)(2) - 0 = -4
    \end{aligned}$

## 參考

- [從極限推導多元變數函數的微分(多元變數函數求導)](https://youtu.be/x9Z23o_Z5sQ?si=j4Lu3oX7Hl0otiTD&t=1998)

- [從極限推導多元變數函數的微分(多元變數函數求導)](https://youtu.be/x9Z23o_Z5sQ?si=KjltJ68WzlxBfSZP&t=2735)

- [梯度 @ wiki](https://zh.wikipedia.org/zh-tw/%E6%A2%AF%E5%BA%A6)

- [矩陣導數](https://ccjou.wordpress.com/2013/05/31/%E7%9F%A9%E9%99%A3%E5%B0%8E%E6%95%B8/)

- [方嚮導數和梯度的推導](https://blog.csdn.net/defi_wang/article/details/108399609)
