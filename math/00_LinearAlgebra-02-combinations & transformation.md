
## 向量空間(vector-space)

- 在一個體(field，$F$)之上的向量空間(vector-space，$V$)，是一個由多個向量組成的集合，具有以下的定義
  - (C1) `封閉性`: $x+y \in V，\forall{xy} \in V$
    > $x、y$ 是向量，$x+y$ 也必須是向量 (向量和)，
    
    > 封閉性，指運算後仍然回到向量空間中
  
  - (C2) `封閉性`: $c \cdot x \in V，\forall{x} \in V，\forall{c}\in{F}$
    > $x$ 是向量，乘上係數c後也必須是向量 (係數積)

  - (VS1) `加法交換率`: $x+y=y+x，\forall{xy} \in{V}$
    > 向量和可交換前後對象

  - (VS2) `加法結合率`: $(x+y)+z = x+(y+z)，\forall{xyz} \in{V}$
    > 向量和可更換結合對象

  - (VS3) `加法單位元素`: $\exist \vec{0} \in{V}，\text{so that } x+\vec{0}=x，\forall{x} \in{V}$
    > 當零向量存在於向量空間時，任何向量加上零向量都不會改變原來的值
    
    > 單位元素，指運算後不會改變原來的值，例如乘法中的1，或加法中為0，或矩陣中的單位矩陣

  - (VS4) `加法反元素`: $\forall{x} \in{V}，\exist{y} \in{V} \text{so that } x+y=\vec{0}$
    > 在向量空間中有向量x，必定存在一個與向量y，使得兩者相加為零向量

  - (VS5) `係數積單位元素`: $\forall{x} \in{V}, 1 \cdot x = x$
    > 向量x乘上係數1，不會改變原來的值

  - (VS6) `係數積結合率`: $(ab)x = a(bx)，\forall{ab} \in{F}，\forall{x} \in{V}$
    > 係數積可以變更結合對象

  - (VS7) `係數積分配律`: $a(x+y)=ax+ay，\forall{a} \in{F}，\forall{xy} \in{V}$
    > 係數積的係數，滿足乘法分配律

  - (VS8) `係數積分配律`: $(a+b)x=ax+bx，\forall{ab} \in{F}，\forall{x} \in{V}$
    > 係數積的向量，滿足乘法分配律

- 範例，判斷以下定義是否是向量空間
  - 向量空間 $S = \{ (x,y) | x、y \in{\reals} \}$
  - 係數 $c \in{\reals}$
  - 定義 
    $\begin{cases}
      (x_1, y_1) + (x_2, y_2) = (x_1+x_2, y_1-y_2) \\
      c(x,y) = (cx, cy)
    \end{cases}$
  - 解答

    判斷是否具有加法交換性(見VS1)

    $\begin{aligned}
      (x_1, y_1) + {\color{blue}{(x_2, y_2)}} = (x_1+x_2, {\color{red}{y_1-y_2}}) \\
      {\color{blue}{(x_2, y_2)}} +(x_1, y_1) = (x_2+x_1, {\color{red}{y_2-y_1}}) \\
    \end{aligned}$

    其中，
    
    $y_1-y_2 \, 與 \, y_2-y_1$ 相差負號，兩者不相等，因此不是向量空間

## 子空間 subspace

- 定義，若一個子集合是向量空間的子空間，子集合必須和向量空間V一樣，
  都是基於相同的Field，且具有相同的加法操作和係數積的定義
  
  - <font color=blue>範例，子集合是子空間的範例</font>
    
    有一個由對角矩陣所構成的集合，集合S為向量空間$M_{2\times2}$的子集，且向量空間$M_{2\times2}$是一個2x2實數矩陣構成的向量空間

    $S = \left\{ 
      \begin{pmatrix}
        a & 0 \\
        0 & b \\
      \end{pmatrix} : a、b \in \reals
    \right\} \subseteq M_{2 \times 2}(\reals)$

    - $矩陣A + 矩陣B \in S$
    - $係數c \cdot 矩陣A \in S$
    - $\forall{A、B} \in S$
    
    根據以上條件，則集合S為$M_{2 \times 2} 的子空間$
  
  - <font color=blue>範例，子集合不是子空間的範例</font>

    $S = \left\{ 
      \begin{pmatrix}
        1 & 0 \\
        0 & a \\
      \end{pmatrix} : a \in \reals
    \right\} \subseteq M_{2 \times 2}(\reals)$

    $\begin{pmatrix}
      1 & 0 \\
      0 & a \\
    \end{pmatrix} + 
    \begin{pmatrix}
      1 & 0 \\
      0 & b \\
    \end{pmatrix} = 
    \begin{pmatrix}
      2 & 0 \\
      0 & a+b \\
    \end{pmatrix} \notin S$

    不符合 $\color{blue}{矩陣A + 矩陣B \in S}$ 的條件，
    因此，
    - 集合S`不是`向量空間
    - 集合S`是`$M_{2\times2}$的子集
    - 集合S`不是`$M_{2\times2}$的子空間

- 定理，向量空間的子集合，同時也是子空間的條件
  
  - 從上述範例我們可以得到
    - 子集合不一定是向量空間
    - 子集合不一定是子空間
    - 子空間一定是向量空間，且一定是某個向量空間的子集合
  
  - 若V是向量空間，W是V的子集，當以下條件成立，且只有以下條件成立(若且惟若)，
  W才會是V的子集，同時也是V的子空間
    - 條件1，$(\vec{x}+\vec{y}) \in W，\forall \vec{x},\vec{y} \in W$，加法操作的封閉性
    - 條件2，$(係數c \cdot \vec{x}) \in W，\forall{\vec{x}} \in W，\forall{c} \in F$，係數積的封閉性
    - 條件3，$\exist \vec{0} \in W，使得 \vec{x} + \vec{0} = \vec{x}，\forall{x} \in W$，必定存在零向量
    - 條件4，$\forall{\vec{x}} \in W，\exist{\vec{y}} \in W，使得 \vec{x}+\vec{y} = \vec{0}$，必定存在反元素

- 定理，若向量空間V的多個子空間集合，$\{ V_i \}$，則子空間的交集 $\bigcap V_i$，也會是向量空間V的子空間
  > 參考，https://youtu.be/EoZrqIORhbc?si=fAeJjFCA2MEOORXd&t=2350

- 為什麼子集合不一定等於子空間，<font color=red>子空間一定包含原點，子集合則不一定</font>

  <img src="pics/diff-subset-subspace.png" width=30% height=auto>

  - 一個向量空間V的子集S，是指向量空間中部分元素的集合，這些元素可能滿足某些特定的性質。例如
    
    $S= \{ (x> 5, y<3)，\forall{x,y} \in V \}$

    子集可以包含集合中的任意元素，而不一定滿足任何特定的代數性質，例如，不滿足加法和係數積的封閉性原則

  - 子空間`必須繼承(滿足)向量空間的性質`，包括`加法和係數積的封閉性`、`零向量`等，
    因此，子空間必須包含原點0，才能滿足此二者的封閉性原則

  - 子空間必須包含原點0，子集合不一定包含原點0

## 線性組合

- 將向量拆解成`多個係數積的和`，例如，$x = A + t\vec{u} + s\vec{v}$，此形式稱為線性組合

  例如，(2,3) = 2(1,0) + 3(0,1) = (2,0) + (0,3) = (2,3)

  稱為向量 (1,0)、(0,1) 和 係數 2、3 的線性組合

- 定義，線性組合的定義
  
  > 在一個域F之上的向量空間V，S為向量空間V的子集合，在向量空間V之中的其中一個向量v，若 (在子集合S中存在多個向量$u_1、u_2、\cdots、u_n$) 和 (在域F中存在多個係數 $c_1、c_2、\cdots、c_n$)，使得 $v = c_1u_1 + c_2u_2 + \cdots + c_nu_n$，則向量v稱為線性組合

  > 在此定義中，向量u通常稱為基本向量(elementary-vectors)

  - 範例，線性組合範例

    $4(1,0,0) + 5(0,1,0) + -3(0,0,1) = (4,5,1)$

    是子集合 S = { (1,0,0), (0,1,0), (0,0,1) } 和 係數 4、5、-3 的線性組合

  - 範例，不是線性組合的範例

    $3x^3 - 2x^2 + 7x +8$ 不是 $x^3 - 2x^2 -5x -3$ 和 $3x^3 - 5x^2 - 4x - 9$ 的線性組合

    若是線性組合，可寫成 
    
    $$3x^3 - 2x^2 + 7x +8 
    = c(x^3 - 2x^2 -5x -3) + d(3x^3 - 5x^2 - 4x - 9)
    $$
    
    解出$d=4，c=-9$ 帶入上式後不相等，因此不是線性組合

## 跨度

- 定義，跨度的定義

  S為向量空間V的非空子集合，子集合的跨度(表示為 span(S))，是由子集合S中的向量，所構成的`所有線性組合`的`集合`

  - S是向量空間V的子集合，
    > 例如，$s = \{v_1,v_2\}$

  - 由子集合S中的向量，可以組成的各種的線性組合，
    > 例如，$a \cdot v_1 + b \cdot v_2$，代表S子集合中的向量，可以構成的所有線性組合

  - span(S) 是`所有線性組合`的集合
    > $span(s) = span(\{ v_1, v_2\}) = \{a \cdot v_1 + b \cdot v_2\}$

  簡略的說，跨度是向量集合中，由向量集合中的向量所構建`所有可能的線性組合的集合`，
  若(所有可能的線性組合的集合)`符合特定的表達式`，代表表達式所建構區域，是向量集合`可擴展的最大範圍`

  向量空間V的非空子集合S是一個向量的集合，span(S) 會將這些向量集合`生成(或擴展)成一個向量空間`，由各種線性組合所構成的向量空間
  
- 範例，span的範例
  
  向量空間的子集合$S = \{(1,0,0), (0,1,0)\}$，計算 span(S)

  $span(S) = span( \{(1,0,0), (0,1,0)\} )$，

  代表由S中的向量所構成所有線性組合，且線性組合是由多個係數和向量的乘積所組合成的和，可表示為 $a(1,0,0) + b(0,1,0)$

  因此，結合上述兩個式子，
  $\begin{aligned}
    span(S) 
    & = span( \{(1,0,0), (0,1,0)\} ) \\
    & = \{ a(1,0,0) + b(0,1,0) ，a、b \in \reals \} \\
    & = \{ (a,0,0) + (0,b,0) ，a、b \in \reals \} \\
    & = \{ (a,b,0) ，a、b \in \reals\}
  \end{aligned}$

  $span(S) = \{(a,b,0)\}$，
    - 代表由子集S的`所有線性組合的表達式`，此表達式構建了一個xy平面
    - 代表(1,0,0), (0,1,0) 兩個向量，所建立的所有線性組合，會形成一個xy平面

- 範例，span的範例
  
  向量空間的子集合$S = {(1,x,x^2)}$，計算 span(S)

  $\begin{aligned}
    span(S) 
    & = span(\{(1,x,x^2)\}) \\
    & = \{ a(1) + b(x) + c(x^2)，a、b、c \in \reals \} \\
    & = \{ cx^2 + bx +a，a、b、c \in \reals\} \\
    & = \mathbb{P}_2(\mathbb{R})
  \end{aligned}$

  代表$(1,x,x^2)$的向量集合中，所建立的所有線性組合是一個實數域的二次多項式集合，
  會形成一個拋物線的平面

- 定理，向量空間V的子集合S的跨度，是向量空間V的子空間，並且，任何包含S的向量空間V的子空間，也包含S的跨度 (包含子集合S的子空間，也會包含S的跨度)

  https://youtu.be/Ig_oGQDXFnY?si=JjvCZJPupNEyGFav&t=872

  Q: 子空間必須要包含零向量，若向量空間V的子集合S不包含零向量，那麼子集合S的跨度，還會是向量空間V的子空間嗎

- 利用跨度描述方程式組是否有解

  - 將跨度寫為係數積的形式

    根據跨度的定義，$span(s) = span(\{ v_1, v_2, \cdots, v_n\}) = \{c_1 \cdot v_1 + c_2 \cdot v_2\ + \cdots + c_n \cdot v_n \}$

    若將跨度中的線性組合寫成`係數積`的形式，

    $\begin{aligned}
      span(s) = span(\{ v_1, v_2\, \cdots, v_n\})
      & = \{c_1 \cdot v_1 + c_2 \cdot v_2\ + \cdots + c_n \cdot v_n \} \\
      & = \{ \begin{bmatrix} c_1 & c_2 & \cdots & c_n \end{bmatrix} \cdot \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n\end{bmatrix} \}
    \end{aligned}$

    其中
    - $c_1、c_2、\cdots、c_n$，為單一實數
    - $v_1、v_2、\cdots、v_n$，為向量

    考慮到方程式組中，解向量(x)是由實數構成的單一向量，
    係數矩陣A是由每一個變數的係數向量組成的係數矩陣，因此可改寫為
    
    $\begin{aligned}
      span(s) = span(\{ v_1, v_2\, \cdots, v_n\})
      & = \{c_1 \cdot v_1 + c_2 \cdot v_2\ + \cdots + c_n \cdot v_n \} \\
      & = \{ \begin{bmatrix} c_1 & c_2 & \cdots & c_n \end{bmatrix} \cdot \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n\end{bmatrix} \} \\
      & = \{ \begin{bmatrix} v_1 & v_2 & \cdots & v_n \end{bmatrix} \cdot \begin{bmatrix} c_1 \\ c_2 \\ \vdots \\ c_n\end{bmatrix} \} \\
      & = A_{m \times n} \cdot x_{n \times 1} 
    \end{aligned}$
    
    <font color=red>由上述可發現方程式組和span的關係</font>
      - span中的向量c，對應方程式組的解向量x
      - span中的$v_n$，對應方程式組中，每一個變數的係數向量，多個係數向量組成係數矩陣A
      - span中的子集合s，對應方程式組中，係數向量的集合，
      - span(s)的結果，實際上是結果向量b的集合

    因此，在方程式組中，只要滿足$Ax = b$的方程式，
    代表結果向量$b$，必定是 $span(s)$ 中的`其中一組`由`線性組合構成的結果向量`

  - 範例，考慮向量集合
    $s=\{ 
      v= \begin{bmatrix} -2 \\ 1 \end{bmatrix}，
      w= \begin{bmatrix} 8 \\ -4 \end{bmatrix} 
    \}$

    <font color=blue>向量b是由向量v和向量w構成的線性組合</font>，

    $b = 3v + w =
      3\begin{bmatrix} -2 \\ 1 \end{bmatrix} +
      \begin{bmatrix} 8 \\ -4 \end{bmatrix} = 
      \begin{bmatrix} -6 \\ 3 \end{bmatrix} +
      \begin{bmatrix} 8 \\ -4 \end{bmatrix} =
      \begin{bmatrix} 2 \\ -1 \end{bmatrix} 
    $

    <font color=blue>寫成係數積的形式</font>

    $b = 3v + w = 
      \begin{bmatrix} 3 & 1 \end{bmatrix} \cdot \begin{bmatrix} v \\ w \end{bmatrix} =
      \begin{bmatrix} v & w \end{bmatrix} \cdot \begin{bmatrix} 3 \\ 1 \end{bmatrix}$
    
    在此表達式中，
    - $\begin{bmatrix} 3 & 1 \end{bmatrix}$ 較為單純，可看作是變數的解向量(x)
    - $v、w$ 可看作是每一個變數自身的係數向量，由$v、w$兩個向量可以構成係數矩陣，即係數矩陣

      $A=\begin{bmatrix} v & w \end{bmatrix} = 
      \begin{bmatrix} 
        -2 & 8 \\
        1 & -4 
      \end{bmatrix}$
    
    - 因此，可將 b 改寫為
   
      $\begin{aligned}
        b  
        & = 3v + w \\ 
        & = \begin{bmatrix} v & w \end{bmatrix} \cdot \begin{bmatrix} 3 \\ 1 \end{bmatrix} 
        & = A \cdot x \\
        & = \begin{bmatrix} -2 & 8 \\ 1 & -4 \end{bmatrix} \cdot \begin{bmatrix} 3 \\ 1 \end{bmatrix}
        & = \begin{bmatrix} -2(3)+8(1) \\ 1(3) -4(1) \end{bmatrix}
        & = \begin{bmatrix} 2 \\ -1 \end{bmatrix} 
      \end{aligned}$

    <font color=blue>結論</font>
    - 係數積是線性組合的另外一種表達方式
    - 只要係數積的條件成立，滿足 Ax = b，則結果向量b，必定是向量構成的線性組合，也必定在span的結果之中
    - 只要結果向量b不在span的結果中，代表結果向量不是線性組合的結果，因此，不滿足 Ax = b，則方程式組無解
  
  - 範例，如何判斷結果向量b是否位於span的結果之中(如何判斷方程式是否有解)

## 線性相依和線性獨立

- 名詞解釋: 行空間(row-space) | 列空間(column-space) | 零空間(null-space)

  若有一方程式組
  $\begin{cases}
    x_1 + 0x_2 -2x_3 = 2 \\
    -2x_1 + 2x_2 + 2x_3 = 2 \\
    x_1 + x_2 -3x_3 = -3 \\
  \end{cases}$

  可用以下幾種方式表達

  - <font color=blue>方法1</font>，將方程式組表示為係數積 `Ax = b` 的形式，可以得到`row-space`

    在此形式中，將<font color=red>方程式的係數</font>寫成同一組向量
    
    係數矩陣中的每一個 `row` 都是同一組向量，共有 (1,0,-2)、(-2,2,2)、(1,1,3) 三組，
    由此三組row-vector構成的空間稱為`row-space`

    $\left[
      \begin{array}{ccc}
        \color{blue}{1} & \color{blue}{0} & \color{blue}{-2} \\
        \color{green}{-2} & \color{green}{2} & \color{green}{2} \\
        \color{red}{1} & \color{red}{1} & \color{red}{-3} \\
      \end{array}
    \right]
    \begin{bmatrix}
      x_1 \\ x_2 \\ x_3
    \end{bmatrix} = 
    \begin{bmatrix}
      2 \\ 2 \\ 5
    \end{bmatrix} 
    $

  - <font color=blue>方法2</font>，將方程式組表示為`線性組合` 的形式，可以得到`column-space`
    
    在此形式中，將<font color=red>同一個變數的係數</font>，寫成同一組向量

    將係數矩陣中的每一個`column`寫成同一組向量，共有 (1,-2,1)、(0,2,1)、(-2,2,-3) 三組，
    由此三組column-vector構成的空間稱為`column-space`

    $
    \begin{bmatrix} \color{blue}{1} \\ \color{blue}{-2} \\ \color{blue}{1} \end{bmatrix}x_1 +
    \begin{bmatrix} \color{green}{0} \\ \color{green}{2} \\ \color{green}{1} \end{bmatrix}x_2 +
    \begin{bmatrix} \color{red}{-2} \\ \color{red}{2} \\ \color{red}{-3} \end{bmatrix}x_3 = 
    \begin{bmatrix} 2 \\ 2 \\ 5 \end{bmatrix}
    $

  - <font color=blue>方法3</font>，將方程式組表示為係數積 `Ax = 0` 的形式，可以得到 `null-space`

    null-space 在係數積的基礎之上，專門指結果矩陣b為0時，所有的解x，所構成的解空間

線性組合和係數機等價
https://youtu.be/e0Bx4nZFSlI?si=OPIjS3zqKQZncB5P&t=895
與解的關係
https://youtu.be/e0Bx4nZFSlI?si=NTryjMx9UthQNZCb&t=1157

- 對於方程式組，需要判斷以下兩個問題
  - 問題1，解是否存在
  - 問題2，若有解，是否是唯一解

- 從向量內積到線性獨立
  - Ax=0 和 Ax=b 的區別，Ax=b 找到一組係數向量A，透過頭尾相加會得到b向量
  - 零空间(null-space)是由所有能够使得 Ax=0 的向量x組成的集合
  - 當b向量為0，且三個方程式有解，代表三個方程式會相交於一條直線，
    該直線上的任何一的點，都是三個方程式的其中一個解，
    該直線稱為 null-space，代表使內積為零的解空間，在有解的狀況下，
    因此解空間也稱為零空間
  - 兩個向量的內積為0，代表兩個向量互相垂直
  - 單一方程式 x+2y+4z 可拆分為 (1,2,4) 和 (x,y,z) 的內積
    若 x+2y+4z = 0，代表 (1,2,4) 和 (x,y,z) 兩個向量是互相垂直的
  - 多個方程式為零，代表多個與(x,y,z) 是互相垂直的向量集合
  - 係數矩陣中的每一個 row ，代表其中一個方程式，也代表該方程式係數向量(row-vector)，
    將所有與(x,y,z)垂直的 row-vector 收集起來，會形成 row-space，
    row-space 和 null-space 是互相垂直的
  - 1d大小的null-space (null-space是一條直線) + 2d大小的row-space 
    = 係數矩陣的維度  (係數矩陣的column數量)
  - 方程式寫成Ax=b的形式，可以表示為 row-vector 的內積，
    也可以表示成 x 和 column-vector 和係數的線性組合，
    x(x的column-vector) + y(y的column-vector) + z(z的column-vector) = (0,0,0)
  - 當係數矩陣不為零，結果矩陣b為零，代表線性相依的，
    因為結果矩陣b為零，代表這些vector是受到限制的(限制為零)，不會span到整個向量空間(會被侷限在某個區域)，這些vector所構成的空間稱為 column-space (column-space的集合)
  - column-space 是所有column-vector構成的空間
  - 線性相依時，結果向量b，必須位於column-vectors構成的平面之中
  - column-space 所組成的線性組合，如果沒有限制結果向量b的值 (結果向量b不等於零，b可以是任何值)，
    則代表解 x,y,z 可以是任何值，也代表是線性獨立
  - 矩陣應於餘電路的範例
  - 判斷某個向量是否位於row-space的方法，將該向量與null-space內積，檢查是否為0
  https://youtu.be/4csuTO7UTMo?si=Sg4f5t6Wt0u5Y9Eo&t=337

- 跨度和方程式解的關係 (利用跨度判斷)

  跨度的概念基於一組向量能夠生成一個向量空間（或子空間）。當線性系統存在解時，這意味著原始向量組中的向量可以被線性組合，生成解空間中的向量。
  
## 應用，線性轉換 / 方陣 / 行列式

- 方程式組和線性轉換的關係

  對於一個兩個變數的聯立方程式組，
  $\begin{cases}
    x + 4y = 5 \\
    2x + y = 3 \\
  \end{cases}$

  聯立方程式組中的每一個方程式，可以繪製出一個2d的平面，而聯立方程式的解($x、y$的值)，代表兩個方程式的交點

  將聯立方程式組寫成矩陣的形式，主要不是計算方程式的焦點，更多的在於`向量轉換後的產物`，
  
  例如，可以將上述聯立方程式組寫成矩陣的形式，

  $\begin{aligned}
    \begin{bmatrix} 1 \\ 2 \end{bmatrix}x \, +  \begin{bmatrix} 4 \\ 1 \end{bmatrix}y = 
    \begin{bmatrix} 5 \\ 3 \end{bmatrix}
  \end{aligned}$
  
  將係數合併成一個矩陣，並寫成`矩陣相乘`的形式

  $\begin{aligned}
    & \begin{bmatrix}   %item1
      1 & 4 \\
      2 & 1 \\
    \end{bmatrix}
    \begin{bmatrix} x \\ y \end{bmatrix} = 
    \begin{bmatrix} 5 \\ 3 \end{bmatrix} \\
    & \quad {\color{blue}{A}} \quad \quad \vec{x} \;\;\; = \; {\color{blue}{\vec{v}}}
  \end{aligned}$
  
  其中，變數係數矩陣A也稱`轉換矩陣`，$\vec{x}$為`輸入變數向量`，$\vec{v}$為輸出的`輸出向量`

  本質上，將方程式寫成`矩陣相乘`的形式後，代表透過轉換矩陣A，可以將輸入向量$\vec{x}$映射成輸出向量$\vec{v}$，
  因此，方程式的係數(轉換矩陣A)描述了<font color=blue>輸入向量$\vec{x}$和輸出向量$\vec{v}$之間的映射關係</font>，因此也稱為線性轉換
  
  考慮下圖，
  - 若輸入向量為(x=1,y1)，則輸出向量為(x=5,y=3)
  - 若輸入向量為(x=2,y2)，則輸出向量為(x=10,y=6)
  - 由上述可以發現無論輸入向量為何，輸出結果必定出現在輸出向量形成的直線上
  
  <img src="./pics/eq-to-matrix.png" width=70% height=auto>

  將方程式的係數寫成轉換矩陣後，<font color=blue>轉換矩陣中的每一個<font color=red>列向量</font>都代表一組向量</font>，透過方程式組和矩陣，可以很方便的表示<font color=blue>向量映射前後的線性關係</font>

--- 

(待整理)
$\begin{cases}
  a_{11}x_1 + a_{12}x_2 + \cdots + a_{1n}x_n = b_1 \\ 
  a_{21}x_1 + a_{22}x_2 + \cdots + a_{2n}x_n = b_2 \\
  \vdots \qquad \qquad \vdots \qquad \qquad \vdots \\
  a_{m1}x_1 + a_{m2}x_2 + \cdots + a_{mn}x_n = b_m \\
\end{cases}$

將上述方程式寫成矩陣的形式

$\begin{bmatrix}    %item1
  a_{11} & a_{12} & \cdots & a_{1n} \\
  a_{21} & a_{22} & \cdots & a_{2n} \\
  \vdots & \vdots & \vdots & \vdots \\
  a_{m1} & a_{m2} & \cdots & a_{mn}
\end{bmatrix}
\begin{bmatrix}    %item2
  x_1 \\ x_2 \\ \vdots \\ x_n \\
\end{bmatrix} = 
\begin{bmatrix}    %item3
  b_1 \\ b_2 \\ \vdots \\ b_m \\
\end{bmatrix}$，得到矩陣形式的 $\bf{A} \bf{x} = \bf{b}$

- 什麼是線性轉換

  對於一個線性方程式組，寫成矩陣形式 $A \bf{x} = \bf{v}$，代表找到一個向量x，向量x在經過線性轉換的作用後，會變成向量v

  https://youtu.be/uQhTuRlWMxw?si=VgqC8VnhuPSgoZnL&t=177

- 利用矩陣來表達線性轉換，且用於`描述線性轉換`的矩陣，通常是一個`方陣`

  透過矩陣乘法，使原矩陣中的所有元素都乘上特定的倍數，可以將原矩陣映射為另一個新的矩陣，對原矩陣造成形變、鏡射、旋轉、伸縮、推移、等各種的映射

  <img src="./pics/matrix-linear-transform.png" width=80% height=auto>

- 行列式
  - 限制，只有`方陣`能計算行列式

  - 功能1，行列式的值(det(a))，表示示了矩陣所描述的`線性變換`，對空間體積的`伸縮因子`

    - 線性轉換方陣的每一個 column，代表每一軸(pivot)轉換之後的位置

      例如，A為單位矩陣
      $\begin{aligned}
        \begin{bmatrix}
          1 & 0 \\
          0 & 1 \\
        \end{bmatrix}
      \end{aligned}$，代表由向量(1,0)和向量(0,1)之間的面積，

      若由一個轉換矩陣，
      $\begin{aligned}
        T = 
        \begin{bmatrix}
          3 & 0 \\
          0 & 2 \\
        \end{bmatrix}
      \end{aligned}$，代表經由轉換矩陣T的轉換後，會變成向量(3,0)和向量(0,2)之間的面積，

      經過T轉換後，面積會擴大為 3 * 2 的面積

    - 伸縮因子是利用線性轉換前後的面積，來評估放大或縮小
      - 轉換前面積為1，轉換後面積為6，伸縮因子為 6/1 = 6
      - https://youtu.be/Ip3X9LOh2dk?si=XVy-2IVMy_UFQ4kk&t=72

    - det(A) > 0，伸縮是正向的，圖形會擴大
    - det(A) < 0，伸縮是反向的，圖形會翻轉
    - det(A) = 0，代表轉換後的面積被壓縮成一條直線(面積被壓縮為0)
      - 兩個軸是位於相同的一條線上
      - 所有的解都會位於該直線上
  
  - 功能2，行列式的值，可以用來判斷`線性方程組是否有解`
  
  - 功能3，用於判斷是否能計算出指定矩陣的逆矩陣 ($det(A) \neq 0$ 才能求逆矩陣 $A^{-1}$)

- 行列式的計算

  https://youtu.be/Ip3X9LOh2dk?si=tBWsJH0udwyHIuyK&t=522

- 特徵向量和特徵值

  https://www.youtube.com/watch?v=M5_2L4Ax-Pc

## Ref

- [子空間的定義 + 定理的證明](https://www.youtube.com/watch?v=EoZrqIORhbc)
- [線性組合與線性系統 + 定理的證明](https://www.youtube.com/watch?v=Ig_oGQDXFnY)
